import * as clipboardy from "clipboardy";
import { parseTaikoHitObjects } from "../hitObjectsParser";
import { TaikoCircle } from "../interfaces";
import { Storyboard } from "../storyboard-helpers";

const storyboard = new Storyboard();

export const generateTaikoObjects = async () => {
    const taikoObjects = (await parseTaikoHitObjects(`${__dirname}/taiko.txt`));
    taikoObjects.forEach(taikoObj => {
        generateCircle(taikoObj);
        generateCircleOverlay(taikoObj);
    });

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const generateCircle = (taikoCircle: TaikoCircle): void => {
    const { startPositionSB, endPositionSB, timeStamp, duration, color } = taikoCircle;
    const rgb = color === "red" ? "235,69,44" : "68,141,171";
    storyboard
        .loadSprite("taikohitcircle.png", startPositionSB)
        .scale(1, null, 0, 0.6)
        .color(timeStamp - duration, null, "255,255,255", rgb)
        .fade(timeStamp - duration, null)
        .move(timeStamp - duration, timeStamp, startPositionSB, endPositionSB);
};

const generateCircleOverlay = (taikoCircle: TaikoCircle): void => {
    const { startPositionSB, endPositionSB, timeStamp, duration, color } = taikoCircle;
    storyboard
        .loadSprite("taikohitcircleoverlay.png", startPositionSB)
        .scale(1, null, 0, 0.6)
        .fade(timeStamp - duration, null)
        .move(timeStamp - duration, timeStamp, startPositionSB, endPositionSB);
};

generateTaikoObjects();