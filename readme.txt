SETUP
*****
1. Download NodeJS from here: https://nodejs.org/en/ (recommended version is fine)
	If you don't know if you already have NodeJS, then you don't have NodeJS
2. Doubleclick setup.bat to execute it (you won't get a virus I promise)
	This will install the required dependencies

HOW TO USE
**********
1. Map some circles. Save the map. Open the .osu file, find the circles you mapped and copy them (one combo at a time)
2. Open the circles.txt file
3. Edit the file as you wish. It is pretty self-explanatory but here is an explanation anyway:
	3a. First line: the desired approach rate. Keep the AR: part and put the number behind it (no space in between)
	3b. Second line: the desired color of the hitcircles. Keep the Color: part and put the RGB value behind it, seperated by commas
	3c. Third line: the desired circle size. Keep the CS: part and put the number behind it.
4. Next, copy the hitcircles from the .osu file on the fourth line and below. Make sure every circle is on a seperate line
5. Save the file and close it
6. Run the create_circles.bat file.
7. A storyboard will be generated and copied to your clipboard
8. Paste it into your storyboard, save the storyboard and reload the map.
9. Tadah!

The combo numbers are based on the order of the circles in the circles.txt file (e.g. first circle is 1, second is 2, etc).
If you have more than 9 circles it will break from the 10th circle onward.

HITBURSTS
*********
These are generated with triggers, meaning they will only show up in the storyboard if you clicked the circle.
It currently checks on any hitsound from the first custom sample set, so make sure to put that on the circles you want these hitbursts to work on.

1. Map some circles and/or sliders. Both will work for this one. Again, open the .osu file and copy what you mapped (combo doesn't matter now)
2. Open the hitbursts.txt file and paste it all in there.
3. Now for every slider, you need to specify the end time. This might get a bit tedious but I only used this once so I didn't bother to improve it.
	3a. Find the end time of a slider in the editor (in milliseconds)
	3b. Add a hashtag (#) to the end of the slider in the hitbursts.txt file and then append the end time
	3c. So it will look something like this: 34,64,167599,2,8,B|27:109|-31:151|-90:117,1,157.500006008148#167786 (where 167786 is the end time)
	3d. Do this for every slider in the hitbursts.txt file
4. Save file, close it
5. Run the create-hitbursts.bat file; this will again generate a storyboard and copy it to your clipboard. You can now paste it into your storyboard.

They are all hit300.png files. If you want 100 or 50 or whatever on some of them you'll need to manually change that.