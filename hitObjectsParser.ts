import * as fs from 'fs';
import { convertToSBPosition } from './helpers';
import { HitObjectOld, Position, Slider, HitCircle, TaikoCircle, TaikoColor, Direction, CtbFruit, CtbDirection, ManiaNote, ManiaDirection } from './interfaces';

export const parseHitObjects = (fileName): Promise<HitObjectOld[]> => {
    return new Promise(resolve => {
        fs.readFile(fileName, (err, data) => {
            if (!data) return resolve([]);
            const lines = data.toString().split('\r\n');
            const hitObjects: HitObjectOld[] = [];
            let ar: number = 9;
            let cs: number = 4;
            let color: string = "106,106,106";
            let backwards: boolean = false;

            let currentCircleNumber = 1;

            lines.forEach(line => {
                if (line.includes("AR"))
                    ar = +line.split(':')[1];
                else if (line.includes("CS"))
                    cs = +line.split(':')[1];
                else if (line.includes("Color"))
                    color = line.split(':')[1];
                else if (line.includes("Backwards"))
                    backwards = line.split(':')[1].trim() === "true";
                else if (line.includes('|')) {
                    // Slider
                    const endTime = line.includes('#') ? +line.split('#')[1] : undefined;
                    const [startX, startY, timeStamp, type, __, ...rest] = line.split(',');
                    const startPosition: Position = { x: +startX, y: +startY };
                    const otherPoints = rest[0].split('|');
                    otherPoints.shift();
                    const sliderPoints: Position[] = [...otherPoints].map(point => {
                        const [x, y] = point.split(':').map(v => +v);
                        return { x, y };
                    });
                    const endPosition = sliderPoints.pop();

                    if (+type === 6) currentCircleNumber = 1;
                    else currentCircleNumber++;

                    const slider: Slider = {
                        position: startPosition,
                        sbPosition: convertToSBPosition(startPosition),
                        startTime: +timeStamp,
                        endTime,
                        sliderPoints,
                        sbSliderPoints: sliderPoints.map(s => convertToSBPosition(s)),
                        endPosition,
                        sbEndPosition: convertToSBPosition(endPosition),
                        backwards,
                        rawString: line,
                        circleNumber: currentCircleNumber,
                        approachRate: ar,
                        circleSize: cs,
                        color
                    }
                    hitObjects.push(slider);
                }
                else {
                    // Not a slider
                    const [x, y, timeStamp, type, ...rest] = line.split(',');

                    if (+type === 12) {
                        // Spinner
                        return;
                    }

                    // Circle
                    const startPosition: Position = { x: +x, y: +y };
                    if (+type === 5) currentCircleNumber = 1;
                    else currentCircleNumber++;

                    const circle: HitCircle = {
                        position: startPosition,
                        sbPosition: convertToSBPosition(startPosition),
                        startTime: +timeStamp,
                        approachRate: ar,
                        circleSize: cs,
                        color,
                        rawString: line,
                        circleNumber: currentCircleNumber
                    };
                    hitObjects.push(circle);
                }
            });
            resolve(hitObjects);
        });
    });
}

export const parseTaikoHitObjects = (fileName: string): Promise<TaikoCircle[]> => {
    return new Promise(resolve => {
        fs.readFile(fileName, (err, data) => {
            const lines = data.toString().split('\r\n');
            const taikoObjects: TaikoCircle[] = [];
            let duration = 1200; // milliseconds
            let direction: Direction = "left";
            let color: TaikoColor = "red";
            lines.forEach(line => {
                if (line.includes("Duration"))
                    duration = +line.split(':')[1].trim();
                else if (line.includes("Direction"))
                    direction = line.split(':')[1].trim() as Direction;
                else if (line.includes("Color"))
                    color = line.split(':')[1].trim() as TaikoColor;
                else {
                    const [x, y, timeStamp, ...rest] = line.split(',').map(v => +v);
                    const startPosition = {
                        x: direction === "left" ? 900 : -260,
                        y
                    };
                    taikoObjects.push({
                        duration,
                        direction,
                        startPosition,
                        startPositionSB: convertToSBPosition(startPosition),
                        endPosition: { x, y },
                        endPositionSB: convertToSBPosition({x, y}),
                        timeStamp,
                        color
                    });
                }
            });
            resolve(taikoObjects);
        });
    });
};

export const parseCtbObjects = (fileName: string): Promise<CtbFruit[]> => {
    return new Promise(resolve => {
        fs.readFile(fileName, (err, data) => {
            const lines = data.toString().split('\r\n');
            const ctbFruits: CtbFruit[] = [];
            let duration = 1200; //milliseconds
            let direction: CtbDirection = "down";
            let endY;
            const fruitLines: string[] = [];
            lines.forEach(line => {
                if (line.includes("Duration"))
                    duration = +line.split(':')[1].trim();
                else if (line.includes("Direction"))
                    direction = line.split(':')[1].trim() as CtbDirection;
                else if (line.includes("End"))
                    endY = +line.split(':')[1].trim();
                else
                    fruitLines.push(line);
            });

            if (!endY)
                endY = direction === "down" ? 341.5 : 25.5; // 274.5 for Jack of all Trades

            fruitLines.forEach(line => {
                const [x, _, timeStamp, ...rest] = line.split(',').map(v => +v);
                const startPosition = { x, y: direction === "down" ? -218.5 : 585.5 };
                const endPosition = { x, y: endY };
                ctbFruits.push({
                    duration,
                    direction,
                    startPosition,
                    startPositionSB: convertToSBPosition(startPosition),
                    endPosition,
                    endPositionSB: convertToSBPosition(endPosition),
                    timeStamp
                });
            });
            resolve(ctbFruits);
        });
    });
};

export const parseManiaObjects = (fileName: string): Promise<ManiaNote[]> => {
    return new Promise(resolve => {
        fs.readFile(fileName, (err, data) => {
            const lines = data.toString().split('\r\n');
            const maniaNotes: ManiaNote[] = [];
            let direction: ManiaDirection = "down";
        });
    });
};

export const isHitCircle = (hitObject: HitObjectOld) => hitObject['endPosition'] === undefined;

export const isSlider = (hitObject: HitObjectOld) => hitObject['endPosition'] !== undefined;