import { Storyboard } from "./storyboard-helpers";
import * as clipboardy from "clipboardy";

const generateStoryboard = () => {
    let startTime = 100261;
    const endTime = 130981;
    const storyboard = new Storyboard();
    const colors = [
        "255,150,150",
        "150,255,150",
        "150,150,255",
        "255,150,255",
        "150,255,255",
        "255,255,150"
    ];
    // const colors = [
    //     { r: 255, g: 150, b: 150 },
    //     { r: 150, g: 255, b: 150 },
    //     { r: 150, g: 150, b: 255 },
    //     { r: 255, g: 150, b: 255 },
    //     { r: 150, g: 255, b: 255 },
    //     { r: 255, g: 255, b: 150 }
    // ];

    let index = 0;

    storyboard.loadSprite("", { x: 320, y: 240 });
    storyboard.color(startTime, startTime + 480, "255,255,255", "255,150,150");
    startTime += 480;

    for (let i = startTime; i < endTime; i += 480) {
        storyboard.color(i, i + 480, colors[index], colors[index + 1 > 5 ? 0 : index + 1]);
        index = (index + 1) % 6;
    }

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

generateStoryboard();