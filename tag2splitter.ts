import fs from "fs";

const baseFile = `osu file format v14

[General]
AudioFilename: Imprinting.mp3
AudioLeadIn: 0
PreviewTime: 78380
Countdown: 0
SampleSet: Soft
StackLeniency: 0
Mode: 0
LetterboxInBreaks: 0
WidescreenStoryboard: 0

[Editor]
DistanceSpacing: 1
BeatDivisor: 4
GridSize: 4
TimelineZoom: 2

[Metadata]
Title:Imprinting
TitleUnicode:Imprinting
Artist:Sakuzyo
ArtistUnicode:削除
Creator:Cherry Blossom
Version:TAG4
Source:
Tags:
BeatmapID:679004
BeatmapSetID:302972

[Difficulty]
HPDrainRate:5
CircleSize:5
OverallDifficulty:7
ApproachRate:8
SliderMultiplier:1.8
SliderTickRate:1

[Events]
//Background and Video events
0,0,"it.jpg",0,0
//Break Periods
//Storyboard Layer 0 (Background)
//Storyboard Layer 1 (Fail)
//Storyboard Layer 2 (Pass)
//Storyboard Layer 3 (Foreground)
//Storyboard Sound Samples

[TimingPoints]
2780,342.857142857143,4,2,0,50,1,0
79580,-100,4,2,1,50,0,1
89865,-100,4,2,1,50,0,0
90551,-100,4,2,1,50,0,1
101522,-100,4,2,1,50,0,0
102894,-100,4,2,1,50,0,0
112494,-100,4,2,1,50,0,0
113780,-100,4,2,1,50,0,0
113865,-100,4,1,1,50,0,0


[Colours]
Combo1 : 7,164,248
Combo2 : 7,248,146
Combo3 : 0,0,128
Combo4 : 254,80,140

[HitObjects]
`;

const splitMap = async () => {
    const fileName = `O:\\osu!\\Songs\\302972 Sakuzyo - Imprinting\\Sakuzyo - Imprinting (Cherry Blossom) [TAG4].osu`;
    const map = fs.readFileSync(fileName).toString();
    const lines = map.split("\n").map(l => l.trim());

    const player1: string[] = [];
    const player2: string[] = [];

    let currentPlayer = player2;

    const hitObjectsStart = lines.findIndex(l => l === "[HitObjects]");
    for (let i = hitObjectsStart + 1; i < lines.length; i++) {
        const line = lines[i];
        const [x, y, time, type, ...rest] = line.split(",");

        if ([5, 6].includes(+type))
            currentPlayer = currentPlayer === player1 ? player2 : player1;

        currentPlayer.push(line);
    }

    const player1File = baseFile.concat(player1.join("\n"));
    const player2File = baseFile.concat(player2.join("\n"));

    await Promise.all([
        fs.writeFileSync("player1.osu", player1File),
        fs.writeFileSync("player2.osu", player2File)
    ]);

    console.log("Done");
};

splitMap();