import { Vector2 } from "../Vector2";
import { HitCircle } from "./HitCircle";
import { Slider } from "./Slider";
import { Spinner } from "./Spinner";

/**
 * Base class for all osu! hit objects
 */
export abstract class HitObject {
    Position: Vector2;
    StartTime: number;
    Radius: number;
    StackHeight: number;
    StackedPosition: Vector2;
    StoryboardPosition: Vector2;
    ObjectNumber: number;
    HitSound: number;

    constructor(pos: Vector2, startTime: number, objectNumber: number, radius?: number, hitSound?: number) {
        this.Position = pos;
        this.StartTime = startTime;
        this.Radius = radius;
        this.StoryboardPosition = this.convertToSBPosition(pos);
        this.ObjectNumber = objectNumber;
        this.HitSound = hitSound;
    };

    convertToSBPosition = (pos: Vector2): Vector2 => {
        return new Vector2(pos.x + 64, pos.y + 56);
    };

    calculateStackedPosition(scale: number): void {
        const coordinate: number = this.StackHeight * scale * -6.4;
        const stackOffset: Vector2 = new Vector2(coordinate, coordinate);
        if (this.Position !== undefined)
            this.StackedPosition = this.Position.add(stackOffset);        
    };
};

export function isCircle(object: HitObject): object is HitCircle {
    return !isSlider(object) && !isSpinner(object);
};

export function isSlider(object: HitObject): object is Slider {
    return !!(object as Slider).Path;
};

export function isSpinner(object: HitObject): object is Spinner {
    return !isSlider(object) && !!(object as Spinner).EndTime;
};