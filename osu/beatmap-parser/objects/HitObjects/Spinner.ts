import { HitObject } from "./HitObject";
import { Vector2 } from "../Vector2";

/**
 * Class for spinners
 */
export class Spinner extends HitObject {
    EndTime: number;

    /**
     * 
     * @param startTime The start time of the spinner
     * @param endTime The end time of the spinner
     */
    constructor(pos: Vector2, startTime: number, endTime: number, objectNumber: number) {
        super(pos, startTime, objectNumber);
        this.EndTime = endTime;
    };
};