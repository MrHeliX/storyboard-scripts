import { Storyboard } from "../../../storyboard-helpers";
import { parseBeatmapFromFile } from "../../beatmap-parser/beatmap-parser";
import * as clipboardy from "clipboardy";
import { generateManiaSection } from "./sections/mania";
import { generateIntro } from "./sections/intro";
import { fffffGenerateGrid } from "./sections/grid";
import { fffffGenerateOutro } from "./sections/outro";

const storyboard = new Storyboard();

const generateStoryboard = async () => {
    const filePath = `O:\\osu!\\Songs\\beatmap-637898903711516765-fffff_short\\[] - () (Mr HeliX) [{}].osu`;
    const parsedBeatmap = await parseBeatmapFromFile(filePath);

    generateBlocker();
    generateBackgroundColorEffects();
    generateIntro(storyboard, parsedBeatmap);
    generateManiaSection(storyboard, parsedBeatmap);
    fffffGenerateGrid(storyboard, parsedBeatmap);
    fffffGenerateOutro(storyboard, parsedBeatmap);

    parsedBeatmap.HitObjects.forEach((hitObject, i) => {

    });

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const generateBlocker = () => {
    storyboard
        .loadSprite("sb/square.png")
        .initScale(0, 20)
        .initColor(0, "0,0,0")
        .fadeIn(4645, 6384)
        .fadeOut(36819);
};

const generateBackgroundColorEffects = () => {
    for (let i = 0; i < 10; i++) {
        const x = -100 + 100 * i;
        const time = 35732 + 40 * i;
        storyboard
            .loadSprite("sb/square.png", { x, y: 600 }, "Overlay", "TopCentre")
            .initVector(time, 2, 10)
            .initColor(time, "173, 132, 81")
            .moveYDecreasing(600, 0, time, time + 435)
            .fadeOut(36820);
    }

    storyboard
        .loadSprite("sb/square.png")
        .initScale(63340, 20)
        .initColor(63340, "111, 70, 122")
        .fadeIn(63340)
        .fadeOut(77688);

    // Kapsalons
    storyboard
        .loadSprite("sb/kapsalon.png", { x: 200, y: 100 })
        .initScale(63340, 0.035)
        .fadeIn(63340, 63340, 0, 0.25)
        .moveX(200, 350, 63340, 77688);

    storyboard
        .loadSprite("sb/kapsalon.png", { x: 550, y: 200 })
        .initScale(63340, 0.035)
        .fadeIn(63340, 63340, 0, 0.25)
        .moveX(550, 400, 63340, 77688);

    storyboard
        .loadSprite("sb/kapsalon.png", { x: 100, y: 300 })
        .initScale(63340, 0.035)
        .fadeIn(63340, 63340, 0, 0.25)
        .moveX(100, 250, 63340, 77688);

    // wooz
    let previousX: number = 320;
    [63775, 65514, 67253, 68993, 70732, 72471, 74210].map(t => {
        let newX: number = previousX;
        while (Math.abs(previousX - newX) < 100)
            newX = Math.round(Math.random() * 640)
        storyboard
            .loadSprite("sb/wooz.png", { x: newX, y: 600 })
            .initScale(t, 0.65)
            .fade(t, t, 0, 0.5)
            .moveYDecreasing(600, 400, t - 200, t + 300)
            .moveYIncreasing(400, 600, t + 300, t + 550);

        previousX = newX;
    });

    storyboard
        .loadSprite("sb/square.png")
        .initColor(36819, "94, 111, 196")
        .initScale(36819, 20)
        .fadeIn(36819)
        .moveXDecreasing(320, -680, 63340, 63775)
        .fadeOut(63776);

    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 200 }, "Overlay", "BottomCentre")
        .initColor(36819, "173,132,81")
        .initScale(36819, 20)
        .fadeIn(36819)
        .moveYIncreasing(200, 150, 42036, 42906)
        .moveXDecreasing(320, -680, 63340, 63775)
        .fadeOut(63776);

    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 200 }, "Overlay", "TopCentre")
        .initColor(36819, "173,132,81")
        .initScale(36819, 20)
        .fadeIn(36819)
        .moveYIncreasing(200, 250, 42036, 42906)
        .moveXDecreasing(320, -680, 63340, 63775)
        .fadeOut(63776);

    storyboard
        .loadSprite("sb/circle.png")
        .initColor(77253, "163, 75, 75")
        .scale(77253, 77688, 0, 15, 1)
        .fadeOut(77688);

    storyboard
        .loadSprite("sb/square.png")
        .initScale(89862, 20)
        .initColor(89862, "95, 168, 201")
        .fadeOut(99210);

    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 100 }, "Overlay", "BottomCentre")
        .initVector(77688, 20, 3)
        .initColor(77688, "163, 75, 75")
        .fadeIn(77688)
        .vector(89862, 90188, 20, 3, 0, 3);
    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 240 }, "Overlay", "BottomCentre")
        .initColor(77688, "163, 75, 75")
        .initVector(77688, 20, 3)
        .fadeIn(77688)
        .vector(90297, 90623, 20, 3, 0, 3);
    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 380 }, "Overlay", "BottomCentre")
        .initColor(77688, "163, 75, 75")
        .initVector(77688, 20, 3)
        .fadeIn(77688)
        .vector(90732, 91058, 20, 3, 0, 3);
    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 520 }, "Overlay", "BottomCentre")
        .initColor(77688, "163, 75, 75")
        .initVector(77688, 20, 3)
        .fadeIn(77688)
        .vector(91166, 91493, 20, 3, 0, 3);

    storyboard
        .loadSprite("sb/gf_gimmick_bg.jpg")
        .initScale(109427, 0.8)
        .fadeIn(109427)
        .fadeOut(112268);

    storyboard
        .loadSprite("sb/square.png")
        .initColor(98558, "93, 176, 114")
        .scale(98558, 99210, 0, 20, 2)
        .rotate(98558, 99210, -3, 0, 1)
        .scale(99645, 100080, 20, 0, 1)
        .rotate(99645, 100080, 0, -3, 2);
};

generateStoryboard();