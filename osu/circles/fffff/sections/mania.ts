import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { HitObject } from "../../../beatmap-parser/objects/HitObjects/HitObject";

const start = 35949;
const end = 63775;

const storyboard = new Storyboard();

export const generateManiaSection = async (sb: Storyboard, parsedBeatmap: Beatmap) => {
    drawManiaTiles();

    const filteredHitObjects = parsedBeatmap.HitObjects.filter(h => h.StartTime >= start && h.StartTime <= end);

    filteredHitObjects.forEach(h => {
        if (h.Position.y === 344)
            drawManiaNote(h);
    });

    drawManiaNotesBlocker();

    filteredHitObjects.forEach(h => {
        if (h.Position.y !== 344)
            drawTaikoNote(h);
    });

    drawTaikoSetup();

    sb.append([storyboard]);
};

const drawManiaTiles = () => {
    for (let i = 0; i < 5; i++) {
        const startTick = Math.abs(2 - i) * -1 + 2;
        const startTime = 35080 + 326 * startTick;
        storyboard
            .loadSprite("sb/rounded_square.png", { x: 160 + i * 80, y: 400 })
            .fadeInDecreasing(startTime, startTime + 250)
            .rotate(startTime, startTime + 250, -2, 0, 1)
            .scale(startTime, startTime + 250, 0, 1, 1)
            .fadeOut(63775);
    }
};

const drawTaikoSetup = () => {
    storyboard
        .loadSprite("sb/taiko-hit-circle.png", { x: 80, y: 160 })
        .initScale(42036, 0.65)
        .initColor(42036, "45, 91, 181")
        .fadeInIncreasing(42036, 42688, 0, 1)
        .scale(63340, 63775, 0.65, 0, 1)
        .fadeOut(63775);

    storyboard
        .loadSprite("sb/taiko-hit-circle.png", { x: 80, y: 240 })
        .initScale(42036, 0.65)
        .initColor(42036, "184, 61, 61")
        .fadeInIncreasing(42036, 42688, 0, 1)
        .scale(63340, 63775, 0.65, 0, 1)
        .fadeOut(63775);

    storyboard
        .loadSprite("sb/taiko-hit-circle.png", { x: 130, y: 200 })
        .initScale(42036, 0.4)
        .fadeInIncreasing(42036, 42688, 0, 1)
        .scale(63340, 63775, 0.4, 0, 1)
        .fadeOut(63775);
};

const drawManiaNote = (hitObject: HitObject) => {
    const { StoryboardPosition, StartTime } = hitObject;
    const ARStartTime = getARStartTime(StartTime);

    storyboard
        .loadSprite("sb/square.png", { x: StoryboardPosition.x, y: -30 })
        .initVector(ARStartTime, 1, 0.2)
        .fadeIn(ARStartTime)
        .moveY(-30, 400, ARStartTime, StartTime);
};

const drawTaikoNote = (hitObject: HitObject) => {
    const { Position, StartTime } = hitObject;
    const ARStartTime = StartTime - 1200;

    const color = Position.y === 104 ? "blue" : "red";

    storyboard
        .loadSprite(`sb/taiko-${color}-circle.png`, { x: 800, y: 200 })
        .initScale(ARStartTime, 0.6)
        .fadeIn(ARStartTime)
        .moveX(800, 130, ARStartTime, StartTime);
};

const drawManiaNotesBlocker = () => {
    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 200 })
        .initVector(42906, 20, 2)
        .initColor(42906, "94, 111, 196")
        .fadeIn(42906)
        .fadeOut(62906);
};

const getARStartTime = (startTime: number) => startTime - 800;