import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { HitObject } from "../../../beatmap-parser/objects/HitObjects/HitObject";

const storyboard = new Storyboard();
const start = 63775;
const end = 77580;

const start2 = 77688;
const end2 = 91493

export const fffffGenerateGrid = async (sb: Storyboard, parsedBeatmap: Beatmap) => {
    drawLines();
    drawGrid();

    parsedBeatmap.HitObjects.forEach(h => {
        if (h.StartTime >= start && h.StartTime <= end)
            drawGridNote(h);
        else if (h.StartTime >= start2 && h.StartTime <= end2)
            drawBullet(h);
    });

    drawCircleBulletSources();

    sb.append([storyboard]);
};

const drawGrid = () => {
    const startAnimation = 63340;
    const endAnimation = 63666;

    const emotes = [
        { x: 0, y: 4, emote: "relaxer_emote.jpg" },
        { x: 0, y: 3, emote: "73_emote.jpg" },
        { x: 0, y: 2, emote: "jarig.jpg" },
        { x: 0, y: 1, emote: "anime_emote.jpg" },
        { x: 0, y: 0, emote: "night2.jpg" },
        { x: 4, y: 4, emote: "anime_emote.jpg" },
        { x: 4, y: 3, emote: "night2.jpg" },
        { x: 4, y: 2, emote: "73_emote.jpg" },
        { x: 4, y: 1, emote: "relaxer_emote.jpg" },
        { x: 4, y: 0, emote: "jarig.jpg" }
    ];

    for (let y = 0; y < 5; y++) {
        for (let x = 0; x < 5; x++) {
            storyboard
                .loadSprite("sb/rounded_square.png", { x: 160 + x * 80, y: 400 })
                .fadeInDecreasing(startAnimation, endAnimation)
                .moveYDecreasing(400, 400 - 80 * y, startAnimation, endAnimation);

            if (x === 0 || x === 4) {
                const disappearTimes = [89862, 90297, 91166, 90732, 91166];
                const newX = x === 0 ? -1 : 5;
                storyboard
                    .moveXDecreasing(160 + x * 80, 160 + newX * 80, 75949, 76166)
                    .scale(disappearTimes[4 - y], disappearTimes[4 - y] + 200, 1, 0, 1)
                    .fadeOut(91601);

                const emote = emotes.find(e => e.x === x && e.y === y);
                if (emote) {
                    storyboard
                        .loadSprite(`sb/${emote.emote}`, { x: 160 + newX * 80, y: 400 - 80 * y })
                        .initScale(77253, 0.27)
                        .fadeIn(77253, 77688, 0, 0.5)
                        .scale(disappearTimes[4 - y], disappearTimes[4 - y] + 200, 0.27, 0, 1)
                        .fadeOut(91601);
                }
            }

            if (x > 0 && x < 4) {
                storyboard
                    .scale(76384, 76601, 1, 0, 1)
                    .fadeOut(91601);
            }

            else
                storyboard.fadeOut(91601);
        }
    }
};

const drawGridNote = (hitObject: HitObject) => {
    const { StoryboardPosition, StartTime } = hitObject;
    const ARStartTime = StartTime - 525;

    storyboard
        .loadSprite("sb/rounded_square.png", StoryboardPosition)
        .initColor(ARStartTime, "200,100,0")
        .scale(ARStartTime, StartTime, 0, 1);
};

const drawCircleBulletSources = () => {
    const disappearTimes = [89862, 90297, 91166, 90732, 91166];
    for (let y = 0; y < 5; y++) {
        storyboard
            .loadSprite("sb/circle.png", { x: 320, y: 400 - 80 * y })
            .fadeInDecreasing(76819, 77036)
            .scale(76819, 77036, 0, 0.5, 1)
            .scale(disappearTimes[4 - y], disappearTimes[4 - y] + 200, 0.5, 0, 1)
    }
};

const drawLines = () => {
    const disappearTimes = [89862, 90297, 91166, 90732, 91166];
    for (let y = 0; y < 5; y++) {
        storyboard
            .loadSprite("sb/line.png", { x: 320, y: 400 - 80 * y })
            .initRotate(77253, 0.5 * Math.PI)
            .initVector(77253, 0.5, 0)
            .vector(77253, 77580, 0.5, 0, 0.5, 4.5, 1)
            .vector(disappearTimes[4 - y], disappearTimes[4 - y] + 200, 0.5, 4.5, 0.5, 0, 1);
    }
};

const drawBullet = (hitObject: HitObject) => {
    const { StoryboardPosition, StartTime } = hitObject;
    const ARStartTime = StartTime - 600;

    const newX = StoryboardPosition.x < 320 ? StoryboardPosition.x + 30 : StoryboardPosition.x - 30;

    storyboard
        .loadSprite("sb/circle.png", { x: 320, y: StoryboardPosition.y })
        .initScale(ARStartTime, 0.3)
        .fadeIn(ARStartTime)
        .moveX(320, newX, ARStartTime, StartTime)
};