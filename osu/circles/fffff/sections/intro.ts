import { HitObjectOptions } from "../../../../interfaces";
import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { isCircle, isSlider } from "../../../beatmap-parser/objects/HitObjects/HitObject";
import { generateSbSlider } from "../../../sliders/storyboard-slider-creator";
import { generateSbHitCircle } from "../../storyboard-circle-creator";

const start = 0;
const end = 35948;

const storyboard = new Storyboard();

export const generateIntro = async (sb: Storyboard, parsedBeatmap: Beatmap) => {
    const options: HitObjectOptions = {
        ar: parsedBeatmap.Difficulty.ApproachRate,
        cs: parsedBeatmap.Difficulty.CircleSize
    };

    parsedBeatmap.HitObjects
        .filter(h => h.StartTime >= start && h.StartTime <= end)
        .forEach(h => {
            if (isCircle(h))
                storyboard.append([generateSbHitCircle(h, options)]);
            else if (isSlider(h))
                storyboard.append([generateSbSlider(h, options)]);
        });

    sb.append([storyboard]);
};