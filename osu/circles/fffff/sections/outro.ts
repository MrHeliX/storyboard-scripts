import { HitObjectOptions } from "../../../../interfaces";
import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { HitObject, isSlider } from "../../../beatmap-parser/objects/HitObjects/HitObject";
import { generateSbSlider } from "../../../sliders/storyboard-slider-creator";
import { generateSbHitCircle } from "../../storyboard-circle-creator";

const storyboard = new Storyboard();

const start = 91601;
const end = 98450;

export const fffffGenerateOutro = (sb: Storyboard, parsedBeatmap: Beatmap) => {
    const options: HitObjectOptions = {
        ar: parsedBeatmap.Difficulty.ApproachRate,
        cs: parsedBeatmap.Difficulty.CircleSize
    };

    const interval = end - start;

    parsedBeatmap.HitObjects
        .filter(h => h.StartTime >= start && h.StartTime <= end)
        .forEach(h => {
            const share = h.StartTime - start;
            options.cs = 4 + Math.round((share / interval) * 5);
            storyboard.append([generateSbHitCircle(h, options)]);
        });

    const finalSlider = parsedBeatmap.HitObjects.find(h => h.StartTime > end);
    if (finalSlider && isSlider(finalSlider))
        storyboard.append([generateSbSlider(finalSlider, { ...options, cs: 4 })]);

    sb.append([storyboard]);
};