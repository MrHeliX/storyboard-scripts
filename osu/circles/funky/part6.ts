import { parseHitObjects } from "../../../hitObjectsParser";
import { HitCircle, Position } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import * as clipboardy from "clipboardy";

const storyboard = new Storyboard();

const generateStuff = async () => {
    const hitObjects = (await parseHitObjects(`${__dirname}/part6.txt`)) as HitCircle[];
    const skip = [];

    initialize();
    hitObjects.forEach((hitObject, i) => {
        if (!skip.includes(hitObject.startTime))
            generateObject(hitObject);
    });
    afterObjects();

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const initialize = () => {
    storyboard
        .loadSprite("sb/square.png")
        .initScale(79345, 20)
        .initColor(79345, "31,162,216")
        .fadeIn(79345)
        .fadeOut(92084);

    const times = [91252, 91355, 91458, 91561, 91663].map(t => t + 10);
    [0, 480, 120, 240, 360].forEach((y, i) => {
        const t = times[i];
        storyboard
            .loadSprite("sb/square.png", { x: 800, y }, "Overlay", "CentreRight")
            .initVector(t, 0, 4)
            .initColor(y, "85,88,207")
            .vector(t, t + 411, 0, 4, 20, 4, 1)
            .fadeOut(92084);
    });
};

const generateObject = (hitObject: HitCircle) => {
    const { startTime, sbPosition } = hitObject;
    const startTimeAR = getARStartTime(startTime);

    const finalCircles = [91252, 91355, 91458, 91561, 91663].map(t => t + 10);

    storyboard
        .loadSprite("sb/circle_border.png", sbPosition)
        .fade(startTime - 725, startTimeAR)
        .scale(startTime - 725, startTimeAR, 0.5, 1, 1);

    if (finalCircles.includes(startTime)) {
        const i = finalCircles.findIndex(t => t === startTime);
        const newPosition = { x: 496, y: 72 + (i * 84) };
        storyboard
            .moveDecreasing(sbPosition, newPosition, startTime, startTime + 411)
            .fadeOut(92084);
    }
    else
        storyboard
            .scale(startTime, startTime + 100, 1, 1.1)
            .fade(startTime, startTime + 100, 1, 0);

    storyboard
        .loadSprite("sb/circle.png", sbPosition)
        .fade(startTimeAR, startTimeAR)
        .color(startTimeAR, startTimeAR, "0,0,0", "120,100,255")
        .scale(startTimeAR, startTime, 0.1, 0.85);
};

const afterObjects = () => {
    let times = [78934, 79345];
    [160, 480].forEach((x, i) => {
        storyboard
            .loadSprite("sb/triangle_large.png", { x, y: 600 })
            .initColor(times[i], "85,88,207")
            .initScale(times[i], 0.5)
            .moveYDecreasing(600, 450, times[i], times[i] + 309)
            .moveYIncreasing(450, 600, times[i] + 309, times[i] + 618);
    });

    times = [79746, 79848, 79951, 80054, 80156].map(t => t + 10);
    [70, 155, 240, 325, 410].forEach((y, i) => {
        storyboard
            .loadSprite("sb/square.png", { x: 1100, y })
            .initVector(times[i], 5, 2)
            .initColor(times[i], "85,88,207")
            .moveX(1100, -200, times[i], times[i] + 411)
    });

    storyboard
        .loadSprite("sb/square.png")
        .initColor(80577, "85,88,207")
        .scale(80577, 80988, 0, 4, 1)
        .rotate(80577, 81194, 0, 2)
        .scale(80988, 81194, 4, 0, 1);

    storyboard
        .loadSprite("sb/square.png", { x: -200, y: 240 }, "Overlay", "CentreLeft")
        .initColor(81399, "85,88,207")
        .initVector(81399, 12, 10)
        .moveXDecreasing(-900, -200, 81399, 82016)
        .fadeOutDecreasing(81810, 82016);

    times = [81810, 81710, 81910];
    [160, 320, 480].forEach((x, i) => {
        storyboard
            .loadSprite("sb/circle.png", { x, y: -100 })
            .initScale(times[i], 2)
            .initColor(times[i], "85,88,207")
            .moveYIncreasing(-100, 600, times[i], 82427);
    });

    storyboard
        .loadSprite("sb/square.png", { x: 800, y: 100 })
        .initVector(82221, 8, 20)
        .initColor(82221, "85,88,207")
        .initRotate(82221, 0.9 * Math.PI)
        .moveXDecreasing(800, 600, 82221, 82632)
        .fadeOut(82632, 83043);

    storyboard
        .loadSprite("sb/square.png", { x: 100, y: 1000 })
        .initVector(82632, 20, 20)
        .initColor(82632, "85,88,207")
        .initRotate(82632, 1.1 * Math.PI)
        .moveDecreasing({ x: 0, y: 1000 }, { x: 100, y: 800 }, 82632, 83043)
        .fadeOut(83043, 83454);

    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 600 }, "Overlay", "BottomCentre")
        .initVector(83454, 2, 10)
        .initColor(83454, "85,88,207")
        .initRotate(83454, -0.5 * Math.PI)
        .rotate(83454, 83660, -0.5 * Math.PI, 0.5 * Math.PI)
        .rotate(83660, 83865, 0.5 * Math.PI, -0.5 * Math.PI);

    times = [83865, 84071, 84276, 84482];
    [0, 213, 417, 640].forEach((x, i) => {
        const t = times[i];
        const t2 = times[0] + 822 + (i * 103);
        storyboard
            .loadSprite("sb/square.png", { x, y: -100 })
            .initColor(t, "85,88,207")
            .initScale(t, 4)
            .moveYIncreasing(-100, 650, t, t + 411)
            .rotate(t, t + 411, 0, 4, 2)
            .moveYDecreasing(650, -100, t2, t2 + 206)
            .rotate(t2, t2 + 206, 4, 3, 1);
    });

    storyboard
        .loadSprite("sb/triangle_large.png")
        .initColor(85509, "85,88,207")
        .scale(85509, 85920, 0, 0.5, 1)
        .rotate(85509, 85920, 0, 12, 1)
        .fadeOut(85714, 85920);

    times = [86218, 86321, 86526, 86629, 86835, 86937, 87040, 87143].map(t => t + 10);
    times.forEach((t, i) => {
        const m = i % 4;
        const startX = m < 2 ? 800 : -200;
        const endX = m < 2 ? -200 : 800;
        const y = 480 - (Math.floor(i / 2) * 160);
        storyboard
            .loadSprite("sb/circle.png", { x: startX, y })
            .initScale(t, 2)
            .initColor(t, "85,88,207")
            .moveXIncreasing(startX, endX, t, t + 411);
    });

    storyboard
        .loadSprite("sb/square.png", { x: -200, y: 240 })
        .initVector(87564, 2, 10)
        .initColor(87564, "85,88,207")
        .moveXIncreasing(-200, 1000, 87564, 88180);

    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 600 })
        .initVector(88797, 20, 5)
        .initColor(88797, "85,88,207")
        .moveYDecreasing(600, 550, 88797, 89002)
        .moveYDecreasing(550, 450, 89619, 89824)
        .moveYDecreasing(450, 600, 90235, 90440);

    storyboard
        .loadSprite("sb/square.png", { x: -200, y: 240 })
        .initVector(88797, 1, 10)
        .initColor(88797, "85,88,207")
        .moveXIncreasing(-200, 295, 88797, 89208)
        .fadeOut(89208, 89619)
        .moveXIncreasing(-200, 295, 89619, 90030)
        .fadeIn(89620)
        .fadeOut(90030, 90440);

    storyboard
        .loadSprite("sb/square.png", { x: 840, y: 240 })
        .initVector(88797, 1, 10)
        .initColor(88797, "85,88,207")
        .moveXIncreasing(840, 345, 88797, 89208)
        .fadeOut(89208, 89619)
        .moveXIncreasing(840, 345, 89619, 90030)
        .fadeIn(89620)
        .fadeOut(90030, 90440);

    storyboard
        .loadSprite("sb/square.png", { x: 320, y: -120 })
        .initVector(88797, 20, 5)
        .initColor(88797, "85,88,207")
        .moveYDecreasing(-120, -70, 88797, 89002)
        .moveYDecreasing(-70, 30, 89619, 89824)
        .moveYDecreasing(30, -120, 90440, 90646);

    storyboard
        .loadSprite("sb/square", { x: 840, y: 600 })
        .initVector(90440, 20, 2)
        .initColor(90440, "85,88,207")
        .initRotate(90440, 0.25 * -Math.PI)
        .moveIncreasing({ x: 840, y: 600 }, { x: -200, y: -120 }, 90440, 91057);
};

const getARStartTime = (startTime: number) => startTime - 550;

generateStuff();