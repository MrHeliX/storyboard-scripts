import { parseHitObjects } from "../../../hitObjectsParser";
import { HitCircle, Position } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import * as clipboardy from "clipboardy";

const storyboard = new Storyboard();

const generateStuff = async () => {
    const hitObjects = (await parseHitObjects(`${__dirname}/part4.txt`)) as HitCircle[];
    const skip = [57965, 58376, 58787, 59198, 59609, 64540, 64951, 65362].map(t => t + 10);

    initialize();
    hitObjects.forEach((hitObject, i) => {
        if (!skip.includes(hitObject.startTime))
            generateObject(hitObject);
    });
    afterObjects();

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const grid: { x: number, y: number, events: { type: "on" | "off", time: number, marked?: boolean, markEnd?: number }[] }[] = [
    { x: -4, y: -2, events: [{ type: "on", time: 59198 }, { type: "off", time: 60841 }] },
    { x: -4, y: -1, events: [{ type: "on", time: 59198 }, { type: "off", time: 60841 }] },
    { x: -4, y: 0, events: [{ type: "on", time: 59198 }, { type: "off", time: 64129 }] },
    { x: -4, y: 1, events: [{ type: "on", time: 57554, marked: true, markEnd: 59198 }, { type: "off", time: 60841 }] },
    { x: -4, y: 2, events: [{ type: "on", time: 57554 }, { type: "off", time: 60841 }] },

    { x: -3, y: -2, events: [{ type: "on", time: 54677 }, { type: "off", time: 55910 }, { type: "on", time: 59198 }, { type: "off", time: 60841 }, { type: "on", time: 64951, marked: true }, { type: "off", time: 65362 }] },
    { x: -3, y: -1, events: [{ type: "on", time: 54266 }, { type: "off", time: 55910 }, { type: "on", time: 59198 }, { type: "off", time: 64129 }, { type: "on", time: 64951 }, { type: "off", time: 65362 }] },
    { x: -3, y: 0, events: [{ type: "on", time: 54266 }, { type: "off", time: 55910 }, { type: "on", time: 59198 }, { type: "off", time: 64129 }, { type: "on", time: 64951 }, { type: "off", time: 65362 }] },
    { x: -3, y: 1, events: [{ type: "on", time: 54266 }, { type: "off", time: 55910 }, { type: "on", time: 57554 }, { type: "off", time: 64129 }, { type: "on", time: 64951 }, { type: "off", time: 65362 }] },
    { x: -3, y: 2, events: [{ type: "on", time: 54266 }, { type: "off", time: 54677 }, { type: "on", time: 57554 }, { type: "off", time: 60841 }, { type: "on", time: 64951 }, { type: "off", time: 65362 }] },

    { x: -2, y: -2, events: [{ type: "on", time: 54677 }, { type: "off", time: 55910 }, { type: "on", time: 59198, marked: true, markEnd: 59609 }, { type: "off", time: 64129 }, { type: "on", time: 64540 }, { type: "off", time: 64951 }] },
    { x: -2, y: -1, events: [{ type: "on", time: 54266 }, { type: "off", time: 55910 }, { type: "on", time: 59198 }, { type: "off", time: 62896 }, { type: "on", time: 64540 }, { type: "off", time: 64951 }] },
    { x: -2, y: 0, events: [{ type: "on", time: 54266 }, { type: "off", time: 55910 }, { type: "on", time: 59198 }, { type: "off", time: 64129 }, { type: "on", time: 64540 }, { type: "off", time: 64951 }] },
    { x: -2, y: 1, events: [{ type: "on", time: 54266 }, { type: "off", time: 55910 }, { type: "on", time: 57965 }, { type: "off", time: 62896 }, { type: "on", time: 64540 }, { type: "off", time: 64951 }] },
    { x: -2, y: 2, events: [{ type: "on", time: 54266 }, { type: "off", time: 54677 }, { type: "on", time: 57965 }, { type: "off", time: 64129 }, { type: "on", time: 64540 }, { type: "off", time: 64951 }] },

    { x: -1, y: -2, events: [{ type: "on", time: 54677 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 64540 }] },
    { x: -1, y: -1, events: [{ type: "on", time: 50876 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 62896 }, { type: "on", time: 64129 }, { type: "off", time: 64540 }] },
    { x: -1, y: 0, events: [{ type: "on", time: 50978 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 64540 }] },
    { x: -1, y: 1, events: [{ type: "on", time: 51081 }, { type: "off", time: 57554 }, { type: "on", time: 57965 }, { type: "off", time: 62896 }, { type: "on", time: 64129, marked: true }, { type: "off", time: 64540 }] },
    { x: -1, y: 2, events: [{ type: "on", time: 52622 }, { type: "off", time: 54677 }, { type: "on", time: 56526 }, { type: "off", time: 57554 }, { type: "on", time: 57965, marked: true, markEnd: 59198 }, { type: "off", time: 64540 }] },
    { x: -1, y: 3, events: [{ type: "on", time: 56526 }, { type: "off", time: 57554 }] },

    { x: 0, y: -2, events: [{ type: "on", time: 54677 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 64129 }] },
    { x: 0, y: -1, events: [{ type: "on", time: 51184 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 62896 }] },
    { x: 0, y: 0, events: [{ type: "on", time: 51287 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 61252 }, { type: "on", time: 62485 }, { type: "off", time: 64129 }] },
    { x: 0, y: 1, events: [{ type: "on", time: 51389 }, { type: "off", time: 57554 }, { type: "on", time: 58376 }, { type: "off", time: 62896 }] },
    { x: 0, y: 2, events: [{ type: "on", time: 52622 }, { type: "off", time: 54677 }, { type: "on", time: 56526 }, { type: "off", time: 57554 }, { type: "on", time: 58376, marked: true, markEnd: 59198 }, { type: "off", time: 64129 }] },
    { x: 0, y: 3, events: [{ type: "on", time: 56526 }, { type: "off", time: 57554 }] },

    { x: 1, y: -2, events: [{ type: "on", time: 54677 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 64540 }] },
    { x: 1, y: -1, events: [{ type: "on", time: 51595 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 61252 }, { type: "on", time: 64129 }, { type: "off", time: 64540 }] },
    { x: 1, y: 0, events: [{ type: "on", time: 51800 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 61252 }, { type: "on", time: 62485 }, { type: "off", time: 64540 }] },
    { x: 1, y: 1, events: [{ type: "on", time: 52006 }, { type: "off", time: 57554 }, { type: "on", time: 58376 }, { type: "off", time: 61252 }, { type: "on", time: 64129 }, { type: "off", time: 64540 }] },
    { x: 1, y: 2, events: [{ type: "on", time: 52622 }, { type: "off", time: 54677 }, { type: "on", time: 56526 }, { type: "off", time: 57554 }, { type: "on", time: 58376 }, { type: "off", time: 64540 }] },
    { x: 1, y: 3, events: [{ type: "on", time: 56526 }, { type: "off", time: 57554 }] },

    { x: 2, y: -2, events: [{ type: "on", time: 56321 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 61252 }, { type: "on", time: 64540 }, { type: "off", time: 64951 }] },
    { x: 2, y: -1, events: [{ type: "on", time: 56321 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 61252 }, { type: "on", time: 63102 }, { type: "off", time: 64129 }, { type: "on", time: 64540, marked: true }, { type: "off", time: 64951 }] },
    { x: 2, y: 0, events: [{ type: "on", time: 56321 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 61252 }, { type: "on", time: 62485 }, { type: "off", time: 64129 }, { type: "on", time: 64540 }, { type: "off", time: 64951 }] },
    { x: 2, y: 1, events: [{ type: "on", time: 56321 }, { type: "off", time: 57554 }, { type: "on", time: 58787, marked: true, markEnd: 59198 }, { type: "off", time: 61252 }, { type: "on", time: 63102 }, { type: "off", time: 64129 }, { type: "on", time: 64540 }, { type: "off", time: 64951 }] },
    { x: 2, y: 2, events: [{ type: "on", time: 56321 }, { type: "off", time: 57554 }, { type: "on", time: 58787 }, { type: "off", time: 61252 }, { type: "on", time: 64540 }, { type: "off", time: 64951 }] },

    { x: 3, y: -2, events: [{ type: "on", time: 56321 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 61252 }, { type: "on", time: 64951 }, { type: "off", time: 65362 }] },
    { x: 3, y: -1, events: [{ type: "on", time: 56321 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 61252 }, { type: "on", time: 64951 }, { type: "off", time: 65362 }] },
    { x: 3, y: 0, events: [{ type: "on", time: 56321 }, { type: "off", time: 57554 }, { type: "on", time: 59198 }, { type: "off", time: 61252 }, { type: "on", time: 62485 }, { type: "off", time: 64129 }, { type: "on", time: 64951 }, { type: "off", time: 65362 }] },
    { x: 3, y: 1, events: [{ type: "on", time: 56321 }, { type: "off", time: 57554 }, { type: "on", time: 58787 }, { type: "off", time: 61252 }, { type: "on", time: 64951 }, { type: "off", time: 65362 }] },
    { x: 3, y: 2, events: [{ type: "on", time: 56321 }, { type: "off", time: 57554 }, { type: "on", time: 58787 }, { type: "off", time: 61252 }, { type: "on", time: 64951 }, { type: "off", time: 65362 }] },
];

const initialize = () => {
    storyboard
        .loadSprite("sb/square.png")
        .initScale(65372, 20)
        .initColor(65372, "0,0,0")
        .fadeIn(65372)
        .fadeOut(65783);

    for (let x = -200; x <= 840; x += 50) {
        for (let y = -100; y <= 580; y += 50) {
            storyboard
                .loadSprite("sb/square.png", { x, y })
                .initColor(65372, "81,199,117")
                .fadeIn(65372)
                .scale(65372, 65783, 1, 0, 1)
                .rotate(65372, 65783, 0, 2, 1)
                .fadeOut(65783);
        }
    };

    grid.forEach(g => {
        storyboard.loadSprite("sb/rounded_square.png", { x: 320 + (g.x * 64), y: 240 + (g.y * 64) });
        g.events.forEach(e => {
            const time = e.time + 10;
            if (e.type === "on")
                storyboard
                    .fadeInDecreasing(time, time + 411)
                    .scale(time, time + 411, 0, 1, 1)
                    .rotate(time, time + 411, -2, 0, 1);
            else
                storyboard
                    .fadeOutDecreasing(time, time + 411)
                    .scale(time, time + 411, 1, 0, 1)
                    .rotate(time, time + 411, 0, -2, 1);

            if (e.marked)
                storyboard.initColor(time, "212,87,87", true);
            if (e.markEnd)
                storyboard.color(e.markEnd, e.markEnd + 206, "212,87,87", "255,255,255");
        });

        if (g.events[g.events.length - 1].type === "on")
            storyboard.fadeOut(65773);
    });
};

const generateObject = (hitObject: HitCircle) => {
    const { startTime, sbPosition } = hitObject;
    const startTimeAR = getARStartTime(startTime);

    storyboard
        .loadSprite("sb/rounded_square.png", sbPosition)
        .initColor(startTimeAR, "212,87,87")
        .fadeIn(startTimeAR, startTimeAR + 200)
        .scale(startTimeAR, startTime, 0, 1);
};

const afterObjects = () => {

};

const getARStartTime = (startTime: number) => startTime - 525;

generateStuff();