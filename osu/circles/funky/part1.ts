import { parseHitObjects } from "../../../hitObjectsParser";
import { HitCircle, Position } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import * as clipboardy from "clipboardy";

const storyboard = new Storyboard();

const generateStuff = async () => {
    const hitObjects = (await parseHitObjects(`${__dirname}/part1.txt`)) as HitCircle[];

    hitObjects.forEach(hitObject => {
        generatePart1(hitObject);
    });

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const generatePart1 = (hitObject: HitCircle) => {
    const { startTime, sbPosition } = hitObject;
    const startTimeAR = getARStartTime(startTime);
    const endTimeBorder = hitObject.startTime >= 24687 ? 25090 : startTime + 100;

    storyboard
        .loadSprite("sb/circle_border.png", sbPosition)
        .fade(startTime - 725, startTimeAR)
        .scale(startTime - 725, startTimeAR, 0.5, 1, 1);

    if (endTimeBorder < 24687)
        storyboard
            .scale(startTime, startTime + 100, 1, 1.1)
            .fadeOut(startTime, startTime + 100);
    else
        storyboard.fadeOut(25920);

    storyboard
        .loadSprite("sb/circle.png", sbPosition)
        .fade(startTimeAR, startTimeAR)
        .color(startTimeAR, startTimeAR, "0,0,0", "120,100,255")
        .scale(startTimeAR, startTime, 0.1, 0.85);
};

const getARStartTime = (startTime: number) => startTime - 525;

generateStuff();