import { parseHitObjects } from "../../../hitObjectsParser";
import { HitCircle, Position } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import * as clipboardy from "clipboardy";

const storyboard = new Storyboard();

const generateStuff = async () => {
    const hitObjects = (await parseHitObjects(`${__dirname}/part5.txt`)) as HitCircle[];
    const skip = [];

    initialize();
    hitObjects.forEach((hitObject, i) => {
        if (!skip.includes(hitObject.startTime))
            generateObject(hitObject);
    });
    afterObjects();

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const initialize = () => {
    storyboard
        .loadSprite("sb/square.png")
        .initScale(78523, 20)
        .initColor(78523, "0,0,0")
        .fadeIn(78523)
        .fadeOut(79345);

    storyboard
        .loadSprite("170.jpg")
        .initScale(78513, 0.8)
        .fade(78513, 78513, 1, 0.08)
        .fadeOut(79355);

    storyboard
        .loadSprite("sb/square.png")
        .initScale(65783, 20)
        .fadeIn(65783)
        .color(65783, 77084, "0,0,0", "216,144,31", 2)
        .scale(78317, 78934, 20, 1, 8)
        .rotate(78317, 79242, 0, Math.PI, 2)
        .color(78317, 79242, "216,144,31", "31,162,216", 2)
        .scale(78934, 79242, 1, 20, 8)
        .fadeOut(79345);

    // Triangle is 90x79
    const stayTillEnd = [{ x: 0, y: 0 }, { x: -1, y: 0 }, { x: 1, y: 0 }, { x: 0, y: 1 }];
    [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5].forEach(column => {
        [-4, -3, -2, -1, 0, 1, 2, 3, 4].forEach(row => {
            const rotated = (column + row) % 2 !== 0;
            const initRotate = rotated ? Math.PI - 1 : -1;
            const endRotate = rotated ? Math.PI : 0;
            const endTime = stayTillEnd.some(t => t.x === column && t.y === row) ? 78523 : 77084;

            if (row === 0 && column === 0)
                storyboard
                    .loadSprite("sb/triangle.png")
                    .fadeInDecreasing(64961, 65372)
                    .scale(64961, 65372, 0, 1, 1)
                    .rotate(64961, 65372, initRotate, endRotate, 1);
            else
                storyboard
                    .loadSprite("sb/triangle.png", { x: 320 + (column * 56), y: 240 + (row * 90) })
                    .fadeInDecreasing(65372, 65783)
                    .scale(65372, 65783, 0, 1, 1)
                    .rotate(65372, 65783, initRotate, endRotate, 1);

            storyboard
                .scale(endTime, endTime + 206, 1, 0, 1);
        });
    });
};

const generateObject = (hitObject: HitCircle) => {
    const { startTime } = hitObject;
    const startTimeAR = getARStartTime(startTime);
    const trianglePosition = calculateTrianglePosition(hitObject);
    const shouldRotate = isRotated(hitObject);

    storyboard
        .loadSprite("sb/triangle.png", trianglePosition, "Overlay", "BottomCentre")
        .initColor(startTimeAR, "31,105,49")
        .fadeIn(startTimeAR)
        .scale(startTimeAR, startTime, 0, 1, 1);

    if (startTime !== 78523)
        storyboard
            .fadeOut(startTime, startTime + 200);

    if (shouldRotate)
        storyboard.initRotate(startTimeAR, Math.PI);
};

const calculateTrianglePosition = (hitObject: HitCircle) => {
    const column = Math.ceil((hitObject.position.y + 48 - 240) / 90);
    return { x: hitObject.position.x + 64, y: 240 + (column * 90) + (isRotated(hitObject) ? -39.5 : 39.5) };
};

const afterObjects = () => {

};

const getARStartTime = (startTime: number) => startTime - 550 + 10;

const isRotated = (hitObject: HitCircle): boolean => {
    const row = (hitObject.position.x + 64 - 320) / 56;
    const column = (hitObject.position.y + 48 - 240) / 90;
    return (row + column) % 2 !== 0;
};

generateStuff();