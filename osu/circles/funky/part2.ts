import { parseHitObjects } from "../../../hitObjectsParser";
import { HitCircle, Position } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import * as clipboardy from "clipboardy";

const storyboard = new Storyboard();

const generateStuff = async () => {
    const hitObjects = (await parseHitObjects(`${__dirname}/part2.txt`)) as HitCircle[];

    initializePart2();
    hitObjects.forEach(hitObject => {
        if (hitObject.startTime < 32495)
            generatePart2(hitObject);
        else
            generatePart2B(hitObject);
    });

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

// PART 2
const topDownMovements = {
    0: [31663, 32074].map(t => t + 10),
    1: [28581, 28889, 29609, 31663, 32074].map(t => t + 10),
    2: [28376, 28992, 32074].map(t => t + 10),
    3: [27554, 29095, 29609, 31663, 32074].map(t => t + 10),
    4: [27759, 29198, 31663, 32074].map(t => t + 10)
};

const secondPartRotations: { [key: number]: { time: number, direction: "left" | "right" }[] } = {
    0: [{ time: 34961, direction: "left" }, { time: 35166, direction: "left" }, { time: 35475, direction: "left" }, { time: 38660, direction: "right" }],
    1: [{ time: 35166, direction: "left" }, { time: 35577, direction: "left" }, { time: 37016, direction: "right" }, { time: 38249, direction: "left" }],
    2: [{ time: 35680, direction: "left" }, { time: 37016, direction: "right" }],
    3: [{ time: 34345, direction: "right" }, { time: 35783, direction: "left" }, { time: 36194, direction: "right" }, { time: 38660, direction: "right" }],
    4: [{ time: 34139, direction: "right" }, { time: 34345, direction: "right" }, , { time: 35988, direction: "left" }, { time: 36194, direction: "right" }, { time: 38249, direction: "left" }, { time: 38660, direction: "right" }]
};

const initializePart2 = () => {
    [64.5, 192.5, 320.5, 448.5, 576.5].forEach((x, i) => {
        const movements = topDownMovements[i];
        let currentY = 119.5;

        storyboard
            .loadSprite("sb/square.png", { x, y: 119.5 })
            .fade(25920, 25920)
            .scale(25920, 26331, 0, 0.5, 1)
            .rotate(25920, 26331, 2.5, Math.PI / 4);
        movements.forEach(m => {
            const nextY = currentY === 119.5 ? 360.5 : 119.5;
            storyboard.move(m, m + 205, { x, y: currentY }, { x, y: nextY }, 1);
            currentY = nextY;
        });

        if (i === 1 || i === 3)
            storyboard
                .moveDecreasing({ x, y: currentY }, { x: 321.0818, y: 368.0636 }, 32495, 32906)
                .fadeOut(32906);
        else if (i === 0)
            storyboard
                .moveXDecreasing(x, -200, 32495, 32906)
                .fadeOut(32495, 32906);
        else if (i === 4)
            storyboard
                .moveXDecreasing(x, 840, 32495, 32906)
                .fadeOut(32495, 32906);
        else
            storyboard
                .moveDecreasing({ x, y: currentY }, { x: 321.0818, y: 368.0636 }, 32495, 32906)
                .fadeOut(32906);

        // Circles
        storyboard
            .loadSprite("sb/circle_border.png", { x, y: 216.5 })
            .move(25920, 26301, { x, y: 216.5 }, { x, y: 400.5 }, 1);

        currentY = 400.5;
        movements.forEach(m => {
            const nextY = currentY === 79.5 ? 400.5 : 79.5;
            storyboard.move(m, m + 205, { x, y: currentY }, { x, y: nextY }, 1);
            currentY = nextY;
        });

        if (i === 1 || i === 3)
            storyboard
                .moveXDecreasing(x, 320.5, 32495, 32906)
                .fadeOut(32906);
        else if (i === 0)
            storyboard
                .moveXDecreasing(x, -200, 32495, 32906)
                .fadeOut(32495, 32906);
        else if (i === 4)
            storyboard
                .moveXDecreasing(x, 840, 32495, 32906)
                .fadeOut(32495, 32906);
        else
            storyboard.fadeOut(32906);

        let currentRotation = 0;
        const rotations = secondPartRotations[i];
        if (rotations.length || i === 2) {
            storyboard
                .loadSprite("sb/lolly.png", { x: 321.0818, y: 368.0636 }, "Overlay", "BottomCentre");

            if (i === 2)
                storyboard.fadeIn(32495, 32906);
            else
                storyboard.fadeIn(rotations[0].time);

            rotations.forEach(r => {
                const { time, direction } = r;
                const nextRotation = direction === "left" ? currentRotation - 1 : currentRotation + 1;
                storyboard
                    .rotate(time, time + 205, currentRotation * Math.PI * 0.15, nextRotation * Math.PI * 0.15, 1);
                currentRotation = nextRotation;
            });
        };

        storyboard
            .scale(39070, 39482, 1, 0, 1);
    });

    storyboard
        .loadSprite("sb/square.png", { x: 321.0818, y: 368.0636 })
        .scale(32906, 32906, 0, 0.5, 1)
        .rotate(32906, 32906, 2.5, Math.PI / 4)
        .fadeIn(32906)
        .fadeOut(39071);

    storyboard
        .loadSprite("sb/bingv.png", { x: 321.0818, y: 368.0636 })
        .scale(32906, 32906, 0, 0.25, 1)
        .rotate(32906, 32906, 2.5, Math.PI / 4)
        .fade(32906, 33112, 0, 0.15)
        .fade(39071, 39071, 0.15, 0);
};

const generatePart2 = (hitObject: HitCircle) => {
    const { startTime, sbPosition } = hitObject;
    const startTimeAR = getARStartTime(startTime);
    const squarePosition = { ...sbPosition, y: sbPosition.y > 320 ? 119.5 : 320.5 };
    const endPosition = { ...sbPosition, y: sbPosition.y > 320 ? sbPosition.y - 40 : sbPosition.y + 40 }

    storyboard
        .loadSprite("sb/circle.png", squarePosition)
        .fade(startTimeAR, startTimeAR)
        .scale(startTimeAR, startTimeAR, 0.3, 0.3)
        .move(startTimeAR, startTime, squarePosition, endPosition);
};

const getEndPosition2B = (hitObject: HitCircle): Position => {
    switch (hitObject.sbPosition.x) {
        case 88.5:
            return { x: 120.6581, y: 223.8433 };
        case 190.5:
            return { x: 206.3205, y: 148.7783 };
        case 320.5:
            return { x: 320.5, y: 119.5 };
        case 452.5:
            return { x: 434.6795, y: 148.7783 };
        case 554.5:
            return { x: 520.3419, y: 223.8433 };
        default: return hitObject.sbPosition;
    };
};

const generatePart2B = (hitObject: HitCircle) => {
    const { startTime, sbPosition } = hitObject;
    const startTimeAR = getARStartTime(startTime);
    const squarePosition = { x: 321.0818, y: 368.0636 };
    const endPosition = getEndPosition2B(hitObject);

    storyboard
        .loadSprite("sb/circle.png", squarePosition)
        .fadeIn(startTimeAR)
        .scale(startTimeAR, startTimeAR, 0.3, 0.3)
        .move(startTimeAR, startTime, squarePosition, endPosition);
};















// PART 3

const initializePart3 = () => {
    const squarePositions = {
        0: { x: 160, y: 120 },
        1: { x: 480, y: 120 },
        2: { x: 160, y: 360 },
        3: { x: 480, y: 360 }
    };

    const linePositions = {
        0: { x: 320, y: 120 },
        1: { x: 160, y: 240 },
        2: { x: 320, y: 360 },
        3: { x: 480, y: 240 }
    };

    [0, 1, 2, 3].forEach(id => {
        const position = linePositions[id];
        storyboard
            .loadSprite("sb/line.png", position)
            .color(32485, 32485, "0,0,0", "78,105,160")
            .vector(32485, 32896, 1, 1, 1, 3)
            .fade(32485, 32485);

        if (id % 2 === 0)
            storyboard.rotate(32485, 32485, 0, Math.PI / 2)
        storyboard.fade(39442, 39442, 1, 0)
    });

    [0, 1, 2, 3].forEach(id => {
        const position = squarePositions[id];
        storyboard
            .loadSprite("sb/square.png", position)
            .color(32485, 32485, "0,0,0", "78,105,160")
            .fade(32485, 32485)
            .scale(32485, 32896, 0, 1.2, 1)
            .rotate(32485, 32896, -2, 0, 1)
            .fade(39442, 39442, 1, 0)
    });
};

const getARStartTime = (startTime: number) => startTime - 525;

generateStuff();