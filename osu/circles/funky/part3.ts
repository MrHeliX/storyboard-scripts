import { parseHitObjects } from "../../../hitObjectsParser";
import { HitCircle, Position } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import * as clipboardy from "clipboardy";

const storyboard = new Storyboard();

const generateStuff = async () => {
    const hitObjects = (await parseHitObjects(`${__dirname}/part3.txt`)) as HitCircle[];

    initialize();
    hitObjects.forEach((hitObject, i) => {
        if (i === 0) return;
        if (hitObject.startTime <= 42769)
            generateObject(hitObject);
        else if (hitObject.startTime <= 46057)
            generateObjectB(hitObject);
        else
            generateObjectC(hitObject);
    });
    afterObjects();

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const initialize = () => {
    // PART A
    changes.forEach(t => {
        const nextColor = getNextColor();
        storyboard
            .loadSprite("sb/square.png", { x: -460, y: 240 })
            .color(t - 411, t - 411, "0,0,0", currentColor)
            .vector(t - 411, t - 411, 0, 0, 10, 10)
            .fadeIn(t - 411)
            .moveXDecreasing(-460, 100, t, t + 500)
            .fadeOut(t + 812);

        storyboard
            .loadSprite("sb/square.png", { x: 1100, y: 240 })
            .color(t - 411, t - 411, "0,0,0", currentColor)
            .vector(t - 411, t - 411, 0, 0, 10, 10)
            .fadeIn(t - 411)
            .moveXDecreasing(1100, 560, t, t + 500)
            .fadeOut(t + 812);

        currentColor = nextColor;
    });

    // PART B
    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 240 })
        .scale(42769, 42769, 0, 20)
        .color(42769, 42769, "0,0,0", "0,0,0")
        .fadeOut(46262);

    [0, 1].forEach(id => {
        const x = id === 0 ? 100 : 560;
        const nextX = id === 0 ? -50 : 710;
        storyboard
            .loadSprite("sb/square.png", { x, y: 240 })
            .color(42358, 42358, "0,0,0", colors[colors.length - 2])
            .vector(42358, 42358, 0, 0, 10.5, 10)
            .fadeIn(42358)
            .moveX(x, nextX, 42769, 43077, 1);

        changesB.forEach((t, i) => {
            if (i % 2 === 0)
                storyboard.moveXDecreasing(i === 0 ? nextX : nextX + 150, nextX - 150, t, t + 411);
            else
                storyboard.moveXDecreasing(nextX - 150, nextX + 150, t, t + 411);
        });

        storyboard
            .vector(46057, 46262, 10.5, 10.5, 16, 16)
            .color(46057, 46262, colors[colors.length - 2], "185,117,76");
        storyboard.fadeOut(50988);
    });

    currentColor = colors[1];

    storyboard
        .loadSprite("sb/square.png", { x: 321.0818, y: 368.0636 })
        .fadeIn(39071)
        .scale(39071, 39482, 0.5, 1.2, 1)
        .rotate(39071, 39482, Math.PI / 4, -2 * Math.PI, 1)
        .color(39071, 39482, "255,255,255", colors[0])
        .fadeOut(39893);
};

const colors = ["1,29,69", "2,44,104", "4,74,173", "6,104,242", "71,144,249", "106,165,250", "140,186,251", "191,216,250"];
let currentColor = colors[0];
const changes = [39472, 39883, 40293, 40704, 41115, 41526, 41937].map(t => t + 10);
const changesB = [43170, 43581, 43992, 44403, 44814, 45225, 45636].map(t => t + 10);
const changesC = [46047, 46458, 46869, 47280, 47691, 48102, 48513, 48924, 49335, 49540, 49746, 49951, 50156, 50259, 50362, 50465, 50567, 50670, 50773, 50876].map(t => t + 10);

const generateObject = (hitObject: HitCircle) => {
    const { startTime, sbPosition } = hitObject;
    const startTimeAR = getARStartTime(startTime);

    storyboard
        .loadSprite("sb/square.png", sbPosition)
        .scale(startTimeAR - 200, startTimeAR - 200, 0, 1.2)
        .color(startTimeAR - 200, startTimeAR - 200, "0,0,0", currentColor)
        .fadeIn(startTimeAR - 200, startTimeAR)
        .fadeOut(startTime);

    storyboard
        .loadSprite("sb/square_border.png", sbPosition)
        .color(startTimeAR, startTimeAR, "0,0,0", currentColor)
        .fadeIn(startTimeAR, startTimeAR + 200)
        .scale(startTimeAR, startTime, 3.5, 1.2);

    currentColor = getNextColor();
};

const generateObjectB = (hitObject: HitCircle) => {
    const { startTime, sbPosition } = hitObject;
    const startTimeAR = getARStartTime(startTime);

    storyboard
        .loadSprite("sb/square.png", sbPosition)
        .scale(startTimeAR - 200, startTimeAR - 200, 0, 1.2)
        .color(startTimeAR - 200, startTimeAR - 200, "0,0,0", colors[colors.length - 2])
        .fadeIn(startTimeAR - 200, startTimeAR)
        .fadeOut(startTime);

    storyboard
        .loadSprite("sb/square_border.png", sbPosition)
        .color(startTimeAR, startTimeAR, "0,0,0", colors[colors.length - 2])
        .fadeIn(startTimeAR, startTimeAR + 200)
        .scale(startTimeAR, startTime, 3.5, 1.2);
};

const generateObjectC = (hitObject: HitCircle) => {
    const { startTime, position, sbPosition } = hitObject;
    const startTimeAR = getARStartTime(startTime);

    const color = position.x > 344 ? colors[0] : (position.x > 168 ? colors[1] : colors[2]);

    storyboard
        .loadSprite("sb/square.png", sbPosition)
        .scale(startTimeAR - 200, startTimeAR - 200, 0, 1.2)
        .color(startTimeAR - 200, startTimeAR - 200, "0,0,0", color)
        .fadeIn(startTimeAR - 200, startTimeAR)
        .fadeOut(startTime);

    storyboard
        .loadSprite("sb/square_border.png", sbPosition)
        .color(startTimeAR, startTimeAR, "0,0,0", color)
        .fadeIn(startTimeAR, startTimeAR + 200)
        .scale(startTimeAR, startTime, 3.5, 1.2);
};

const afterObjects = () => {
    // PART C
    storyboard
        .loadSprite("sb/square.png")
        .vector(52221, 52221, 0, 0, 20, 10)
        .color(52221, 52221, "0,0,0", "81,199,117")
        .fadeOut(65372);

    [0, 1, 2].map(id => {
        const x = 640 - (320 * id);
        const scaleX = id === 1 ? 5 : 8;
        storyboard
            .loadSprite("sb/square.png", { x, y: 0 }, "Overlay", "BottomCentre")
            .fadeIn(46057)
            .color(46057, 46057, "0,0,0", colors[id])
            .vector(46057, 46057, 0, 0, scaleX, 10)
            .moveYDecreasing(480, 0, 52221, 52632)
            .fadeOut(52632);

        changesC.forEach((c, i) => {
            const nextTime = changesC[i + 1];
            const m = i % 3;
            if (m === id)
                storyboard.moveYIncreasing(0, 480, c, nextTime ?? c + 103);
            if (m === (id + 2) % 3 && i > 1 && i !== changesC.length - 1)
                storyboard.moveYIncreasing(480, 0, c, nextTime ?? c + 103);
        });
    });
};

const getNextColor = () => {
    const i = colors.findIndex(c => c === currentColor);
    if (i === colors.length - 1)
        return colors[0];
    return colors[i + 1];
};

const getARStartTime = (startTime: number) => startTime - 525;

generateStuff();