import { parseHitObjects } from "../../../hitObjectsParser";
import { HitCircle, Position } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import * as clipboardy from "clipboardy";

const storyboard = new Storyboard();

const generateStuff = async () => {
    const hitObjects = (await parseHitObjects(`${__dirname}/part7.txt`)) as HitCircle[];
    const skip = [];

    initialize();
    hitObjects.forEach((hitObject, i) => {
        if (!skip.includes(hitObject.startTime))
            generateObject(hitObject);
    });
    afterObjects();

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const leftRightMovements = {
    0: [93307, 94335, 95362, 97417, 97828].map(t => t + 10),
    1: [93513, 94951, 97417].map(t => t + 10),
    2: [94848].map(t => t + 10),
    3: [93513, 94746, 97417].map(t => t + 10),
    4: [93307, 94129, 94643, 97417, 97828].map(t => t + 10)
};

const secondPartRotations: { [key: number]: { time: number, direction: "left" | "right", big?: boolean }[] } = {
    0: [{ time: 101526, direction: "right" }, { time: 101937, direction: "left" }, { time: 102759, direction: "right", big: true }, { time: 104403, direction: "left", big: true }],
    1: [{ time: 100910, direction: "right" }, { time: 101424, direction: "right" }, { time: 101937, direction: "left" }, { time: 103992, direction: "left" }, { time: 104403, direction: "right" }],
    2: [{ time: 100088, direction: "right", big: true }, { time: 101321, direction: "right" }, { time: 101732, direction: "left" }],
    3: [{ time: 99883, direction: "right", big: true }, { time: 100088, direction: "right", big: true }, { time: 100704, direction: "left" }, { time: 101218, direction: "right" }, { time: 101732, direction: "left" }, { time: 103992, direction: "right" }, { time: 104403, direction: "left" }],
    4: [{ time: 99883, direction: "right", big: true }, { time: 100088, direction: "right", big: true }, { time: 101732, direction: "left" }, { time: 101937, direction: "right" }, { time: 102759, direction: "left", big: true }, { time: 104403, direction: "right", big: true }]
};

const initialize = () => {
    storyboard
        .loadSprite("sb/square.png")
        .initColor(98249, "194,189,50")
        .initVector(98249, 20, 10)
        .fadeIn(98249)
        .fadeOut(105646);

    storyboard
        .loadSprite("sb/square.png")
        .initColor(92084, "85,88,207")
        .initVector(92084, 20, 10)
        .fadeIn(92084)
        .vector(98249, 98660, 20, 10, 20, 0, 1)
        .fadeOut(98660);

    [72, 156, 240, 324, 408].forEach((y, i) => {
        const movements = leftRightMovements[i];

        storyboard
            .loadSprite("sb/line.png", { x: 181, y }, "Overlay", "BottomCentre")
            .initColor(91673, "120,200,255")
            .fadeIn(91673)
            .initRotate(91673, 0.5 * Math.PI)
            .vector(91673, 92084, 0.5, 0, 0.5, 2.8, 1)
            .fadeOut(98660);

        if (i !== 0)
            storyboard
                .vector(98249, 98660, 0.5, 2.8, 0.5, 0, 1)
                .moveXDecreasing(181, 320, 98249, 98660);
        else
            storyboard.vector(98249, 98660, 0.5, 2.8, 0.5, 2.6, 1);

        storyboard
            .loadSprite("sb/circle_border.png", { x: 496, y })
            .fadeIn(92084);

        let currentX = 496;
        movements.forEach(m => {
            const nextX = currentX === 496 ? 146 : 496;
            storyboard.moveXDecreasing(currentX, nextX, m, m + 205);
            currentX = nextX;
        });

        if (i !== 0)
            storyboard.scale(98249, 98660, 1, 0, 1);

        storyboard.fadeOut(98660);

        storyboard
            .loadSprite("sb/square.png", { x: 181, y })
            .fadeIn(91673)
            .scale(91673, 92084, 0, 0.5, 1)
            .rotate(91673, 92084, 2.5, 0.25 * Math.PI, 1);

        currentX = 181;
        movements.forEach(m => {
            const nextX = currentX === 461 ? 181 : 461;
            storyboard.moveXDecreasing(currentX, nextX, m, m + 205);
            currentX = nextX;
        });

        if (i !== 0)
            storyboard.scale(98249, 98660, 0.5, 0, 1);
        else
            storyboard.fadeOut(98249);
            // storyboard.moveXDecreasing(461, 433.45454, 98249, 98660);

        storyboard.fadeOut(98660);

        // PART B
        const rotations = secondPartRotations[i];
        let currentRotation = 0;
        const appearTime = rotations[0]?.time ?? 98660;
        storyboard.loadSprite("sb/lolly.png", { x: 433.8, y: 72.5 }, "Overlay", "BottomCentre");
        if (i === 0)
            storyboard
                .fadeIn(98249, 98660)
                .initRotate(98249, -0.5 * Math.PI);
        else
            storyboard.initRotate(appearTime, -0.5 * Math.PI);

        rotations.forEach(r => {
            const time = r.time + 10;
            const angle = r.big ? 2 : 1;
            const nextRotation = r.direction === "right" ? currentRotation - angle : currentRotation + angle;
            const baseRotation = -Math.PI / 2;
            storyboard.rotate(time, time + 205, baseRotation + currentRotation * (Math.PI / 8), baseRotation + nextRotation * (Math.PI / 8), 1);
            currentRotation = nextRotation;
        });

        const baseRotation = -Math.PI / 2;
        storyboard
            .rotate(104824, 105235, baseRotation + currentRotation * (Math.PI / 8), baseRotation - 2 * (Math.PI / 8), 2)
            .scale(104824, 105235, 1, 0, 1)
            .fadeOut(105235);
    });

    storyboard
        .loadSprite("sb/square.png", { x: 461, y: 72 })
        .fadeIn(98249)
        .initScale(98249, 0.5)
        .initRotate(98249, 0.25 * Math.PI)
        .moveXDecreasing(461, 433.45454, 98249, 98660)
        .scale(104824, 105235, 0.5, 0)
        .rotate(104824, 105235, 0.25 * Math.PI, -5)
        .fadeOut(105235)
};

const generateObject = (hitObject: HitCircle) => {
    const { startTime, position, sbPosition } = hitObject;
    const startTimeAR = getARStartTime(startTime);

    if (startTime < 98660) {
        const squarePosition = { ...sbPosition, x: sbPosition.x > 320 ? 181 : 461 };
        const endPosition = { ...sbPosition, x: sbPosition.x > 320 ? sbPosition.x - 40 : sbPosition.x + 40 }

        storyboard
            .loadSprite("sb/circle.png", squarePosition)
            .fade(startTimeAR, startTimeAR)
            .scale(startTimeAR, startTimeAR, 0.3, 0.3)
            .move(startTimeAR, startTime, squarePosition, endPosition);
    }
    else {
        const squarePosition = { x: 433.45454, y: 72 };
        const getEndPosition = () => {
            switch (position.x) {
                case 82: return { x: 181, y: 72 };
                case 104: return { x: 202.2455, y: 169.1182 };
                case 166: return { x: 256.3715, y: 249.9427 };
                case 260: return { x: 336.7182, y: 306.5 };
                case 370: return { x: 433.3363, y: 327.3364 };
                default: return sbPosition;
            };
        };

        storyboard
            .loadSprite("sb/circle.png", squarePosition)
            .fade(startTimeAR, startTimeAR)
            .scale(startTimeAR, startTimeAR, 0.3, 0.3)
            .move(startTimeAR, startTime, squarePosition, getEndPosition());
    }
};

const afterObjects = () => {

};

const getARStartTime = (startTime: number) => startTime - 550;

generateStuff();