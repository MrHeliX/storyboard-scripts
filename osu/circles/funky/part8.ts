import { parseHitObjects } from "../../../hitObjectsParser";
import { HitCircle, Position } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import * as clipboardy from "clipboardy";

const storyboard = new Storyboard();

const generateStuff = async () => {
    const hitObjects = (await parseHitObjects(`${__dirname}/part8.txt`)) as HitCircle[];
    const skip = [];

    initialize();
    currentColor = colors[0];
    hitObjects.forEach((hitObject, i) => {
        if (!skip.includes(hitObject.startTime)) {
            if (hitObject.startTime < 108934)
                generateObject(hitObject);
            else if (hitObject.startTime < 112221)
                generateObjectB(hitObject);
            else
                generateObjectC(hitObject);
        }
    });
    afterObjects();

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const changes = [105225, 105636, 106047, 106458, 106869, 107280, 107691].map(t => t + 10);
const changesB = [108924, 109335, 109746, 110156, 110567, 110978, 111389].map(t => t + 10);
const changesC = [111800, 112211, 112622, 113033, 113444, 113855, 114266, 114677, 115088, 115293, 115499, 115704, 115910, 116013, 116115, 116218, 116321, 116424, 116526, 116629].map(t => t + 10);

// Green
// const colors = ["17,53,17", "26,80,26", "44,133,44", "62,187,62", "86,198,86", "113,207,113", "140,216,140", "167,225,167"];
// Red
const colors = ["64,14,9", "92,21,14", "154,35,24", "215,49,34", "224,74,61", "229,103,92", "233,132,123", "238,160,153"];
let currentColor = colors[0];

const initialize = () => {
    // PART A
    changes.forEach(t => {
        const nextColor = getNextColor();
        storyboard
            .loadSprite("sb/square.png", { x: 320, y: -100 }, "Overlay", "BottomCentre")
            .color(t - 411, t - 411, "0,0,0", currentColor)
            .vector(t - 411, t - 411, 0, 0, 20, 10)
            .fadeIn(t - 411)
            .moveYDecreasing(-100, 240, t, t + 411)
            .fadeOut(t + 812);

        storyboard
            .loadSprite("sb/square.png", { x: 320, y: 580 }, "Overlay", "TopCentre")
            .color(t - 411, t - 411, "0,0,0", currentColor)
            .vector(t - 411, t - 411, 0, 0, 20, 10)
            .fadeIn(t - 411)
            .moveYDecreasing(580, 240, t, t + 411)
            .fadeOut(t + 812);

        currentColor = nextColor;
    });

    // PART B
    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 240 })
        .scale(108523, 108523, 0, 20)
        .color(108523, 108523, "0,0,0", "0,0,0")
        .fadeOut(112016);

    [0, 1].forEach(id => {
        const nextY = id === 0 ? 140 : 340;
        const anchor = id === 0 ? "BottomCentre" : "TopCentre";
        storyboard
            .loadSprite("sb/square.png", { x: 320, y: 240 }, "Overlay", anchor)
            .color(108112, 108112, "0,0,0", colors[colors.length - 2])
            .vector(108112, 108112, 0, 0, 20, 10)
            .fadeIn(108112)
            .moveYDecreasing(240, nextY, 108513, 108924);

        changesB.forEach((t, i) => {
            if (i % 2 === 0)
                storyboard.moveYDecreasing(i === 0 ? nextY : nextY + 150, nextY - 150, t, t + 411);
            else
                storyboard.moveYDecreasing(nextY - 150, nextY + 150, t, t + 411);
        });

        if (id === 0)
            storyboard.moveYDecreasing(-10, 90, 111810, 112016);
        else
            storyboard.moveYDecreasing(190, 90, 111810, 112016);

        storyboard
            .color(111810, 112016, colors[colors.length - 2], "99,198,141")
            .fadeOut(116742);
    });

    currentColor = colors[1];
};

const generateObject = (hitObject: HitCircle) => {
    const { startTime, sbPosition } = hitObject;
    const startTimeAR = getARStartTime(startTime);

    // First object
    if (startTime === 105646) {
        storyboard
            .loadSprite("sb/square.png", sbPosition)
            .scale(104824, 105235, 0, 1.2, 1)
            .color(104824, 104824, "0,0,0", currentColor)
            .rotate(104824, 105235, 4, 0, 1)
            .fadeOut(startTime);
    }
    else {
        storyboard
            .loadSprite("sb/square.png", sbPosition)
            .scale(startTimeAR - 200, startTimeAR - 200, 0, 1.2)
            .color(startTimeAR - 200, startTimeAR - 200, "0,0,0", currentColor)
            .fadeIn(startTimeAR - 200, startTimeAR)
            .fadeOut(startTime);
    }

    storyboard
        .loadSprite("sb/square_border.png", sbPosition)
        .color(startTimeAR, startTimeAR, "0,0,0", currentColor)
        .fadeIn(startTimeAR, startTimeAR + 200)
        .scale(startTimeAR, startTime, 3.5, 1.2);

    currentColor = getNextColor();
};

const generateObjectB = (hitObject: HitCircle) => {
    const { startTime, sbPosition } = hitObject;
    const startTimeAR = getARStartTime(startTime);

    storyboard
        .loadSprite("sb/square.png", sbPosition)
        .scale(startTimeAR, startTimeAR, 0, 1.2)
        .color(startTimeAR, startTimeAR, "0,0,0", colors[colors.length - 2])
        .fadeIn(startTimeAR, startTimeAR)
        .fadeOut(startTime);

    storyboard
        .loadSprite("sb/square_border.png", sbPosition)
        .color(startTimeAR, startTimeAR, "0,0,0", colors[colors.length - 2])
        .fadeIn(startTimeAR, startTimeAR + 200)
        .scale(startTimeAR, startTime, 3.5, 1.2);
};

const generateObjectC = (hitObject: HitCircle) => {
    const { startTime, position, sbPosition } = hitObject;
    const startTimeAR = getARStartTime(startTime);

    const color = position.x > 344 ? colors[0] : (position.x > 168 ? colors[1] : colors[2]);

    storyboard
        .loadSprite("sb/square.png", sbPosition)
        .scale(startTimeAR - 200, startTimeAR - 200, 0, 1.2)
        .color(startTimeAR - 200, startTimeAR - 200, "0,0,0", color)
        .fadeIn(startTimeAR - 200, startTimeAR)
        .fadeOut(startTime);

    storyboard
        .loadSprite("sb/square_border.png", sbPosition)
        .color(startTimeAR, startTimeAR, "0,0,0", color)
        .fadeIn(startTimeAR, startTimeAR + 200)
        .scale(startTimeAR, startTime, 3.5, 1.2);
};

const afterObjects = () => {
    // PART C
    [0, 1, 2].map(id => {
        const y = 480 - (240 * id);
        const scaleY = id === 1 ? 4 : 6;

        [0, 1].map(subId => {
            const anchor = subId === 0 ? "CentreRight" : "CentreLeft";
            const x = subId === 0 ? -160 : 800;
            storyboard
                .loadSprite("sb/square.png", { x, y }, "Overlay", anchor)
                .fadeIn(111810)
                .color(111810, 111810, "0,0,0", colors[id])
                .vector(111810, 111810, 0, 0, 20, scaleY)
                .moveXDecreasing(320, x, 117975, 118386)
                .fadeOut(118386);

            changesC.forEach((c, i) => {
                const nextTime = changesC[i + 1];
                const m = i % 3;
                if (m === id)
                    storyboard.moveXIncreasing(x, 320, c, nextTime ?? c + 103);
                if (m === (id + 2) % 3 && i > 1 && i !== changesC.length - 1)
                    storyboard.moveXIncreasing(320, x, c, nextTime ?? c + 103);
            });
        });
    });

    // Final object
    storyboard.loadSprite("sb/rounded_square.png");

    let scale = 0;
    [116742, 116845, 116947, 117050].forEach(time => {
        storyboard.scale(time, time + 20, scale, scale + 0.25);
        scale += 0.25;
    });

    let rotation = 0;
    [117153, 117358, 117564, 117769].forEach(time => {
        storyboard.rotate(time, time + 51, rotation, rotation - 0.25 * Math.PI);
        rotation -= 0.25 * Math.PI;
    });

    storyboard.fadeOut(117975);

    storyboard
        .loadSprite("sb/rounded_square.png")
        .fadeIn(117153)
        .initColor(117153, "31,100,216")
        .scale(117153, 117975, 0, 1);

    rotation = 0;
    [117153, 117358, 117564, 117769].forEach(time => {
        storyboard.rotate(time, time + 51, rotation, rotation - 0.25 * Math.PI);
        rotation -= 0.25 * Math.PI;
    });
};

const getNextColor = () => {
    const i = colors.findIndex(c => c === currentColor);
    if (i === colors.length - 1)
        return colors[0];
    return colors[i + 1];
};

const getARStartTime = (startTime: number) => startTime - 550;

generateStuff();