import { convertToSBPosition, isDefined } from "../../../helpers";
import { HitObjectOptions, Position } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import { Slider } from "../../beatmap-parser/objects/HitObjects/Slider";
import { PathType } from "../../beatmap-parser/Objects/PathType";
import { Vector2 } from "../../beatmap-parser/objects/Vector2";
import { generateSbHitCircle, getStoryboardValues } from "../../circles/storyboard-circle-creator";
import { calculateRotation, getSliderPathWithRepeats } from "../../sliders/storyboard-slider-creator";

export const dandelionGenerateSbSlider = (slider: Slider, _options?: HitObjectOptions): Storyboard => {
    const sb = new Storyboard();
    const options = _options ?? {
        cs: 4,
        ar: 9
    };

    generateSliderPath(sb, slider, options);
    generateSliderHead(sb, slider, options);
    // generateSliderBall(sb, slider);
    // generateFollowCircle(sb, slider);

    if (slider.RepeatCount > 0)
        generateReturnArrows(sb, slider, options);

    return sb;
};

const generateSliderHead = (sb: Storyboard, slider: Slider, options?: HitObjectOptions): void => {
    const { startTime, endTime, csFactors } = getStoryboardValues(slider, options);
    
    sb
        .loadSprite("approachcircle.png", slider.StoryboardPosition)
        .initColor(startTime, "213,104,88")
        .fade(startTime, endTime)
        .scale(startTime, endTime, csFactors.increasingApproachRateFactor, csFactors.decreasingApproachRateFactor);
};

const generateSliderPath = (sb: Storyboard, slider: Slider, options?: HitObjectOptions): void => {
    const { csFactors, startTime, endTime } = getStoryboardValues(slider, options);

    let sliderPath: Vector2[] = slider.Path.calculatedPath;

    if (slider.Path.pathType === PathType.Linear) {
        const sectionCount = Math.round(slider.Path.expectedDistance / 10);
        sliderPath = Array.from(Array(sectionCount + 1)).map((_, i) => {
            return slider.Path.calculatedPath[1].subtract(slider.Path.calculatedPath[0]).scale(i / sectionCount);
        });
    }
    else if (slider.Path.hasSubPaths) {
        sliderPath = [];
        const subPaths = [slider.Path.calculatedPath.slice(0, 2), slider.Path.calculatedPath.slice(1, 3)];
        subPaths.forEach((subPath, i) => {
            const sectionCount = Math.round((slider.Path.cumulativeLength[i + 1] - slider.Path.cumulativeLength[i]) / 10);
            const newPath = Array.from(Array(sectionCount + 1)).map((_, i) => {
                if (i > sectionCount) return null;
                return subPath[1].subtract(subPath[0]).scale(i / sectionCount).add(subPath[0]);
            }).filter(isDefined);

            sliderPath.push(...newPath);
        });
    }

    const interval = slider.Duration / sliderPath.length;

    sliderPath.forEach((step, i) => {
        const sbPos = convertToSBPosition(slider.Position.add(step));
        const delay = Math.round(i * interval);
        const size = csFactors.decreasingHitCircleFactor * 1.6;
        sb
            .loadSprite("sb/circle.png", sbPos)
            .initColor(1, "213,104,88")
            .scale(startTime + delay, endTime + delay, size / 2, size, 1)
            .fade(startTime + delay, endTime + delay)
            .fade(slider.StartTime + delay, slider.StartTime + delay + 20, 1, 0)
        // .fade(Math.round(slider.EndTime), Math.round(slider.EndTime) + 200, 1, 0);
    });
};

const generateSliderBall = (sb: Storyboard, slider: Slider, options?: HitObjectOptions): void => {
    const { csFactors } = getStoryboardValues(slider, options);
    sb
        .loadAnimation("sliderb.png", slider.StoryboardPosition, 10, 10)
        .scale(Math.round(slider.StartTime), null, 1, csFactors.sliderBallAndFollowCircleFactor)
        .fadeIn(Math.round(slider.StartTime));

    generateSliderMovingObject(sb, slider);
};

const generateFollowCircle = (sb: Storyboard, slider: Slider, options?: HitObjectOptions): void => {
    const { csFactors } = getStoryboardValues(slider, options);
    sb
        .loadSprite("sliderfollowcircle.png", slider.StoryboardPosition)
        .initScale(slider.StartTime, csFactors.sliderBallAndFollowCircleFactor)
        .fadeIn(slider.StartTime);

    generateSliderMovingObject(sb, slider);
};

const generateSliderMovingObject = (sb: Storyboard, slider: Slider): void => {
    const pathWithRepeats = getSliderPathWithRepeats(slider);

    const duration = slider.EndTime - slider.StartTime;
    const interval = pathWithRepeats.length > 1 ? duration / (pathWithRepeats.length - 1) : 0;

    let previousPos: Position;
    let previousRotation: number = 0;

    pathWithRepeats.forEach((step, index) => {
        const sbPos = convertToSBPosition(slider.Position.add(step));
        if (!previousPos) {
            previousPos = sbPos;
            return;
        }

        const { rotation, shouldFlip } = calculateRotation(sbPos, previousPos, false);

        const startTime = Math.round(slider.StartTime + ((index - 1) * interval));
        const endTime = Math.round(slider.StartTime + (index * interval));

        sb
            .move(startTime, endTime, previousPos, sbPos)
            .rotate(startTime, endTime, previousRotation, rotation);

        if (shouldFlip)
            sb.flip(startTime, endTime, "H");

        previousPos = sbPos;
        previousRotation = rotation;
    });
};

const generateReturnArrows = (sb: Storyboard, slider: Slider, options?: HitObjectOptions): void => {
    const { csFactors, startTime, endTime } = getStoryboardValues(slider, options);

    const sliderDuration = slider.EndTime - slider.StartTime;
    const durationWithoutRepeats = Math.round(sliderDuration / (slider.RepeatCount + 1));

    const repeatsOnTail = Math.ceil(slider.RepeatCount / 2);
    const repeatsOnHead = Math.floor(slider.RepeatCount / 2);

    const tailRotation = calculateRotation(slider.HeadCircle.Position.add(slider.Path.calculatedPath[slider.Path.calculatedPath.length - 2]), slider.TailCircle.Position, false);
    const headRotation = calculateRotation(slider.HeadCircle.Position.add(slider.Path.calculatedPath[1]), slider.HeadCircle.Position, false);

    sb
        .loadSprite("reversearrow.png", slider.TailCircle.StoryboardPosition)
        .initScale(1, csFactors.decreasingHitCircleFactor)
        .initRotate(1, tailRotation.rotation);

    if (tailRotation.shouldFlip)
        sb.flip(1, 1, "H");

    sb
        .fadeIn(startTime, endTime)
        .fadeOut(slider.StartTime + durationWithoutRepeats);

    Array.from(Array(repeatsOnTail - 1)).forEach((_, i) => {
        const fadeInTime = Math.round(slider.StartTime + (i * 2 + 1) * durationWithoutRepeats);
        const fadeOutTime = Math.round(fadeInTime + 2 * durationWithoutRepeats);

        sb
            .fadeIn(fadeInTime, fadeInTime + 160)
            .fadeOut(fadeOutTime);
    });

    if (repeatsOnHead === 0)
        return;

    sb
        .loadSprite("reversearrow.png", slider.HeadCircle.StoryboardPosition)
        .initScale(1, csFactors.decreasingHitCircleFactor)
        .initRotate(1, headRotation.rotation);

    if (headRotation.shouldFlip)
        sb.flip(1, 1, "H");

    Array.from(Array(repeatsOnHead)).forEach((_, i) => {
        const fadeInTime = Math.round(slider.StartTime + (i * 2) * durationWithoutRepeats);
        const fadeOutTime = Math.round(fadeInTime + 2 * durationWithoutRepeats);

        sb
            .fadeIn(fadeInTime, fadeInTime + 160)
            .fadeOut(fadeOutTime);
    });
};