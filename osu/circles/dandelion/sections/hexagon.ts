import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { HitObject } from "../../../beatmap-parser/objects/HitObjects/HitObject";
import { Vector2 } from "../../../beatmap-parser/objects/Vector2";

const storyboard = new Storyboard();

const start = 44556;
const end = 58667

export const dandelionGenerateHexagonSection = (sb: Storyboard, beatmap: Beatmap) => {
    const filteredObjects = beatmap.HitObjects.filter(h => h.StartTime >= start && h.StartTime <= end);

    generateHexagonGrid(filteredObjects);

    filteredObjects.forEach(h => {
        generateHexagonCircle(h);
    });

    sb.append([storyboard]);
};

const generateHexagonGrid = (hitObjects: HitObject[]) => {
    for (let y = 0; y < 7; y++) {
        for (let x = 0; x < 9; x++) {
            const shift = y % 2 === 0 ? -35 : 0;
            const posX = 40 + 70 * x + shift;
            const posY = 60 + 60 * y;

            if (x === 7 && y === 5) {
                storyboard
                    .loadSprite("sb/bingv.png", { x: posX, y: posY })
                    .initScale(44556, 0.3)
                    .fadeIn(44556, 44556, 0.4, 0.4)
                    .fadeOut(57889, 57889, 0.4);
            }

            if (x === 4 && y === 3) {
                storyboard
                    .loadSprite("sb/hexagon.png", { x: 320, y: 240 }, "Overlay")
                    .initScale(43667, 0.6)
                    .fadeInIncreasing(43667, 44556);
            }
            else {
                storyboard
                    .loadSprite("sb/hexagon.png", { x: posX, y: posY })
                    .initScale(44112, 0)
                    .fadeInDecreasing(44112, 44556)
                    .scale(44112, 44556, 0, 0.6, 1)
                    .rotate(44112, 44556, 1, 0, 1);
            }

            let hasLateHit = false;

            const objectsOnHexagon = hitObjects.filter(h => h.StoryboardPosition.x === posX && h.StoryboardPosition.y === posY);
            objectsOnHexagon.forEach(h => {
                if (h.StartTime < 57889) {
                    storyboard
                        .scale(h.StartTime, h.StartTime + 250, 0.6, 0.5, 1)
                        .fadeOutDecreasing(h.StartTime, h.StartTime + 250, 1, 0.6);
                }

                if (h.StartTime + 900 < 57889) {
                    storyboard
                        .fadeInIncreasing(h.StartTime + 600, h.StartTime + 900, 0.6, 1)
                        .scale(h.StartTime + 600, h.StartTime + 900, 0.5, 0.6, 2);
                }
                else if (h.StartTime < 57889) {
                    hasLateHit = true;
                    storyboard
                        .scale(57889, 58445, 0.5, 0, 2)
                        .fadeOut(57889, 58445, 0.6, 0);
                }
            });

            if (!hasLateHit) {
                storyboard
                    .scale(57889, 58445, 0.6, 0, 2)
                    .fadeOut(57889, 58445);
            }
        }
    }
};

const hexagonColors = [
    { time: 44556, color: "183,224,88" },
    { time: 45445, color: "213,204,88" },
    { time: 46445, color: "213,174,88" },
    { time: 46778, color: "213,144,88" },
    { time: 47445, color: "213,104,88" },
    { time: 48334, color: "213,144,88" },
    { time: 50112, color: "213,174,88" },
    { time: 51112, color: "213,204,88" },
    { time: 51889, color: "183,224,88" },
    { time: 52778, color: "213,204,88" },
    { time: 53667, color: "213,174,88" },
    { time: 54556, color: "213,174,88" },
    { time: 54889, color: "213,104,88" },
    { time: 55445, color: "213,144,88" },
    { time: 57223, color: "213,174,88" }
];

const generateHexagonCircle = (hitObject: HitObject) => {
    const { StartTime, StoryboardPosition } = hitObject;
    const ARStartTime = StartTime - 600;
    const color = [...hexagonColors].reverse().find(c => StartTime >= c.time);

    storyboard
        .loadSprite("sb/hexagon.png", StoryboardPosition)
        .initColor(ARStartTime, color?.color)
        .scale(ARStartTime, StartTime, 0, 0.6);
};