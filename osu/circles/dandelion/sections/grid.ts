import { Position } from "../../../../interfaces";
import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { HitObject } from "../../../beatmap-parser/objects/HitObjects/HitObject";

const storyboard = new Storyboard();
const start = 94778;
const end = 108555;

export const dandelionGenerateGridSection = (sb: Storyboard, beatmap: Beatmap) => {
    generateGrid();

    beatmap.HitObjects
        .filter(h => h.StartTime >= start && h.StartTime <= end)
        .forEach(h => {
            generateGridCircle(h);
        });

    generateFlowerParticles();

    sb.append([storyboard]);
};

const generateGrid = () => {
    storyboard
        .loadSprite("sb/rounded_square.png")
        .fadeInDecreasing(93883, 94778)
        .scale(93883, 94778, 0, 1, 1)
        .fadeOutIncreasing(108556, 109000)
        .scale(108556, 109000, 1, 0.5, 2);

    for (let x = 0; x < 7; x++) {
        for (let y = 0; y < 5; y++) {
            if (x === 3 && y === 2) continue;

            const posX = 320 + (x - 3) * 65;
            const posY = 240 + (y - 2) * 65;
            storyboard
                .loadSprite("sb/rounded_square.png", { x: posX, y: posY })
                .scale(94778, 95111, 0, 1, 1)
                .rotate(94778, 95111, (x - 3) * 0.5, 0, 1);
            
            if (x < 2 || x > 4) {
                storyboard
                    .fadeOutIncreasing(106889, 107777)
                    .scale(106889, 107777, 1, 0.5, 2);
            }
            else {
                storyboard
                    .fadeOutIncreasing(108556, 109000)
                    .scale(108556, 109000, 1, 0.5, 2);
            }
        }
    }
};

const generateGridCircle = (hitObject: HitObject) => {
    const { StartTime, StoryboardPosition } = hitObject;
    const ARStartTime = StartTime - 525;
    const secondHalf = StartTime >= 101445;

    storyboard
        .loadSprite("sb/rounded_square.png", StoryboardPosition)
        .initColor(ARStartTime, secondHalf ? "183,224,88" : "213,104,88")
        .scale(ARStartTime, StartTime, 0, 1)
        .fadeOut(StartTime, StartTime + 100);
};

const generateFlowerParticles = () => {
    for (let t = start; t < end - 1200; t += 25) {
        const startPos: Position = { x: 800, y: Math.round(Math.random() * 480) };
        const endPos: Position = { x: -100, y: Math.round(Math.random() * 480) };
        const speed = Math.round(Math.random() * 1200 + 1000);
        const size = Math.random() * 0.2 + 0.1;
        
        storyboard
            .loadSprite("sb/flower.png", startPos)
            .fadeIn(t, t, 0, 0.8)
            .initColor(t, "242, 232, 107")
            .initScale(t, size)
            .move(t, t + speed, startPos, endPos);
    };
};