import { HitObjectOptions } from "../../../../interfaces";
import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { HitObject, isSlider } from "../../../beatmap-parser/objects/HitObjects/HitObject";
import { dandelionGenerateSbSlider } from "../dandelion-slider-creator";

const storyboard = new Storyboard();

const flowerSizes = [
    { start: 16112, end: 17556, size: 0.75 },
    { start: 18001, end: 19334, size: 0.6 },
    { start: 19778, end: 20445, size: 0.4 },
    { start: 20667, end: 21334, size: 0.9 },
    { start: 21556, end: 22889, size: 0.5 },
    { start: 25223, end: 25333, size: 0.9 },
    { start: 25334, end: 25444, size: 0.8 },
    { start: 25445, end: 25555, size: 0.7 },
    { start: 25556, end: 25666, size: 0.6 },
    { start: 25667, end: 25777, size: 0.5 },
    { start: 25778, end: 25888, size: 0.5 },
    { start: 26334, end: 26889, size: 0.4 },
    { start: 27001, end: 27445, size: 0.55 },
    { start: 27556, end: 28334, size: 0.7 },
    { start: 28445, end: 29445, size: 0.85 },
    { start: 29556, end: 30445, size: 1 }
];

export const dandelionGenerateIntro = (sb: Storyboard, beatmap: Beatmap) => {
    generateMotiveCircles();

    beatmap.HitObjects.forEach((h, i) => {
        if (h.StartTime >= 9001 && h.StartTime < 16112)
            generateMotiveApproachCircle(h);
        else if (h.StartTime >= 16112 &&  h.StartTime <= 30334) {
            if (isSlider(h)) {
                const size = flowerSizes.find(s => s.start <= h.StartTime && s.end >= h.StartTime);
                const options: HitObjectOptions = {
                    cs: Math.round(4 / (size?.size ?? 1)),
                    ar: 9,
                    hideNumber: true
                };
                storyboard.append([dandelionGenerateSbSlider(h, options)]);
            }
            else
                generateFlowerCircle(h, beatmap.HitObjects[i + 1]);
        }
    });

    sb.append([storyboard]);
};

const generateMotiveCircles = () => {
    for (let i = 0; i < 5; i++) {
        const x = 128 + 96 * i;
        const y = 360;

        storyboard
            .loadSprite("sb/circle.png", { x, y })
            .scale(8112, 8556, 0, 1, 1)
            .fadeOutIncreasing(15667, 16112)
            .scale(15667, 16112, 1, 0.2);
    }
};

const generateMotiveApproachCircle = (hitObject: HitObject) => {
    const colors = ["213,104,88", "213,144,88", "213,174,88", "213,204,88", "183,224,88"];
    const { StartTime, StoryboardPosition } = hitObject;
    const ARStartTime = StartTime - 525;

    const index = (StoryboardPosition.x - 128) / 96;

    storyboard
        .loadSprite("sb/circle", StoryboardPosition)
        .initColor(ARStartTime, colors[index])
        .scale(ARStartTime, StartTime, 0, 1, 1)
        .fadeOut(StartTime, StartTime + 100);
};

const generateFlowerCircle = (hitObject: HitObject, nextObject?: HitObject) => {
    const { StartTime, StoryboardPosition } = hitObject;
    const ARStartTime = StartTime - 525;

    let size = flowerSizes.find(s => s.start <= StartTime && s.end >= StartTime)?.size ?? 1;
    if (nextObject?.StackedPosition.almostEquals(hitObject.StackedPosition, 1))
        size *= 0.75;

    storyboard
        .loadSprite("sb/circle_border", StoryboardPosition)
        .fadeIn(ARStartTime - 200, ARStartTime)
        .scale(ARStartTime - 200, ARStartTime, size / 2, size, 1)
        .fadeOut(StartTime, StartTime + 100);

    storyboard
        .loadSprite("sb/circle.png", StoryboardPosition)
        .initColor(ARStartTime, "213,144,88")
        .scale(ARStartTime, StartTime, 0.1, 0.85 * size);
};