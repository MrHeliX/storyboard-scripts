import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { HitObject } from "../../../beatmap-parser/objects/HitObjects/HitObject";

const storyboard = new Storyboard();
const start = 108666;
const end = 122777;

export const dandelionGenerateEnding = (sb: Storyboard, beatmap: Beatmap) => {
    drawPart1Grid();
    drawPart2Flower();

    const filteredObjects = beatmap.HitObjects.filter(h => h.StartTime >= start && h.StartTime <= end);
    filteredObjects.forEach(h => {
        if (h.StartTime < 115667)
            drawPart1Circle(h);
        else
            drawPart2Circle(h);
    });

    sb.append([storyboard]);
};

const drawPart1Grid = () => {
    for (let y = 0; y < 5; y++) {
        const posX = y % 2 === 0 ? 500 : 120;
        const posY = 240 + 85 * (y - 2);

        storyboard
            .loadSprite("sb/circle.png", { x: posX, y: posY })
            .fadeInDecreasing(107333, 108222)
            .scale(107333, 108222, 0, 1, 1)
            .scale(115222, 115667, 1, 0, 1)
            .fadeOutDecreasing(115222, 115667);
    }
};

const drawPart2Flower = () => {
    storyboard
        .loadSprite("sb/big-flower.png")
        .initColor(114778, "224,216,148")
        .fadeInDecreasing(114778, 115445)
        .scale(114778, 115445, 0, 0.85, 1)
        .rotate(114778, 115445, 2, 0, 1)
        .scale(122556, 122889, 0.85, 0, 2);

    for (let i = 0; i < 12; i++) {
        const diffX = Math.round(Math.sin((Math.PI / 6) * i) * 155);
        const diffY = Math.round(Math.cos((Math.PI / 6) * i) * 155);

        storyboard
            .loadSprite("sb/circle_border.png", { x: 320 + diffX, y: 240 - diffY })
            .initScale(115222, 0.45)
            .fadeInIncreasing(115222, 115556)
            .move(122556, 122889, { x: 320 + diffX, y: 240 - diffY }, { x: 320, y: 240 })
            .scale(122556, 122889, 0.45, 0);

        storyboard
            .loadSprite("sb/circle.png")
            .initColor(122333, "200, 100, 50")
            .scale(122333, 122778, 0.3, 0.1, 1)
            .move(122333, 122778, { x: 320 + diffX, y: 240 - diffY }, { x: 320, y: 240 });
    }

    storyboard
        .loadSprite("sb/circle_border.png")
        .initScale(121889, 0.45)
        .fadeInIncreasing(121889, 122333)
        .scale(122556, 122889, 0.45, 0);
};

const drawPart1Circle = (hitObject: HitObject) => {
    const colors = ["213,104,88", "213,144,88", "213,174,88", "213,204,88", "183,224,88"];
    const { StartTime, StoryboardPosition } = hitObject;
    const ARStartTime = StartTime - 525;

    const index = (StoryboardPosition.y - 240) / 85 + 2;

        storyboard
            .loadSprite("sb/circle.png", StoryboardPosition)
            .initColor(ARStartTime, colors[index])
            .scale(ARStartTime, StartTime, 0, 1, 1)
            .fadeOut(StartTime, StartTime + 100);
};

const drawPart2Circle = (hitObject: HitObject) => {
    const { StartTime, StoryboardPosition } = hitObject;
    const ARStartTime = StartTime - 650;

    storyboard
        .loadSprite("sb/circle.png")
        .initColor(ARStartTime, "213,144,88")
        .scale(ARStartTime, StartTime, 0, 0.2, 1)
        .move(ARStartTime, StartTime, { x: 320, y: 240 }, StoryboardPosition);
};
