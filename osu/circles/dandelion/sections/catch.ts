import { isDefined } from "../../../../helpers";
import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { HitCircle } from "../../../beatmap-parser/objects/HitObjects/HitCircle";
import { isCircle, isSlider } from "../../../beatmap-parser/objects/HitObjects/HitObject";
import { Slider } from "../../../beatmap-parser/objects/HitObjects/Slider";
import { PathType } from "../../../beatmap-parser/Objects/PathType";
import { Vector2 } from "../../../beatmap-parser/objects/Vector2";

const storyboard = new Storyboard();

const start = 80112;
const end = 94223;

export const dandelionGenerateCatch = (sb: Storyboard, beatmap: Beatmap) => {
    generateCatchBar();
    beatmap.HitObjects
        .filter(h => h.StartTime >= start && h.StartTime <= end)
        .forEach(h => {
            if (isSlider(h))
                generateSlider(h);
            else if (isCircle(h))
                generateCircle(h);
        });

    sb.append([storyboard]);
};

const generateCatchBar = () => {
    storyboard
        .loadSprite("sb/catch-bar.png", { x: 320, y: 400 })
        .vector(79667, 80112, 0, 1, 1, 1, 1)
        .vector(94334, 94667, 1, 1, 0, 1, 1);
};

const generateCircle = (circle: HitCircle) => {
    const { StartTime, StoryboardPosition } = circle;
    const ARStartTime = StartTime - 1150;
    const rotationSpeed = Math.random() * 6 - 3;

    storyboard
        .loadSprite("sb/flower.png", { x: StoryboardPosition.x, y: -100 })
        .initScale(ARStartTime, 0.3)
        .initColor(ARStartTime, "213,144,88")
        .rotate(ARStartTime, StartTime, 0, rotationSpeed)
        .moveY(-100, StoryboardPosition.y, ARStartTime, StartTime, 15);
};

const generateSlider = (slider: Slider) => {
    const { StartTime, Duration, Path, StoryboardPosition } = slider;

    let sliderPath: Vector2[] = Path.calculatedPath;

    if (Path.pathType === PathType.Linear) {
        const sectionCount = Math.round(Path.expectedDistance / 25);
        sliderPath = Array.from(Array(sectionCount + 1)).map((_, i) => {
            return Path.calculatedPath[1].subtract(slider.Path.calculatedPath[0]).scale(i / sectionCount);
        });
    }
    else if (Path.hasSubPaths) {
        sliderPath = [];
        const subPaths = [Path.calculatedPath.slice(0, 2), Path.calculatedPath.slice(1, 3)];
        subPaths.forEach((subPath, i) => {
            const sectionCount = Math.round((slider.Path.cumulativeLength[i + 1] - slider.Path.cumulativeLength[i]) / 25);
            const newPath = Array.from(Array(sectionCount + 1)).map((_, i) => {
                if (i > sectionCount) return null;
                return subPath[1].subtract(subPath[0]).scale(i / sectionCount).add(subPath[0]);
            }).filter(isDefined);

            sliderPath.push(...newPath);
        });
    }

    const interval = Duration / sliderPath.length;

    sliderPath.forEach((pathPart, i) => {
        const truePosition = { x: StoryboardPosition.x + pathPart.x, y: StoryboardPosition.y + pathPart.y };
        const time = Math.round(StartTime + interval * i);
        const ARStartTime = time - 1150;

        const rotationSpeed = Math.random() * 6 - 3;

        storyboard
            .loadSprite("sb/flower.png", { x: truePosition.x, y: -100 })
            .initScale(ARStartTime, i === 0 ? 0.3 : 0.18)
            .initColor(ARStartTime, i === 0 ? "213,104,88" : "255,255,255")
            .rotate(ARStartTime, time, 0, rotationSpeed)
            .moveY(-100, truePosition.y, ARStartTime, time, 15);
    });
};