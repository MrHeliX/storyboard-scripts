import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { HitObject } from "../../../beatmap-parser/objects/HitObjects/HitObject";

const storyboard = new Storyboard();
const darknessStoryboard = new Storyboard();

const start = 30334;
const end = 44445;

export const dandelionGenerateDarknessSection = (sb: Storyboard, beatmap: Beatmap) => {
    generateDarkness();

    beatmap.HitObjects
        .filter(h => h.StartTime >= start && h.StartTime <= end)
        .forEach(h => {
            generateDarknessCircle(h);
        });

    darknessStoryboard.fadeOut(end);

    storyboard.append([darknessStoryboard]);

    generateTransitionToHexagons();

    sb.append([storyboard]);
};

const generateDarkness = () => {
    darknessStoryboard
        .loadSprite("sb/square.png")
        .initColor(start, "0,0,0")
        .initScale(start, 20)
        .fadeInIncreasing(start + 100, start + 700, 0, 1);

    for (let t = 31223; t < 44445; t += 889) {
        darknessStoryboard
            .fadeOutDecreasing(t, t + 250, 1, 0.7)
            .fadeInIncreasing(t + 250, t + 700, 0.7, 1);
    }
};

const generateDarknessCircle = (hitObject: HitObject) => {
    const { StartTime, StoryboardPosition } = hitObject;
    const ARStartTime = StartTime - 1000;

    storyboard
        .loadSprite("sb/circle.png", StoryboardPosition)
        .initColor(ARStartTime, "164,157,68")
        .scale(ARStartTime, ARStartTime + 400, 0, 1, 1)
        .fadeIn(ARStartTime, ARStartTime + 300)
        .fadeOut(StartTime, StartTime + 100);
};

const generateTransitionToHexagons = () => {
    storyboard
        .loadSprite("sb/square.png", { x: 0, y: 240 })
        .initColor(44112, "203,224,148")
        .initVector(44112, 0, 10)
        .vector(44112, 44334, 0, 10, 5, 10, 1)
        .fadeOut(44667);
    storyboard
        .loadSprite("sb/square.png", { x: 220, y: 240 })
        .initColor(44223, "203,224,148")
        .initVector(44223, 0, 10)
        .vector(44223, 44445, 0, 10, 5, 10, 1)
        .fadeOut(44667);
    storyboard
        .loadSprite("sb/square.png", { x: 440, y: 240 })
        .initColor(44334, "203,224,148")
        .initVector(44334, 0, 10)
        .vector(44334, 44556, 0, 10, 5, 10, 1)
        .fadeOut(44667);
    storyboard
        .loadSprite("sb/square.png", { x: 660, y: 240 })
        .initColor(44445, "203,224,148")
        .initVector(44445, 0, 10)
        .vector(44445, 44667, 0, 10, 5, 10, 1)
        .fadeOut(44667);
};