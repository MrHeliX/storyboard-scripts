import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { HitObject } from "../../../beatmap-parser/objects/HitObjects/HitObject";

const storyboard = new Storyboard();

const start = 58778;
const end = 80001;

export const dandelionGenerateEdgesSection = (sb: Storyboard, beatmap: Beatmap) => {
    const filteredObjects = beatmap.HitObjects.filter(h => h.StartTime >= start && h.StartTime <= end);
    filteredObjects.forEach((h, i) => {
        const prevObject = filteredObjects[i - 1];
        generateEdgeCircle(h, prevObject);
    });

    sb.append([storyboard]);
};

const generateEdgeCircle = (hitObject: HitObject, prevObject?: HitObject) => {
    const { StartTime, StoryboardPosition } = hitObject;
    const AppearTime = StartTime - 625;
    const ARStartTime = StartTime - 525;

    const pos = prevObject?.StoryboardPosition ?? StoryboardPosition;
    const colorCircle = hitObject.ObjectNumber === 1;

    storyboard
        .loadSprite("sb/circle.png", pos)
        .initScale(AppearTime, 0.2)
        .initColor(AppearTime, colorCircle ? "213,174,88" : "255,255,255")
        .fadeIn(AppearTime, ARStartTime)
        .fadeOut(StartTime, StartTime + 100);

    if (prevObject) {
        storyboard
            .moveDecreasing(pos, StoryboardPosition, AppearTime, ARStartTime);
    }

    if (colorCircle) {
        storyboard
            .loadSprite("approachcircle.png", StoryboardPosition)
            .initColor(ARStartTime, "213,174,88")
            .fadeIn(ARStartTime, StartTime)
            .scale(ARStartTime, StartTime, 0.75, 0.1325)
    }
};