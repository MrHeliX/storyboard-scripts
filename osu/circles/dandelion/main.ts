import { HitObjectOptions } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import { parseBeatmapFromFile } from "../../beatmap-parser/beatmap-parser";
import { Beatmap } from "../../beatmap-parser/Objects/Beatmap";
import { generateSbHitCircle } from "../storyboard-circle-creator";
import { dandelionGenerateCatch } from "./sections/catch";
import { dandelionGenerateDarknessSection } from "./sections/darkness";
import { dandelionGenerateEdgesSection } from "./sections/edges";
import { dandelionGenerateEnding } from "./sections/ending";
import { dandelionGenerateGridSection } from "./sections/grid";
import { dandelionGenerateHexagonSection } from "./sections/hexagon";
import { dandelionGenerateIntro } from "./sections/intro";

const storyboard = new Storyboard();

const generateStoryboard = async () => {
    const filePath = `O:\\osu!\\Songs\\beatmap-637903144772131139-dandelion\\saminun - dandelion (Mr HeliX) [gen].osu`;
    const beatmap = await parseBeatmapFromFile(filePath);

    generateBlocker();
    generateBackgroundColorEffects();

    generateIntroNormalCircles(beatmap)
    dandelionGenerateIntro(storyboard, beatmap);
    dandelionGenerateDarknessSection(storyboard, beatmap);
    dandelionGenerateHexagonSection(storyboard, beatmap);
    dandelionGenerateEdgesSection(storyboard, beatmap);
    dandelionGenerateCatch(storyboard, beatmap);
    dandelionGenerateGridSection(storyboard, beatmap);
    dandelionGenerateEnding(storyboard, beatmap);

    storyboard.copyToClipboard();
};

const generateBlocker = () => {
    storyboard
        .loadSprite("korenbloemen.jpg")
        .initScale(0, 0.6858182)
        .fadeOut(1445);

    storyboard
        .loadSprite("sb/square.png")
        .initScale(0, 20)
        .initColor(0, "0,0,0")
        .fadeIn(112, 1445)
        .fadeOut(16112)
        .fadeIn(30334)
        .fadeOut(44556)
        .fadeIn(57889)
        .fadeOut(80112)
        .fadeIn(94334)
        .fadeOut(95445)
        .fadeIn(108556)
        .fadeOut(115667);

    storyboard
        .loadSprite("korenbloemen.jpg")
        .initScale(0, 0.6858182)
        .fadeIn(122333)
        .fadeOut(128499);
};

const generateBackgroundColorEffects = () => {
    // Intro
    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 240 }, "Overlay", "CentreRight")
        .initColor(12556, "213,174,88")
        .initVector(12556, 0, 10)
        .vector(12556, 16223, 0, 10, 1, 10, 2);

    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 240 }, "Overlay", "CentreLeft")
        .initColor(12556, "213,174,88")
        .initVector(12556, 0, 10)
        .vector(12556, 16223, 0, 10, 1, 10, 2);

    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 240 }, "Overlay", "CentreRight")
        .initColor(15112, "213,174,88")
        .initVector(15112, 0, 10)
        .vector(15112, 16112, 0, 10, 15, 10, 2)
        .fadeOut(23445);

    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 240 }, "Overlay", "CentreLeft")
        .initColor(15112, "213,174,88")
        .initVector(15112, 0, 10)
        .vector(15112, 16112, 0, 10, 15, 10, 2)
        .fadeOut(23445);

    // Intro part 2
    storyboard
        .loadSprite("sb/square.png", { x: -100, y: 240 }, "Overlay", "CentreRight")
        .initColor(23001, "213,204,88")
        .initScale(23001, 20)
        .moveXDecreasing(-100, 800, 23001, 23445)
        .fadeOut(44667);

    storyboard
        .loadSprite("sb/square.png")
        .initColor

    // Hexagons
    storyboard
        .loadSprite("sb/square.png")
        .initColor(44667, "203,224,148")
        .initScale(44667, 20)
        .fadeOut(57889);

    // Edges
    const edgesColorChanges = [
        { time: 58778, color: "87, 107, 143" },
        { time: 60112, color: "118, 204, 82" },
        { time: 60556, color: "141, 171, 70" },
        { time: 61889, color: "156, 82, 78" },
        { time: 62334, color: "186, 114, 73" },
        { time: 63667, color: "164, 111, 199" },
        { time: 64112, color: "75, 75, 196" },
        { time: 65001, color: "79, 123, 189" },
        { time: 65889, color: "181, 62, 74" },
        { time: 67223, color: "99, 191, 98" },
        { time: 67667, color: "88, 184, 142" },
        { time: 68556, color: "143, 76, 126" },
        { time: 69445, color: "201, 160, 93" },
        { time: 70778, color: "114, 219, 202" },
        { time: 71223, color: "200, 204, 78" },
        { time: 72112, color: "112, 199, 86" },
        { time: 73001, color: "184, 89, 55" },
    ];

    for (let x = 0; x < 9; x++) {
        for (let y = 0; y < 5; y++) {
            storyboard
                .loadSprite("sb/square.png", { x: -160 + x * 120, y: y * 120 })
                .initColor(57889, "203,224,148")
                .initScale(57889, 3)
                .fadeIn(57889)
                .scale(57889, 58556, 3, 2, 1);

            edgesColorChanges.forEach((c, i) => {
                const prevColor = edgesColorChanges[i - 1]?.color ?? "203,224,148";
                storyboard.color(c.time + 30 * x, c.time + 40 * x + 350, prevColor, c.color, 1);
            });

            let tick = 0;
            for (let t = 73001; t < 78001; t += 444) {
                const m = (x + y + tick) % 2;
                if (m === 0 && tick !== 11)
                    storyboard.scale(t, t + 300, 2, 1, 1);
                else if (m === 1 && tick !== 0)
                    storyboard.scale(t, t + 300, 1, 2, 1);

                tick++;
            }

            let disappearTime: number;
            if (y === 0) disappearTime = 78334;
            if (y === 1) disappearTime = 78556;
            if (y === 3) disappearTime = 78667;
            if (y === 4) disappearTime = 78778;

            // const m = (x + y + tick) % 2;
            if (disappearTime)
                storyboard.scale(disappearTime, disappearTime + 300, 2, 0, 1);

            if (y === 2) {
                storyboard
                    .scale(79001 + x * 30, 79301 + x * 30, 2, 1.15, 1)
                    .moveDecreasing({ x: -160 + x * 120, y: 240 }, { x: 40 + x * 70, y: 400 }, 79334 + x * 30, 79667 + x * 30)
                    // .moveYDecreasing(240, 400, 79223, 79423)
                    // .moveXDecreasing(-160 + x * 120, x * 80, 79667, 79889);
            }
            
            storyboard.fadeOut(80112);
        }
    }

    // Cat photo
    storyboard
        .loadSprite("sb/078.jpg")
        .initScale(94334, 0.5)
        .fadeIn(94334, 94334, 0, 0.075)
        .fadeOut(95445);

    // Catch background
    storyboard
        .loadSprite("sb/square.png", { x: 320, y: 500 }, "Overlay", "TopCentre")
        .initScale(79667, 20)
        .initColor(79667, "224,216,148")
        .moveYDecreasing(500, -100, 79667, 80112)
        .fadeOutIncreasing(94334, 94667);

    // Catch flower
    storyboard
        .loadSprite("sb/big-flower.png", { x: 500, y: 100 })
        .fadeIn(79667, 79667, 0, 0.7)
        .initColor(79667, "213,174,88")
        .scale(79667, 80112, 0, 1, 1)
        .rotate(79667, 94334, 0, 1)
        .scale(94334, 94556, 1, 0.7, 2)
        .rotate(94334, 94556, 1, 0.8, 2)
        .fadeOutIncreasing(94334, 94556, 0.7);

    // Grid background
    storyboard
        .loadSprite("sb/square.png")
        .initColor(94666, "224,184,148")
        .scale(94666, 95000, 0, 20, 1)
        .fadeOut(101889);

    storyboard
        .loadSprite("sb/square.png", { x: -100, y: 240 }, "Overlay", "CentreRight")
        .initColor(101445, "203,224,148")
        .initScale(101445, 20)
        .moveXDecreasing(-100, 800, 101445, 101889)
        .fadeOut(108556, 109000);

    // Ending background
    storyboard
        .loadSprite("sb/circle.png")
        .initColor(115222, "213,174,88")
        .scale(115222, 115778, 0, 15, 1)
        .scale(122333, 122889, 15, 0, 2);
};

const generateIntroNormalCircles = (beatmap: Beatmap) => {
    const options: HitObjectOptions = {
        ar: beatmap.Difficulty.ApproachRate,
        cs: beatmap.Difficulty.CircleSize
    };

    beatmap.HitObjects
        .filter(h => h.StartTime < 9001)
        .forEach(h => storyboard.append([generateSbHitCircle(h, options)]));
};

generateStoryboard();