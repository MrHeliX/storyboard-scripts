import { HitObjectOptions } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import { Beatmap } from "../../beatmap-parser/Objects/Beatmap";
import { parseBeatmapFromFile } from "../../beatmap-parser/beatmap-parser";
import { generateSbHitCircle } from "../storyboard-circle-creator";
import { arabesqueGenerateARSection } from "./sections/ar";
import { arabesqueGenerateGridSection } from "./sections/grid";

const storyboard = new Storyboard();

const generateStoryboard = async () => {
    const filePath = `O:\\osu!\\Songs\\beatmap-638195076868331192-audio\\uraboroshi - Retrospective Arabesque (Mr HeliX) [b].osu`;
    const beatmap = await parseBeatmapFromFile(filePath);

    generateBlocker();
    generateIntroNormalCircles(beatmap);
    arabesqueGenerateGridSection(storyboard, beatmap);
    arabesqueGenerateARSection(storyboard, beatmap);

    storyboard.copyToClipboard();
};

const generateBlocker = () => {
    storyboard
        .loadSprite("sb/zwart.jpg")
        .initScale(8299, 20)
        .fadeIn(8299)
        .fadeOut(118053);
};

const generateIntroNormalCircles = (beatmap: Beatmap) => {
    const options: HitObjectOptions = {
        ar: beatmap.Difficulty.ApproachRate,
        cs: beatmap.Difficulty.CircleSize
    };

    const objects = beatmap.HitObjects.filter(h => h.StartTime >= 8299 && h.StartTime < 18966);
    const shrinkingObjects = beatmap.HitObjects.filter(h => h.StartTime >= 18966 && h.StartTime < 34966);

    objects.forEach(h => storyboard.append([generateSbHitCircle(h, options)]));
    shrinkingObjects.forEach((h, i) => {
        options.cs = (i + 1) / shrinkingObjects.length * 3 + beatmap.Difficulty.CircleSize;
        storyboard.append([generateSbHitCircle(h, options)]);
    });
};

generateStoryboard();
