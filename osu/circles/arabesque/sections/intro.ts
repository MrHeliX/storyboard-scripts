import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { HitObject } from "../../../beatmap-parser/objects/HitObjects/HitObject";

const storyboard = new Storyboard();

export const arabesqueGenerateIntro = (sb: Storyboard, beatmap: Beatmap) => {
    const objects = beatmap.HitObjects.filter(h => h.StartTime >= 8299 && h.StartTime < 18966);
    const shrinkingObjects = beatmap.HitObjects.filter(h => h.StartTime >= 18966 && h.StartTime < 29633);
    objects.forEach((h, i) => {
        generateFlowerCircle(h, 1, objects[i + 1]);
    });

    shrinkingObjects.forEach((h, i) => {
        const size = (1 - (i + 1) / shrinkingObjects.length) * 0.4 + 0.6;
        generateFlowerCircle(h, size, shrinkingObjects[i + 1]);
    });

    sb.append([storyboard]);
};

export const generateFlowerCircle = (hitObject: HitObject, size: number, nextObject?: HitObject, sb?: Storyboard) => {
    if (!sb) sb = storyboard;
    
    const { StartTime, StoryboardPosition } = hitObject;
    const ARStartTime = StartTime - 600;

    if (nextObject?.StackedPosition.almostEquals(hitObject.StackedPosition, 1))
        size *= 0.75;

    sb
        .loadSprite("sb/circle_border", StoryboardPosition)
        .fadeIn(ARStartTime - 200, ARStartTime)
        .scale(ARStartTime - 200, ARStartTime, size / 2 / 2, size / 2, 1)
        .fadeOut(StartTime, StartTime + 100);

    sb
        .loadSprite("sb/circle.png", StoryboardPosition)
        .initColor(ARStartTime, "213,144,88")
        .scale(ARStartTime, StartTime, 0.1, 0.88 * size);
};
