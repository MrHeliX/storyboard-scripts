import { HitObjectOptions } from "../../../../interfaces";
import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { HitObject } from "../../../beatmap-parser/objects/HitObjects/HitObject";
import { generateSbHitCircle } from "../../storyboard-circle-creator";

const storyboard = new Storyboard();
const start = 56299;
const end = 66966;

const sections = [
    { startTime: 56299, endTime: 58966, startAR: 9, endAR: 7, startCS: 4, endCS: 4 },
    { startTime: 58966, endTime: 59466, startAR: 7, endAR: 7, startCS: 4, endCS: 6 },
    { startTime: 58966, endTime: 60966, startAR: 7, endAR: 7, startCS: 6, endCS: 6 },
    { startTime: 60966, endTime: 62299, startAR: 7, endAR: 5, startCS: 6, endCS: 7 },
    { startTime: 62299, endTime: 63966, startAR: 5, endAR: 5, startCS: 7, endCS: 7 },
    { startTime: 63966, endTime: 66966, startAR: 5, endAR: 10, startCS: 7, endCS: 4 }
];

export const arabesqueGenerateARSection = (sb: Storyboard, beatmap: Beatmap) => {
    beatmap.HitObjects
        .filter(h => h.StartTime >= start && h.StartTime <= end)
        .forEach((h, i) => {
            generateCircle(h, beatmap);
        });

    sb.append([storyboard]);
};

const generateCircle = (h: HitObject, beatmap: Beatmap) => {
    const section = sections.find(s => s.startTime <= h.StartTime && s.endTime > h.StartTime);
    const percentage = section ? (h.StartTime - section.startTime) / (section.endTime - section.startTime) : 0;
    const options: HitObjectOptions = {
        ar: section ? (section.endAR - section.startAR) * percentage + section.startAR : beatmap.Difficulty.ApproachRate,
        cs: section? (section.endCS - section.startCS) * percentage + section.startCS : beatmap.Difficulty.CircleSize
    };

    storyboard.append([generateSbHitCircle(h, options)]);
};
