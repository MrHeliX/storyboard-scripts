import { Storyboard } from "../../../../storyboard-helpers";
import { Beatmap } from "../../../beatmap-parser/Objects/Beatmap";
import { HitObject } from "../../../beatmap-parser/objects/HitObjects/HitObject";
import { generateFlowerCircle } from "./intro";

const storyboard = new Storyboard();
const start = 34966;
const end = 56299;

const miniGridCoords = { minX: 265, maxX: 375, minY: 185, maxY: 295 };

export const arabesqueGenerateGridSection = (sb: Storyboard, beatmap: Beatmap) => {
    generateGrid();

    beatmap.HitObjects
        .filter(h => h.StartTime >= start && h.StartTime <= end)
        .forEach(h => {
            generateGridCircle(h);
        });

    sb.append([storyboard]);
};

const generateGrid = () => {
    const spacing = 55;
    for (let i = 0; i < 5; i++) {
        storyboard
            .loadSprite("sb/rounded_square.png", { x: (320 - spacing * 2) + spacing * i, y: 240 - spacing * 2 })
            .fadeIn(33633)
            .scale(33633, 34299, 0, 0.5, 1)
            .scale(44299 + 83 * i, 44549 + 83 * 1, 0.5, 0, 1);
    }

    for (let x = 0; x < 5; x++) {
        for (let y = 1; y < 5; y++) {
            storyboard
                .loadSprite("sb/rounded_square.png", { x: (320 - spacing * 2) + spacing * x, y: (240 - spacing * 2) + spacing * y })
                .fadeIn(34966 + 83 * (4 - x))
                .scale(34966 + 83 * (4 - x), 35216 + 83 * (4 - x), 0, 0.5, 1);

            if (x === 0 || x === 4 || y === 4) {
                storyboard.scale(44299 + 83 * (x + y), 44549 + 83 * (x + y), 0.5, 0, 1);
            }
            else {
                storyboard.fadeOut(end);
            }
        }
    }
};

const generateGridCircle = (hitObject: HitObject) => {
    const { StartTime, StoryboardPosition } = hitObject;
    const ARStartTime = StartTime - 600;
    const secondHalf = StartTime >= 45133;

    if (!secondHalf || (StoryboardPosition.x >= miniGridCoords.minX && StoryboardPosition.x <= miniGridCoords.maxX && StoryboardPosition.y >= miniGridCoords.minY && StoryboardPosition.y <= miniGridCoords.maxY)) {
        storyboard
            .loadSprite("sb/rounded_square.png", StoryboardPosition)
            .initColor(ARStartTime, "213,104,88")
            .scale(ARStartTime, StartTime, 0, 0.5)
            .fadeOut(StartTime, StartTime + 100);
    }
    else {
        generateFlowerCircle(hitObject, 0.9, undefined, storyboard);
    }
};
