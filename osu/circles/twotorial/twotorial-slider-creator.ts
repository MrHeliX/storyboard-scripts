import { convertToSBPosition, isDefined } from "../../../helpers";
import { HitObjectOptions, Position } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import { Slider } from "../../beatmap-parser/objects/HitObjects/Slider";
import { PathType } from "../../beatmap-parser/Objects/PathType";
import { Vector2 } from "../../beatmap-parser/objects/Vector2";
import { generateSbHitCircle, getStoryboardValues } from "../../circles/storyboard-circle-creator";
import { calculateRotation, getSliderPathWithRepeats } from "../../sliders/storyboard-slider-creator";
import { TwoTorialSection } from "./main";
import { generateSbHitCircleTwoTorial } from "./twotorial-circle-creator";

export const generateSbSliderTwoTorial = (slider: Slider, delay: number, section: TwoTorialSection, options?: HitObjectOptions): Storyboard => {
    const sb = new Storyboard();

    generateSliderPath(sb, slider, delay, section.go, options);
    generateSliderHead(sb, slider, delay, section, options);
    generateSliderBall(sb, slider, delay);
    generateFollowCircle(sb, slider, delay);

    if (slider.RepeatCount > 0) {
        generateReturnArrows(sb, slider, options);
        generateReturnArrows(sb, slider, options, delay);
    }

    generateHitsounds(sb, slider);

    return sb;
};

const generateSliderHead = (sb: Storyboard, slider: Slider, delay: number, section: TwoTorialSection, options?: HitObjectOptions): void => {
    slider.HeadCircle.ObjectNumber = slider.ObjectNumber;
    sb.append([
        generateSbHitCircleTwoTorial(slider.HeadCircle, delay, section, options)
    ]);
};

const generateSliderPath = (sb: Storyboard, slider: Slider, delay: number, go: number, options?: HitObjectOptions): void => {
    const { csFactors, startTime, endTime } = getStoryboardValues(slider, options);

    let sliderPath: Vector2[] = slider.Path.calculatedPath;

    if (slider.Path.pathType === PathType.Linear) {
        const sectionCount = Math.round(slider.Path.expectedDistance / 10);
        sliderPath = Array.from(Array(sectionCount + 1)).map((_, i) => {
            return slider.Path.calculatedPath[1].subtract(slider.Path.calculatedPath[0]).scale(i / sectionCount);
        });
    }
    else if (slider.Path.hasSubPaths) {
        sliderPath = [];
        const subPaths = [slider.Path.calculatedPath.slice(0, 2), slider.Path.calculatedPath.slice(1, 3)];
        subPaths.forEach((subPath, i) => {
            const sectionCount = Math.round((slider.Path.cumulativeLength[i + 1] - slider.Path.cumulativeLength[i]) / 10);
            const newPath = Array.from(Array(sectionCount + 1)).map((_, i) => {
                if (i > sectionCount) return null;
                return subPath[1].subtract(subPath[0]).scale(i / sectionCount).add(subPath[0]);
            }).filter(isDefined);

            sliderPath.push(...newPath);
        });
    }

    const delayedEndTime = Math.round(slider.EndTime + delay);

    sliderPath.forEach(step => {
        const sbPos = convertToSBPosition(slider.Position.add(step));
        sb
            .loadSprite("sb/hitcircle.png", sbPos)
            .initColor(1, "255,80,80")
            .scale(startTime, endTime, 0, csFactors.decreasingHitCircleFactor, 1)
            .fade(startTime, endTime)
            .fade(delayedEndTime, delayedEndTime + 200, 1, 0);
    });
};

const generateSliderBall = (sb: Storyboard, slider: Slider, delay: number): void => {
    sb
        .loadAnimation("sliderb.png", slider.StoryboardPosition, 10, 10)
        .scale(Math.round(slider.StartTime), null, 1, 0.5694545)
        .fadeIn(Math.round(slider.StartTime));

    generateSliderMovingObject(sb, slider);

    sb
        .loadAnimation("sliderb.png", slider.StoryboardPosition, 10, 10)
        .scale(Math.round(slider.StartTime + delay), null, 1, 0.5694545)
        .fadeIn(Math.round(slider.StartTime + delay));

    generateSliderMovingObject(sb, slider, delay);
};

const generateFollowCircle = (sb: Storyboard, slider: Slider, delay: number): void => {
    sb
        .loadSprite("sliderfollowcircle.png", slider.StoryboardPosition)
        .initScale(slider.StartTime, 0.5694545)
        .fadeIn(slider.StartTime);

    generateSliderMovingObject(sb, slider);

    sb
        .loadSprite("sliderfollowcircle.png", slider.StoryboardPosition)
        .initScale(slider.StartTime + delay, 0.5694545)
        .fadeIn(slider.StartTime + delay);

    generateSliderMovingObject(sb, slider, delay);
};

const generateSliderMovingObject = (sb: Storyboard, slider: Slider, delay: number = 0): void => {
    const pathWithRepeats = getSliderPathWithRepeats(slider);

    const duration = slider.EndTime - slider.StartTime;
    const interval = pathWithRepeats.length > 1 ? duration / (pathWithRepeats.length - 1) : 0;

    let previousPos: Position;
    let previousRotation: number = 0;

    pathWithRepeats.forEach((step, index) => {
        const sbPos = convertToSBPosition(slider.Position.add(step));
        if (!previousPos) {
            previousPos = sbPos;
            return;
        }

        const { rotation, shouldFlip } = calculateRotation(sbPos, previousPos, false);

        const startTime = Math.round(slider.StartTime + delay + ((index - 1) * interval));
        const endTime = Math.round(slider.StartTime + delay + (index * interval));

        sb
            .move(startTime, endTime, previousPos, sbPos)
            .rotate(startTime, endTime, previousRotation, rotation);

        if (shouldFlip)
            sb.flip(startTime, endTime, "H");

        previousPos = sbPos;
        previousRotation = rotation;
    });
};

const generateReturnArrows = (sb: Storyboard, slider: Slider, options?: HitObjectOptions, delay: number = 0): void => {
    const { csFactors, startTime, endTime } = getStoryboardValues(slider, options);

    const sliderDuration = slider.EndTime - slider.StartTime;
    const durationWithoutRepeats = Math.round(sliderDuration / (slider.RepeatCount + 1));

    const repeatsOnTail = Math.ceil(slider.RepeatCount / 2);
    const repeatsOnHead = Math.floor(slider.RepeatCount / 2);

    const tailRotation = calculateRotation(slider.HeadCircle.Position.add(slider.Path.calculatedPath[slider.Path.calculatedPath.length - 2]), slider.TailCircle.Position, false);
    const headRotation = calculateRotation(slider.HeadCircle.Position.add(slider.Path.calculatedPath[1]), slider.HeadCircle.Position, false);

    sb
        .loadSprite("reversearrow.png", slider.TailCircle.StoryboardPosition)
        .initScale(1, csFactors.decreasingHitCircleFactor)
        .initRotate(1, tailRotation.rotation);

    if (tailRotation.shouldFlip)
        sb.flip(1, 1, "H");

    sb
        .fadeIn(startTime + delay, endTime + delay)
        .fadeOut(slider.StartTime + delay + durationWithoutRepeats);

    Array.from(Array(repeatsOnTail - 1)).forEach((_, i) => {
        const fadeInTime = Math.round(slider.StartTime + delay + (i * 2 + 1) * durationWithoutRepeats);
        const fadeOutTime = Math.round(fadeInTime + 2 * durationWithoutRepeats);

        sb
            .fadeIn(fadeInTime, fadeInTime + 160)
            .fadeOut(fadeOutTime);
    });

    if (repeatsOnHead === 0)
        return;

    sb
        .loadSprite("reversearrow.png", slider.HeadCircle.StoryboardPosition)
        .initScale(1, csFactors.decreasingHitCircleFactor)
        .initRotate(1, headRotation.rotation);

    if (headRotation.shouldFlip)
        sb.flip(1, 1, "H");

    Array.from(Array(repeatsOnHead)).forEach((_, i) => {
        const fadeInTime = Math.round(slider.StartTime + delay + (i * 2) * durationWithoutRepeats);
        const fadeOutTime = Math.round(fadeInTime + 2 * durationWithoutRepeats);

        sb
            .fadeIn(fadeInTime, fadeInTime + 160)
            .fadeOut(fadeOutTime);
    });
};

const generateHitsounds = (sb: Storyboard, slider: Slider): void => {
    const sliderDuration = slider.EndTime - slider.StartTime;
    const durationWithoutRepeats = Math.round(sliderDuration / (slider.RepeatCount + 1));
    
    Array.from(Array(slider.RepeatCount)).map((_, i) => {
        sb.loadSound("sb/hitsound.wav", Math.round(slider.StartTime + (i + 1) * durationWithoutRepeats), 75);
    });

    sb.loadSound("sb/hitsound.wav", Math.round(slider.EndTime), 75);
};