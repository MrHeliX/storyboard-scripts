import { parseBeatmapFromFile } from "../../beatmap-parser/beatmap-parser";
import { isSlider } from "../../beatmap-parser/objects/HitObjects/HitObject";
import fs from "fs";
import * as clipboardy from "clipboardy";
import { twoTorialSections } from "./main";

const generateTimingPoints = async () => {
    const filePath = `O:\\osu!\\Songs\\beatmap-637850571148593558-PHQUASE vs DJ TOTTO - TWO-TORIAL (MXM effect)\\a - t (Mr HeliX) [dd nerfed 2].osu`;
    const parsedBeatmap = await parseBeatmapFromFile(filePath);

    const timingPointsSource = fs.readFileSync("./timing-points.txt").toString().split("\r\n");
    const parsedTimingPoints = parseTimingPoints(timingPointsSource);

    const newTimingPoints: TimingPoint[] = [];

    parsedBeatmap.HitObjects.forEach((hitObject, i) => {
        const twoTorialSection = twoTorialSections.find(s => hitObject.StartTime >= s.listen && hitObject.StartTime < s.go);
        const delay = !!twoTorialSection ? twoTorialSection.go - twoTorialSection.listen : 0;

        const previousObject = parsedBeatmap.HitObjects[i - 1];
        const endTimePreviousObject = !!previousObject
            ? (isSlider(previousObject) ? previousObject.EndTime : previousObject.StartTime)
            : 0;

        const time = (endTimePreviousObject + hitObject.StartTime) / 2 + delay;
        const previousTimingPoint = parsedTimingPoints.filter(t => t.time < time).pop();
        if (!previousTimingPoint) return;
        const sampleIndex = i % 10 + 1;

        newTimingPoints.push({
            ...previousTimingPoint,
            time,
            sampleIndex,
            isUninherited: false
        });

        if (isSlider(hitObject)) {
            const start = hitObject.RepeatCount > 0
                ? hitObject.Duration / (hitObject.RepeatCount + 1) + hitObject.StartTime - 10
                : hitObject.EndTime - 10;

            newTimingPoints.push({
                ...previousTimingPoint,
                time: start + delay,
                sampleIndex: 11,
                isUninherited: false
            });
        }
    });

    const allTimingPoints = parsedTimingPoints.concat(newTimingPoints).sort((a, b) => a.time - b.time).map(t => {
        return `${t.time},${t.beatLength},${t.meter},${t.sampleSet},${t.sampleIndex},${t.volume},${Number(t.isUninherited)},${t.effects}`;
    });
    clipboardy.writeSync(allTimingPoints.join("\r\n"));
    console.log("Timing points copied to clipboard");
};

interface TimingPoint {
    time: number,
    beatLength: number,
    meter: number,
    sampleSet: number,
    sampleIndex: number,
    volume: number,
    isUninherited: boolean,
    effects: number
};

const parseTimingPoints = (source: string[]): TimingPoint[] => {
    return source.map(s => {
        const [time, beatLength, meter, sampleSet, sampleIndex, volume, isInherited, effects] = s.split(",");
        return {
            time: +time,
            beatLength: +beatLength,
            meter: +meter,
            sampleSet: +sampleSet,
            sampleIndex: +sampleIndex,
            volume: +volume,
            isUninherited: Boolean(+isInherited),
            effects: +effects
        };
    });
};

generateTimingPoints();