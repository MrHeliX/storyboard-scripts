import { HitObjectOptions } from '../../../interfaces';
import { Storyboard } from '../../../storyboard-helpers';
import { HitCircle } from '../../beatmap-parser/objects/HitObjects/HitCircle';
import { TwoTorialSection } from './main';

const colors = ["255,80,80", "18,124,255"];

export const generateSbHitCircleTwoTorial = (hitObject: HitCircle, delay: number, section: TwoTorialSection, options?: HitObjectOptions): Storyboard => {
    const sb = new Storyboard();

    getHitCircleSB(sb, hitObject, delay, section.go, options);
    getHitCircleOverlaySB(sb, hitObject, delay, options);
    getApproachCircleSB(sb, hitObject, options);
    getHitCircleNumberSB(sb, hitObject, delay, options);
    getHitCircleSound(sb, hitObject);

    return sb;
};

export const getStoryboardValues = (hitObject: HitCircle, options?: HitObjectOptions) => {
    const duration = getMsFromAr(options?.ar);
    return {
        csFactors: getPixelsFromCs(options?.cs),
        startTime: hitObject.StartTime - duration,
        endTime: hitObject.StartTime
    };
};

const getHitCircleSB = (sb: Storyboard, hitObject: HitCircle, delay: number, go: number, options?: HitObjectOptions) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject, options);
    const endTimeWithDelay = endTime + delay;
    sb
        .loadSprite("hitcircle.png", hitObject.StoryboardPosition)
        .initColor(1, "108,108,108")
        .scale(startTime, endTime, 0, csFactors.decreasingHitCircleFactor, 1)
        .fade(startTime, endTime)
        .color(go - 282, go - 100, "108,108,108", colors[1])
        .scale(endTimeWithDelay, endTimeWithDelay + 50, csFactors.decreasingHitCircleFactor, csFactors.increasingHitCircleFactor)
        .fade(endTimeWithDelay, endTimeWithDelay + 200, 1, 0);
};

const getHitCircleOverlaySB = (sb: Storyboard, hitObject: HitCircle, delay: number, options?: HitObjectOptions) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject, options);
    const endTimeWithDelay = endTime + delay;
    sb
        .loadSprite("hitcircleoverlay.png", hitObject.StoryboardPosition)
        .fade(startTime, endTime)
        .scale(startTime, endTime, 0, csFactors.decreasingHitCircleFactor, 1)
        .scale(endTimeWithDelay, endTimeWithDelay + 50, csFactors.decreasingHitCircleFactor, csFactors.increasingHitCircleFactor)
        .fade(endTimeWithDelay, endTimeWithDelay + 200, 1, 0);
};

const getApproachCircleSB = (sb: Storyboard, hitObject: HitCircle, options?: HitObjectOptions) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject, options);
    sb
        .loadSprite("approachcircle.png", hitObject.StoryboardPosition)
        .initColor(1, "108,108,108")
        .fade(startTime, endTime)
        .scale(startTime, endTime, csFactors.increasingApproachRateFactor, csFactors.decreasingApproachRateFactor);
};

const getHitCircleNumberSB = (sb: Storyboard, hitObject: HitCircle, delay: number, options?: HitObjectOptions) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject, options);
    const endTimeWithDelay = endTime + delay;
    sb
        .loadSprite(`default-${hitObject.ObjectNumber}.png`, hitObject.StoryboardPosition)
        .scale(startTime, endTime, 0, csFactors.decreasingNumberFactor, 1)
        .rotate(startTime, endTime, -1, 0, 1)
        .fade(startTime, endTime)
        .fadeOut(endTimeWithDelay);
};

const getHitCircleSound = (sb: Storyboard, hitObject: HitCircle) => {
    sb.loadSound("sb/hitsound.wav", hitObject.StartTime, 75);
};

const getMsFromAr = (ar: number): number => {
    return ar <= 5
        ? 1800 - (ar * 120)
        : 1200 - ((ar - 5) * 150);
};

// const getMsFromAr = (ar?: number) => {
//     switch (ar) {
//         case 0:
//             return 1800;
//         case 1:
//             return 1680;
//         case 2:
//             return 1560;
//         case 3:
//             return 1440;
//         case 4:
//             return 1320;
//         case 5:
//             return 1200;
//         case 6:
//             return 1050;
//         case 7:
//             return 900;
//         case 8:
//             return 750;
//         case 8.5:
//             return 675;
//         case 9:
//             return 600;
//         case 9.5:
//             return 525;
//         case 10:
//             return 450;
//         case 10.33:
//             return 400;
//         case 10.5:
//             return 375;
//         case 11:
//             return 300;
//         case 11.5:
//             return 225;
//         case 12:
//             return 150;
//         default:
//             return 600; //AR 9
//     };
// };

const getPixelsFromCs = (cs: number = 4) => {
    if (cs < 0)
        return {
            decreasingHitCircleFactor: 0.5752727,
            increasingHitCircleFactor: 0.75,
            decreasingApproachRateFactor: 0.5896,
            increasingApproachRateFactor: 2.15,
            decreasingNumberFactor: 0.45
        };

    let radius = 0;
    switch (cs) {
        case 4:
            radius = 36.48;
            break;
        case 4.5:
            radius = 34.25;
            break;
        case 5:
            radius = 32.01;
            break;
        case 5.5:
            radius = 29.77;
            break;
        case 6:
            radius = 27.53;
            break;
        case 6.5:
            radius = 25.29;
            break;
        case 7:
            radius = 23.05;
            break;
        case 8:
            radius = 18.57;
            break;
        case 9:
            radius = 14.09;
            break;
        case 10:
            radius = 9.6;
            break;
        default:
            radius = 36.48;
    };

    const diameter = radius * 2;
    return {
        decreasingHitCircleFactor: diameter / 126.82680752971590690814982181494,
        increasingHitCircleFactor: (diameter / 126.82680752971590690814982181494) * 1.3037295181919809509472637933279,
        decreasingApproachRateFactor: diameter / 123.7449118046132971506105834464,
        increasingApproachRateFactor: (diameter / 123.7449118046132971506105834464) * 3.646540027137042062415196743555,
        decreasingNumberFactor: diameter / 162.13333333333333333333333333333
    };
};
