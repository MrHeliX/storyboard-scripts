import clipboardy from "clipboardy";
import { HitObjectOptions } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import { parseBeatmapFromFile } from "../../beatmap-parser/beatmap-parser";
import { Beatmap } from "../../beatmap-parser/Objects/Beatmap";
import { HitObject, isCircle, isSlider } from "../../beatmap-parser/objects/HitObjects/HitObject";
import { generateSbSlider } from "../../sliders/storyboard-slider-creator";
import { generateSbHitCircle } from "../storyboard-circle-creator";
import { generateSbHitCircleTwoTorial } from "./twotorial-circle-creator";
import { generateSbSliderTwoTorial } from "./twotorial-slider-creator";

const storyboard = new Storyboard();

export interface TwoTorialSection {
    listen: number,
    go: number
};

const arSections = {
    slowStart: 28217,
    slowEnd: 32653,
    fastStart: 32724,
    fastEnd: 36456,
    slowAR: 7,
    fastAR: 10
};

const csSections = {
    start: 77794,
    end: 86879,
    csStart: 4,
    csEnd: 6.5
};

export const twoTorialSections: TwoTorialSection[] = [
    { listen: 50752, go: 53006 },
    { listen: 55259, go: 57513 },
    { listen: 59766, go: 64273 },
    { listen: 68780, go: 73287 },
    { listen: 112724, go: 117237 }
];

const generateStoryboard = async () => {
    generateBlocker();

    const filePath = `O:\\osu!\\Songs\\beatmap-637850571148593558-PHQUASE vs DJ TOTTO - TWO-TORIAL (MXM effect)\\a - t (Mr HeliX) [dd nerfed 2].osu`;
    const parsedBeatmap = await parseBeatmapFromFile(filePath);
    parsedBeatmap.HitObjects.forEach((hitObject, i) => {
        const options: HitObjectOptions = {
            ar: parsedBeatmap.Difficulty.ApproachRate,
            cs: parsedBeatmap.Difficulty.CircleSize
        };

        if (hitObject.StartTime >= arSections.slowStart && hitObject.StartTime < arSections.fastEnd)
            generateARGimmick(parsedBeatmap, hitObject);
        else if (hitObject.StartTime >= csSections.start && hitObject.StartTime < csSections.end)
            generateCSGimmick(parsedBeatmap, hitObject);
        else if (twoTorialSections.some(s => hitObject.StartTime >= s.listen && hitObject.StartTime < s.go))
            generateTwoTorialGimmick(parsedBeatmap, hitObject);
        else {
            if (isCircle(hitObject))
                storyboard.append([generateSbHitCircle(hitObject, options)]);
            else if (isSlider(hitObject))
                storyboard.append([generateSbSlider(hitObject, options)]);
        }

        generateHitBurst(hitObject, i % 10 + 1);
    });

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const generateBlocker = (): void => {
    storyboard
        .loadSprite("firefox_2022-04-09_17-18-49.jpg")
        .initScale(1, 0.45)
        .fadeInIncreasing(2371, 3216)
        .fadeOut(6103)
        .fadeInIncreasing(7089, 8568)
        .fadeOut(10541)
        .fadeInIncreasing(46245, 49062)
        .fadeOut(77513, 77794);

    storyboard
        .loadSprite("sb/zwart.jpg")
        .initScale(1, 20)
        .fadeIn(0, 1)
        .fadeOutDecreasing(2301, 3427, 1, 0.5)
        .fadeInIncreasing(3427, 4554, 0.5, 1)
        .fadeOutDecreasing(6244, 6385)
        .fadeInIncreasing(7934, 9765)
        .fadeOutIncreasing(45963, 46245, 1, 0.25)
        .fadeInIncreasing(49625, 50470, 0.25, 0.8)
        .fadeIn(77513, 77794, 0.8, 1)
        .fadeOutDecreasing(123146, 124555);
};

const generateARGimmick = (beatmap: Beatmap, hitObject: HitObject): void => {
    let ar: number;
    if (hitObject.StartTime < arSections.slowEnd) {
        const slowDuration = arSections.slowEnd - arSections.slowStart;
        const arDifference = beatmap.Difficulty.ApproachRate - arSections.slowAR;
        const percentage = (hitObject.StartTime - arSections.slowStart) / slowDuration;
        ar = beatmap.Difficulty.ApproachRate - (arDifference * percentage);
    }
    else if (hitObject.StartTime < arSections.fastEnd) {
        const fastDuration = arSections.fastEnd - arSections.fastStart;
        const arDifference = arSections.fastAR - arSections.slowAR;
        const percentage = (hitObject.StartTime - arSections.fastStart) / fastDuration;
        ar = arSections.slowAR + (arDifference * percentage);
    }
    else
        ar = beatmap.Difficulty.ApproachRate;

    const options: HitObjectOptions = {
        ar,
        cs: beatmap.Difficulty.CircleSize
    };

    if (isCircle(hitObject))
        storyboard.append([generateSbHitCircle(hitObject, options)]);
    else if (isSlider(hitObject))
        storyboard.append([generateSbSlider(hitObject, options)]);
};

const generateCSGimmick = (beatmap: Beatmap, hitObject: HitObject): void => {
    const duration = csSections.end - csSections.start;
    const percentage = (hitObject.StartTime - csSections.start) / duration;
    const csDifference = csSections.csEnd - csSections.csStart;

    const options: HitObjectOptions = {
        ar: beatmap.Difficulty.ApproachRate,
        cs: csSections.csStart + percentage * csDifference
    };

    if (isCircle(hitObject))
        storyboard.append([generateSbHitCircle(hitObject, options)]);
    else if (isSlider(hitObject))
        storyboard.append([generateSbSlider(hitObject, options)]);
};

const generateTwoTorialGimmick = (beatmap: Beatmap, hitObject: HitObject): void => {
    const section = twoTorialSections.find(s => hitObject.StartTime >= s.listen && hitObject.StartTime < s.go);
    const delay = section.go - section.listen;
    const options: HitObjectOptions = {
        ar: 10,
        cs: beatmap.Difficulty.CircleSize
    };

    if (isCircle(hitObject))
        storyboard.append([generateSbHitCircleTwoTorial(hitObject, delay, section, options)]);
    else
        storyboard.append([generateSbSliderTwoTorial(hitObject, delay, section, options)]);
};

const generateHitBurst = (hitObject: HitObject, sampleSetIndex: number): void => {
    // 300 zone: +/- 38 ms
    // 100 zone: +/- 84 ms
    // 50 zone: +/- 130 ms
    const twoTorialSection = twoTorialSections.find(s => hitObject.StartTime >= s.listen && hitObject.StartTime < s.go);
    const delay = !!twoTorialSection ? twoTorialSection.go - twoTorialSection.listen : 0;

    if (isSlider(hitObject)) {
        const sliderTime = Math.round(hitObject.EndTime - hitObject.StartTime);
        const triggerEndTime = Math.min(hitObject.RepeatCount > 0
            ? hitObject.Duration / (hitObject.RepeatCount + 1) + hitObject.StartTime - 1
            : hitObject.EndTime - 1, hitObject.StartTime + 84);

        storyboard
            .loadAnimation("hit100-.png", hitObject.TailCircle.StoryboardPosition, 1, 10)
            .trigger(`HitSound${sampleSetIndex}`, hitObject.StartTime - 84 + delay, hitObject.StartTime - 38 + delay)
            .fade(sliderTime, sliderTime + 100)
            .scale(sliderTime, sliderTime + 100, 0.3, 0.7)
            .scale(sliderTime + 120, sliderTime + 140, 0.7, 0.5)
            .scale(sliderTime + 140, sliderTime + 170, 0.5, 0.6)
            .fade(sliderTime + 500, sliderTime + 1100, 1, 0)
            .endTrigger();

        if (triggerEndTime > hitObject.StartTime + 38) {
            storyboard
                .loadAnimation("hit100-.png", hitObject.TailCircle.StoryboardPosition, 1, 10)
                .trigger(`HitSound${sampleSetIndex}`, hitObject.StartTime + 38 + delay, Math.round(triggerEndTime) + delay)
                .fade(sliderTime, sliderTime + 100)
                .scale(sliderTime, sliderTime + 100, 0.3, 0.7)
                .scale(sliderTime + 120, sliderTime + 140, 0.7, 0.5)
                .scale(sliderTime + 140, sliderTime + 170, 0.5, 0.6)
                .fade(sliderTime + 500, sliderTime + 1100, 1, 0)
                .endTrigger();
        }
    }
    else if (isCircle(hitObject)) {
        storyboard
            .loadAnimation("hit100-.png", hitObject.StoryboardPosition, 1, 10)
            .trigger(`HitSound${sampleSetIndex}`, hitObject.StartTime - 84 + delay, hitObject.StartTime - 38 + delay)
            .fade(0, 100)
            .scale(0, 100, 0.3, 0.7)
            .scale(120, 140, 0.7, 0.5)
            .scale(140, 170, 0.5, 0.6)
            .fade(500, 1100, 1, 0)
            .endTrigger();

        storyboard
            .loadAnimation("hit100-.png", hitObject.StoryboardPosition, 1, 10)
            .trigger(`HitSound${sampleSetIndex}`, hitObject.StartTime + 38 + delay, hitObject.StartTime + 84 + delay)
            .fade(0, 100)
            .scale(0, 100, 0.3, 0.7)
            .scale(120, 140, 0.7, 0.5)
            .scale(140, 170, 0.5, 0.6)
            .fade(500, 1100, 1, 0)
            .endTrigger();
    }
};

generateStoryboard();