import { isSlider, parseHitObjects } from "../../hitObjectsParser";
import { HitCircle } from "../../interfaces";
import { Storyboard } from "../../storyboard-helpers";
import * as clipboardy from "clipboardy";

const storyboard = new Storyboard();

const generateHitCircles = async () => {
    const hitObjects = (await parseHitObjects(`${__dirname}/batting-show.txt`)) as HitCircle[];

    hitObjects.forEach(hitObject => {
        if (isSlider(hitObject)) return;
        // getRotatingHitCircleSB(hitObject);
        // getHitCircleSB(hitObject);
    });

    generateSpam();

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const colorOptions = [
    ["255,0,0", "255,240,0", "255,144,0"],
    ["255,240,0", "255,144,0", "255,0,0"],
    ["255,144,0", "255,0,0", "255,240,0"]
];

const getColors = () => {
    const seed = Math.round(Math.random() * 2);
    const colorTransitions = colorOptions[seed];
    return colorTransitions;
};

const getHitCircleSB = (hitObject: HitCircle) => {
    storyboard
        .loadSprite("sb/hitcircle.png", hitObject.sbPosition)
        .fade(hitObject.startTime - 750, hitObject.startTime);
};

const getRotatingHitCircleSB = (hitObject: HitCircle) => {
    const { sbPosition, startTime } = hitObject;
    const startRotation = Math.random() * Math.PI;
    const directionSeed = Math.random();
    const direction = directionSeed >= 0.5 ? 2 : -2;
    storyboard
        .loadSprite("sb/hitcircle.png", sbPosition)
        .fade(startTime - 750, startTime)
        .rotate(startTime - 750, startTime, startRotation, startRotation + direction );
};

const getHitCircleSBColor = (hitObject: HitCircle) => {
    const colors = getColors();
    const { sbPosition, startTime } = hitObject;
    storyboard
        .loadSprite("sb/hitcircle-grey.png", sbPosition)
        .fade(startTime - 750, startTime)
        .color(startTime - 750, startTime - 500, "255,255,255", colors[0])
        .color(startTime - 550, startTime - 250, colors[0], colors[1])
        .color(startTime - 350, startTime - 0, colors[1], colors[2])
};

const generateSpam = () => {
    const startTime = 81061;
    const endTime = 92341;
    for (let i = startTime; i < endTime - 750; i += 500) {
        for (let j = 0; j < 15; j++) {
            const position = { x: Math.round(Math.random() * 640), y: Math.round(Math.random() * 480) };
            const startRotation = Math.random() * Math.PI;
            const directionSeed = Math.random();
            const direction = directionSeed >= 0.5 ? 2 : -2;
            storyboard
                .loadSprite("sb/hitcircle.png", position)
                .fade(i, i + 750)
                // .rotate(i, i + 750, startRotation, startRotation + direction);
        }
    }
};

generateHitCircles();