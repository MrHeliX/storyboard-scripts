import { isSlider, parseHitObjects } from "../../hitObjectsParser";
import { HitCircle, HitObjectOld } from "../../interfaces";
import * as clipboardy from "clipboardy";
import * as fs from "fs";
import { Storyboard } from "../../storyboard-helpers";

export const convertToInvisibleCircles = async (circles?: HitObjectOld[]): Promise<string[]> => {
    const hitObjects = circles ?? (await parseHitObjects(`${__dirname}/math/invisible_circles.txt`));
    const allHitObjects = await parseHitObjects(`${__dirname}/math/circles.txt`);

    const converted = allHitObjects.map(h => {
        // if (isSlider(h) || !hitObjects.some(i => i.startTime === h.startTime)) return h.rawString;
        return `${h.position.x},${h.position.y},${h.startTime + 46},6,0,L,1,2000,0|0,0:0|2:0,0:0:0:0:`
    });

    clipboardy.writeSync(converted.join("\r\n"));
    console.log("Invisible circles copied to clipboard");
    return converted;
};

export const convertToInvisibleCirclesMath = async (circles?: HitObjectOld[]): Promise<string[]> => {
    const hitObjects = circles ?? (await parseHitObjects(`${__dirname}/math/invisible_circles.txt`));

    const converted = hitObjects.map(h => {
        return `${h.position.x},${h.position.y},${h.startTime + 46},6,0,L,1,2000,0|0,0:0|2:0,0:0:0:0:`
    });

    return converted;
};

export const generateInvisibleCirclesTimingPointsMath = async (circles: HitObjectOld[]): Promise<string> => {
    const hitObjects = circles
    const timingPoints = (await fs.readFileSync(`${__dirname}/math/timing_points.txt`)).toString();

    const extraTimingPoints = hitObjects.map(h => {
        return `${h.startTime + 36},-100,4,2,1,0,0,0\r\n${h.startTime + 56},-100,4,2,1,70,0,0`;
    }).join("\r\n");

    const results = timingPoints.concat("\r\n", extraTimingPoints);
    return results;
};

export const generateInvisibleCirclesTimingPoints = async (): Promise<string> => {
    const hitObjects = await parseHitObjects(`${__dirname}/math/invisible_circles.txt`);
    const timingPoints = (await fs.readFileSync(`${__dirname}/math/timing_points.txt`)).toString();

    const extraTimingPoints = hitObjects.map(h => {
        return `${h.startTime + 65},-100,4,2,1,0,0,0\r\n${h.startTime + 85},-100,4,2,1,70,0,0`;
    }).join("\r\n");

    const results = timingPoints.concat("\r\n", extraTimingPoints);
    clipboardy.writeSync(results);
    console.log("Timing points copied to clipboard");
    return results;
};

export const generateInvisibleCirclesHitSounds = async (): Promise<void> => {
    const hitObjects = await parseHitObjects(`${__dirname}/math/invisible_circles.txt`);
    const sb = new Storyboard();

    hitObjects.forEach(hitObject => {
        sb.loadSound("sb/hitsound.wav", hitObject.startTime, 70);
    });

    clipboardy.writeSync(sb.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

// convertToInvisibleCircles();
// generateInvisibleCirclesTimingPoints();
generateInvisibleCirclesHitSounds();