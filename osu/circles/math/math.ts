export enum MathType {
    ADDITION = "add",
    SUBTRACTION = "subtract",
    MULTIPLICATION = "multiply",
    DIVISION = "divide",
    LESS = "less",
    GREATER = "greater",
    SQUARED = "squared",
    ROOT = "root"
};

export const calculateAnswer = (values: number[], type: MathType): number => {
    const [value1, value2, value3] = values;
    switch (type) {
        case MathType.ADDITION: return value1 + value2 + (value3 ?? 0);
        case MathType.SUBTRACTION: return value1 - value2 - (value3 ?? 0);
        case MathType.MULTIPLICATION: return value1 * value2 * (value3 ?? 1);
        case MathType.DIVISION: return value1 / value2 / (value3 ?? 1);
        default: return 0;
    };
};

export const getCompareAnswer = (value1: number, value2: number, type: MathType): boolean => {
    switch (type) {
        case MathType.LESS: return value1 < value2;
        case MathType.GREATER: return value1 > value2;
        default: return false;
    };
};

export const getBoundariesWrongAnswers = (type: MathType, value1: number, value2: number, value3: number = 1): { min: number, max: number } => {
    const [minValue, maxValue] = [Math.min(value1, value2), Math.max(value1, value2)];
    const answer = calculateAnswer([value1, value2, value3], type);
    console.log(value1, value2, value3, answer);

    switch (type) {
        case MathType.ADDITION: return { min: maxValue + 1, max: answer + 5 };
        case MathType.SUBTRACTION: return { min: Math.max(answer - 5, 0), max: maxValue - 1 };
        case MathType.MULTIPLICATION: return { min: value1 * (value2 - 1) * value3, max: Math.min(answer + 50, 999) };
        case MathType.DIVISION: return { min: answer - 5, max: maxValue - 1 }
    }
}

export const shuffle = (array: any[]) => {
    var currentIndex = array.length, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
    }

    return array;
};
