import { Storyboard } from "../../../storyboard-helpers";
import { getBoundariesWrongAnswers, calculateAnswer, MathType, shuffle, getCompareAnswer } from "./math";
import * as clipboardy from "clipboardy";
import { convertToSBPosition } from "../../../helpers";
import { generateBeatmapFile } from "./map-generator";
import { convertToInvisibleCircles, convertToInvisibleCirclesMath, generateInvisibleCirclesTimingPointsMath } from "../invisible-circle-creator";
import { HitObjectOld } from "../../../interfaces";

let storyboard;

const circlePositions = [96, 176, 256, 336, 416];
let correctAnswers: { time: number, x: number, y: number }[] = [];

const generate = async (id?: number) => {
    storyboard = new Storyboard();
    correctAnswers = [];

    const section1 = { start: 4148, end: 14648 };
    const section2 = { start: 16148, end: 26648 };
    const section3 = { start: 28148, end: 38648 };
    const section4 = { start: 40148, end: 49148 };

    const section5 = { start: 51773, end: 57023 };
    const section6 = { start: 57773, end: 63023 };
    const section7 = { start: 63773, end: 69023 };
    const section8 = { start: 69773, end: 75023 };

    let timeStamp = section1.start;
    let previousValues: number[] = [];

    const { ADDITION, SUBTRACTION, MULTIPLICATION } = MathType;

    while (timeStamp <= section1.end) {
        const values = [generateNumber(1, 9), generateNumber(1, 9), generateNumber(1, 9)];

        if (!checkIfDifferentValues(values, previousValues)) continue;
        generateQuestion(ADDITION, values, 5, timeStamp, 3000);
        timeStamp += 3000;
        previousValues = values;
    }

    timeStamp = section2.start;
    while (timeStamp <= section2.end) {
        const values = [generateNumber(5, 9), generateNumber(1, 5), generateNumber(1, 3)];

        if (!checkIfDifferentValues(values, previousValues)) continue;
        generateQuestion(MULTIPLICATION, values, 5, timeStamp, 3000);
        timeStamp += 3000;
        previousValues = values;
    };

    while (timeStamp <= section3.end) {
        const values = [generateNumber(1, 9), generateNumber(1, 5), generateNumber(1, 3)];

        if (!checkIfDifferentValues(values, previousValues)) continue;
        generateQuestion(ADDITION, values, 5, timeStamp, 1500);
        timeStamp += 1500;
        previousValues = values;
    }

    timeStamp = section4.start;
    while (timeStamp <= section4.end) {
        const values = [generateNumber(1, 5), generateNumber(1, 3), generateNumber(1, 2)]

        if (!checkIfDifferentValues(values, previousValues)) continue;
        generateQuestion(MULTIPLICATION, values, 5, timeStamp, 1500);
        timeStamp += 1500;
        previousValues = values;
    }

    timeStamp = section5.start;
    while (timeStamp <= section5.end) {
        const values = [0, 1].map(_ => generateNumber(0, 9));
        if (!checkIfDifferentValues(values, previousValues)) continue;
        generateCompareQuestion(MathType.LESS, values, timeStamp, 750);
        timeStamp += 750;
        previousValues = values;
    }

    timeStamp = section6.start;
    while (timeStamp <= section6.end) {
        const values = [0, 1].map(_ => generateNumber(0, 9));
        if (!checkIfDifferentValues(values, previousValues)) continue;
        generateCompareQuestion(MathType.GREATER, values, timeStamp, 750);
        timeStamp += 750;
        previousValues = values;
    }

    timeStamp = section7.start;
    while (timeStamp <= section7.end) {
        const values = [0, 1].map(_ => generateNumber(0, 9));
        if (!checkIfDifferentValues(values, previousValues)) continue;
        generateCompareQuestion(MathType.LESS, values, timeStamp, 750);
        timeStamp += 750;
        previousValues = values;
    }

    timeStamp = section8.start;
    while (timeStamp <= section8.end) {
        const values = [0, 1].map(_ => generateNumber(0, 9));
        if (!checkIfDifferentValues(values, previousValues)) continue;
        generateCompareQuestion(MathType.GREATER, values, timeStamp, 750);
        timeStamp += 750;
        previousValues = values;
    }

    // Hard question #1
    generateHugeQuestion(79879, 13696);

    // Hard question #2
    generateHugeQuestion(94364, 11266);

    const finalTimings = [
        { start: 106344, end: 107677 },
        { start: 107677, end: 109010 },
        { start: 109010, end: 110307 },
        { start: 110307, end: 111604 },
        { start: 111604, end: 112887 },
        { start: 112887, end: 114170 },
        { start: 114170, end: 115420 },
        { start: 115420, end: 116670 },
    ];

    const finalTimingsHarder = [
        { start: 116670, end: 119131 },
        { start: 119131, end: 121531 },
        { start: 121531, end: 123872 },
        { start: 123872, end: 126202 },
    ];

    finalTimings.forEach(t => {
        let values = [0, 1].map(_ => generateNumber(2, 9));
        while (!checkIfDifferentValues(values, previousValues)) {
            values = [0, 1].map(_ => generateNumber(2, 9));
        }

        generateQuestion(MathType.MULTIPLICATION, values, 5, t.start, t.end - t.start);
        previousValues = values;
    });

    finalTimingsHarder.forEach(t => {
        let values = shuffle([generateNumber(6, 9), generateNumber(1, 3), generateNumber(1, 2)]);
        while (!checkIfDifferentValues(values, previousValues)) {
            values = shuffle([generateNumber(6, 9), generateNumber(1, 3), generateNumber(1, 2)]);
        }
        generateQuestion(MathType.MULTIPLICATION, values, 5, t.start, t.end - t.start);
        previousValues = values;
    });

    const { circles, timingPoints } = await generateCorrectAnswerCircles();

    generateBeatmapFile(storyboard, circles, timingPoints, id);
    // clipboardy.writeSync(storyboard.toStoryboard().concat("\n").concat(circles));
    // console.log("Storyboard copied to clipboard");
};

const generateQuestion = (type: MathType, values: number[], answerCount: number, time: number, duration: number) => {
    const [value1, value2, value3] = values;
    const correctAnswer = calculateAnswer(values, type);
    const { min, max } = getBoundariesWrongAnswers(type, value1, value2, value3);
    const shuffledOptions = getAnswerOptions(answerCount - 1, min, max, correctAnswer);

    const endTime = time + duration;

    if (!value3) {
        storyboard
            .loadSprite(`sb/${value1}.png`, { x: 270, y: 50 })
            .fadeIn(time, time + 200)
            .fadeOut(endTime, endTime + 200);

        storyboard
            .loadSprite(`sb/${type}.png`, { x: 320, y: 50 })
            .fadeIn(time, time + 200)
            .fadeOut(endTime, endTime + 200);

        storyboard
            .loadSprite(`sb/${value2}.png`, { x: 370, y: 50 })
            .fadeIn(time, time + 200)
            .fadeOut(endTime, endTime + 200);
    }
    else {
        storyboard
            .loadSprite(`sb/${value1}.png`, { x: 220, y: 50 })
            .fadeIn(time, time + 200)
            .fadeOut(endTime, endTime + 200);

        storyboard
            .loadSprite(`sb/${type}.png`, { x: 270, y: 50 })
            .fadeIn(time, time + 200)
            .fadeOut(endTime, endTime + 200);

        storyboard
            .loadSprite(`sb/${value2}.png`, { x: 320, y: 50 })
            .fadeIn(time, time + 200)
            .fadeOut(endTime, endTime + 200);

        storyboard
            .loadSprite(`sb/${type}.png`, { x: 370, y: 50 })
            .fadeIn(time, time + 200)
            .fadeOut(endTime, endTime + 200);

        storyboard
            .loadSprite(`sb/${value3}.png`, { x: 420, y: 50 })
            .fadeIn(time, time + 200)
            .fadeOut(endTime, endTime + 200);
    }

    shuffledOptions.forEach((option, i) => {
        const sbPos = convertToSBPosition({ x: circlePositions[i], y: 100 });
        if (option === correctAnswer) correctAnswers.push({ time: endTime, x: circlePositions[i], y: 100 });
        storyboard
            .loadSprite("sb/circle_border.png", sbPos)
            .fadeIn(time, time + 200)
            .fadeOut(endTime, endTime + 200);

        storyboard
            .loadSprite("approachcircle.png", sbPos)
            .initColor(endTime - 525, "255,255,255")
            .fadeIn(endTime - 525, endTime)
            .scale(endTime - 525, endTime, 2.15, 0.5896);


        if (option < 10)
            storyboard
                .loadSprite(`sb/${option}.png`, sbPos)
                .initScale(time, 0.5)
                .fadeIn(time, time + 200)
                .fadeOut(endTime, endTime + 200);
        else if (option < 100) {
            const digit1 = Math.floor(option / 10);
            const digit2 = option - (digit1 * 10);

            storyboard
                .loadSprite(`sb/${digit1}.png`, { x: sbPos.x - 10, y: sbPos.y })
                .initScale(time, 0.5)
                .fadeIn(time, time + 200)
                .fadeOut(endTime, endTime + 200);

            storyboard
                .loadSprite(`sb/${digit2}.png`, { x: sbPos.x + 10, y: sbPos.y })
                .initScale(time, 0.5)
                .fadeIn(time, time + 200)
                .fadeOut(endTime, endTime + 200);
        }
        else {
            const digit1 = Math.floor(option / 100);
            const digit2 = Math.floor((option - (digit1 * 100)) / 10);
            const digit3 = option - (digit1 * 100) - (digit2 * 10);

            storyboard
                .loadSprite(`sb/${digit1}.png`, { x: sbPos.x - 15, y: sbPos.y })
                .initScale(time, 0.3)
                .fadeIn(time, time + 200)
                .fadeOut(endTime, endTime + 200);

            storyboard
                .loadSprite(`sb/${digit2}.png`, { x: sbPos.x, y: sbPos.y })
                .initScale(time, 0.3)
                .fadeIn(time, time + 200)
                .fadeOut(endTime, endTime + 200);

            storyboard
                .loadSprite(`sb/${digit3}.png`, { x: sbPos.x + 15, y: sbPos.y })
                .initScale(time, 0.3)
                .fadeIn(time, time + 200)
                .fadeOut(endTime, endTime + 200);
        }
    });
};

const generateCompareQuestion = (type: MathType, values: number[], time: number, duration: number) => {
    const [value1, value2] = values;
    const correctAnswer = getCompareAnswer(value1, value2, type);

    const endTime = time + duration;
    storyboard
        .loadSprite(`sb/${value1}.png`, { x: 270, y: 150 })
        .fadeIn(time, time + 200)
        .fadeOut(endTime, endTime + 200);

    storyboard
        .loadSprite(`sb/${type}.png`, { x: 320, y: 150 })
        .fadeIn(time, time + 200)
        .fadeOut(endTime, endTime + 200);

    storyboard
        .loadSprite(`sb/${value2}.png`, { x: 370, y: 150 })
        .fadeIn(time, time + 200)
        .fadeOut(endTime, endTime + 200);

    const circlePositions = [176, 336];
    circlePositions.forEach((x, i) => {
        const sbPos = convertToSBPosition({ x, y: 192 });
        const option = i === 0;

        if (correctAnswer === option) correctAnswers.push({ time: endTime, x, y: 192 });

        storyboard
            .loadSprite("sb/circle_border.png", sbPos)
            .fadeIn(time, time + 200)
            .fadeOut(endTime, endTime + 200);

        storyboard
            .loadSprite("approachcircle.png", sbPos)
            .initColor(endTime - 525, "255,255,255")
            .fadeIn(endTime - 525, endTime)
            .scale(endTime - 525, endTime, 2.15, 0.5896);

        storyboard
            .loadSprite(`sb/${option}.png`, sbPos)
            .fadeIn(time, time + 200)
            .fadeOut(endTime, endTime + 200);
    });
};

const generateHugeQuestion = (time: number, duration: number) => {
    // 0:  a * (b + c) * d * (e - f)
    // 1: a * (b + c) - d * (e - f)
    const questionSeed = Math.round(Math.random());

    const numbers = [0, 1, 2, 3, 4, 5].map(_ => generateNumber(1, 9));
    const [a, b, c, d, e, f] = numbers;
    const types = {
        1: MathType.MULTIPLICATION,
        4: MathType.ADDITION,
        7: questionSeed === 0 ? MathType.ADDITION : MathType.SUBTRACTION,
        9: MathType.MULTIPLICATION,
        12: MathType.SUBTRACTION
    };

    const brackets = {
        2: "open",
        6: "close",
        10: "open",
        14: "close"
    };

    const answer = questionSeed === 0
        ? a * (b + c) + d * (e - f)
        : a * (b + c) - d * (e - f);

    if (answer < 0 || answer > 99 || e === f || a === 1 || d === 1) return generateHugeQuestion(time, duration);
    const endTime = time + duration;

    for (let i = 0; i < 17; i++) {
        const x = 35 * i + 57.5;
        const y = 50;

        const type = types[i];
        const bracket = brackets[i];
        const number = !type && !bracket ? numbers.shift() : null;

        const spriteName = type ?? (bracket ? `bracket_${bracket}` : number);

        storyboard
            .loadSprite(`sb/${spriteName}.png`, { x, y })
            .initScale(time, 0.6)
            .fadeIn(time, time + 200)
            .fadeOut(endTime, endTime + 200);
    }

    const answerOptions = getAnswerOptions(4, 0, 100, answer);
    answerOptions.forEach((option, i) => {
        const sbPos = convertToSBPosition({ x: circlePositions[i], y: 116 });
        if (option === answer) correctAnswers.push({ time: endTime, x: circlePositions[i], y: 116 });
        storyboard
            .loadSprite("sb/circle_border.png", sbPos)
            .fadeIn(time, time + 200)
            .fadeOut(endTime, endTime + 200);

        storyboard
            .loadSprite("approachcircle.png", sbPos)
            .initColor(endTime - 525, "255,255,255")
            .fadeIn(endTime - 525, endTime)
            .scale(endTime - 525, endTime, 2.15, 0.5896);

        if (option < 10)
            storyboard
                .loadSprite(`sb/${option}.png`, sbPos)
                .initScale(time, 0.5)
                .fadeIn(time, time + 200)
                .fadeOut(endTime, endTime + 200);
        else if (option < 100) {
            const digit1 = Math.floor(option / 10);
            const digit2 = option - (digit1 * 10);

            storyboard
                .loadSprite(`sb/${digit1}.png`, { x: sbPos.x - 10, y: sbPos.y })
                .initScale(time, 0.5)
                .fadeIn(time, time + 200)
                .fadeOut(endTime, endTime + 200);

            storyboard
                .loadSprite(`sb/${digit2}.png`, { x: sbPos.x + 10, y: sbPos.y })
                .initScale(time, 0.5)
                .fadeIn(time, time + 200)
                .fadeOut(endTime, endTime + 200);
        }
        else {
            const digit1 = Math.floor(option / 100);
            const digit2 = Math.floor((option - (digit1 * 100)) / 10);
            const digit3 = option - digit1 - digit2;

            storyboard
                .loadSprite(`sb/${digit1}.png`, { x: sbPos.x - 15, y: sbPos.y })
                .initScale(time, 0.3)
                .fadeIn(time, time + 200)
                .fadeOut(endTime, endTime + 200);

            storyboard
                .loadSprite(`sb/${digit2}.png`, { x: sbPos.x, y: sbPos.y })
                .initScale(time, 0.3)
                .fadeIn(time, time + 200)
                .fadeOut(endTime, endTime + 200);

            storyboard
                .loadSprite(`sb/${digit3}.png`, { x: sbPos.x + 15, y: sbPos.y })
                .initScale(time, 0.3)
                .fadeIn(time, time + 200)
                .fadeOut(endTime, endTime + 200);
        }
    });
}

const generateCorrectAnswerCircles = async (): Promise<{ circles: string, timingPoints: string }> => {
    const circlesToBeInvisible = correctAnswers.filter((_, i) => i % 4 === 3);
    const circles = correctAnswers.filter(c => !circlesToBeInvisible.some(i => i.time === c.time));
    const normalCircles = circles.map(a => `${a.x},${a.y},${a.time},5,0,0:0:0:0:`);
    const mappedCirclesToBeInvisible = circlesToBeInvisible.map(c => ({ position: { x: c.x, y: c.y }, startTime: c.time } as HitObjectOld));
    const invisibleCircles = await convertToInvisibleCirclesMath(mappedCirclesToBeInvisible);
    const timingPoints = await generateInvisibleCirclesTimingPointsMath(mappedCirclesToBeInvisible);
    return { circles: normalCircles.concat(invisibleCircles).join("\n"), timingPoints };
};

const generateNumber = (min: number, max: number): number => {
    return Math.round(Math.random() * (max - min)) + min;
};

const getAnswerOptions = (count: number, min: number, max: number, answer: number): number[] => {
    const otherOptions: number[] = [];

    let i = 0;
    while (i < count) {
        const option = generateNumber(min, max);
        if (otherOptions.includes(option) || option === answer)
            continue;
        otherOptions.push(option);
        i++;
    }

    return shuffle([...otherOptions, answer])
};

const checkIfDifferentValues = (currentValues: number[], previousValues: number[]): boolean => {
    for (let i = 0; i < currentValues.length; i++) {
        if (currentValues[i] === previousValues[i])
            return false;
    }
    return true;
};


const generateAll = async () => {
    for (let i = 1; i <= 50; i++) {
        await generate(i);
    }
};

generateAll();