import * as fs from "fs";

const getFileName = (id: number): string => `KillerBlood - Run Lads Run (Mr HeliX) [Math Test - Version #${id}].osu`;

const getIds = async () => {
    await fs.writeFileSync("./live/ids.txt", "");
    for (let i = 1; i <= 50; i++) {
        const file = (await fs.readFileSync(`./live/${getFileName(i)}`)).toString().split("\n");
        const idLine = file.find(l => l.includes("BeatmapID"));
        const id = +idLine.split(":")[1];
        await fs.appendFileSync("./live/ids.txt", `#${i}: ${id}\n`);
    }
};

getIds();