import { Position } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";

export type GridItemCoordinate = -2 | -1 | 0 | 1 | 2;
interface GridItem {
    sb: Storyboard,
    originalRow: GridItemCoordinate,
    originalColumn: GridItemCoordinate,
    currentRow: GridItemCoordinate,
    currentColumn: GridItemCoordinate,
    hidden: boolean
};

type Grid = GridItem[];

const coordinates: GridItemCoordinate[] = [-2, -1, 0, 1, 2];
const stepSize = 80;

const grid: Grid = [];
const initialItems = [{ x: -2, y: -1 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: 1, y: 0 }, { x: 2, y: -1 }];

const gridCircleFiles = {
    "-2": "93,69,69",
    "-1": "93,85,69",
    "0": "87,93,69",
    "1": "69,93,75",
    "2": "69,83,93"
};

export const initializeGrid = (): void => {
    coordinates.forEach(x => {
        coordinates.forEach(y => {
            const pos: Position = { x: x * stepSize + 320, y: y * stepSize + 240 };
            const gridItem: GridItem = {
                sb: new Storyboard(),
                currentRow: y,
                currentColumn: x,
                originalRow: y,
                originalColumn: x,
                hidden: false
            };

            const filePostFix = (x + y) % 2 === 0 ? y : "";

            gridItem.sb.loadSprite(`sb/octopus_circle${filePostFix}.png`, pos, "Overlay", "Centre")

            const initialItemIndex = initialItems.findIndex(i => i.x === x && i.y === y);
            if (initialItemIndex > -1) {
                const startTime = 6550 + initialItemIndex * 600 - 75;
                gridItem.sb
                    .scale(startTime, startTime + 200, 0, 0.45, 2)
                    .fadeInDecreasing(startTime, startTime + 200)
                    .fadeOut(120000);
            }
            else {
                gridItem.sb
                    .fadeInIncreasing(26150, 26450)
                    .scale(26150, 26450, 0, 0.45, 1)
                    .fadeOut(120000);
            }

            grid.push(gridItem);
        });
    });
};

export const getGridItem = (x: GridItemCoordinate, y: GridItemCoordinate, originalPosition?: boolean): GridItem => {
    return originalPosition
        ? grid.find(g => g.originalColumn === x && g.originalRow === y)
        : grid.find(g => g.currentColumn === x && g.currentRow === y);
};

const getGridItems = (x: GridItemCoordinate, y: GridItemCoordinate, originalPosition?: boolean): GridItem[] => {
    return originalPosition
        ? grid.filter(g => g.originalColumn === x && g.originalRow === y)
        : grid.filter(g => g.currentColumn === x && g.currentRow === y);
};

const getGridItemsInRow = (row: GridItemCoordinate): GridItem[] => {
    return grid.filter(g => g.currentRow === row);
};

const getGridItemsInColumn = (column: GridItemCoordinate): GridItem[] => {
    return grid.filter(g => g.currentColumn === column);
}

export const getGridItemPosition = (x: GridItemCoordinate, y: GridItemCoordinate): Position => {
    return { x: x * stepSize + 320, y: y * stepSize + 240 };
};

export const getGridStoryboards = (): Storyboard[] => {
    return grid.map(v => v.sb);
};

export const moveRow = (rowNumber: GridItemCoordinate, time: number, direction: "left" | "right", amount: number = 1) => {
    const gridItems = getGridItemsInRow(rowNumber);
    gridItems.forEach(gridItem => {
        const { sb, currentRow, currentColumn } = gridItem;
        const currentPosition = getGridItemPosition(currentColumn, currentRow);

        const newPositionX = direction === "left" ? currentPosition.x - stepSize * amount : currentPosition.x + stepSize * amount;
        // sb.moveXDecreasing(currentPosition.x, newPositionX, time, time + 100);
        sb.moveDecreasing(currentPosition, { x: newPositionX, y: currentPosition.y }, time, time + 100);
        gridItem.currentColumn = currentColumn + amount * (direction === "left" ? -1 : 1) as GridItemCoordinate;
    });
};

export const moveColumn = (colNumber: GridItemCoordinate, time: number, direction: "up" | "down", amount: number = 1) => {
    const gridItems = getGridItemsInColumn(colNumber);
    gridItems.forEach(gridItem => {
        const { sb, currentRow, currentColumn } = gridItem;
        const currentPosition = getGridItemPosition(currentColumn, currentRow);

        const newPositionY = direction === "up" ? currentPosition.y - stepSize * amount : currentPosition.y + stepSize * amount;
        // sb.moveYDecreasing(currentPosition.y, newPositionY, time, time + 100);
        sb.moveDecreasing(currentPosition, { x: currentPosition.x, y: newPositionY }, time, time + 100);
        gridItem.currentRow = currentRow + amount * (direction === "up" ? -1 : 1) as GridItemCoordinate;
    });
};

export const moveGridItem = (x: GridItemCoordinate, y: GridItemCoordinate, time: number, newX: number, newY: number, amount: number = 1, speed: number = 100, original?: boolean) => {
    const gridItem = getGridItem(x, y, original);
    const { sb, currentRow, currentColumn } = gridItem;
    const currentPosition = getGridItemPosition(currentColumn, currentRow);

    const newPos = getGridItemPosition(newX as GridItemCoordinate, newY as GridItemCoordinate);
    // const newPositionX = direction === "left" ? currentPosition.x - stepSize * amount : (direction === "right" ? currentPosition.x + stepSize * amount : currentPosition.x);
    // const newPositionY = direction === "up" ? currentPosition.y - stepSize * amount : (direction === "down" ? currentPosition.y + stepSize * amount : currentPosition.y);
    sb.moveDecreasing(currentPosition, newPos, time, time + speed);

    // gridItem.currentRow = currentRow + amount * (direction === "up" ? -1 : (direction === "down" ? 1 : 0)) as GridItemCoordinate;
    // gridItem.currentColumn = currentColumn + amount * (direction === "left" ? -1 : (direction === "right" ? 1 : 0)) as GridItemCoordinate;
    gridItem.currentRow = newY as GridItemCoordinate;
    gridItem.currentColumn = newX as GridItemCoordinate;
};

export const moveRawGridItem = (gridItem: GridItem, time: number, newX: number, newY: number, speed: number = 100, original?: boolean) => {
    const { sb, currentRow, currentColumn } = gridItem;
    const currentPosition = getGridItemPosition(currentColumn, currentRow);

    const newPos = getGridItemPosition(newX as GridItemCoordinate, newY as GridItemCoordinate);
    sb.moveDecreasing(currentPosition, newPos, time, time + speed);

    gridItem.currentRow = newY as GridItemCoordinate;
    gridItem.currentColumn = newX as GridItemCoordinate;
};

export const hideGridItem = (x: GridItemCoordinate, y: GridItemCoordinate, time: number, speed: number = 100) => {
    const gridItem = getGridItems(x, y).find(g => !g.hidden);
    if (!gridItem) return;
    gridItem.sb.scale(time, time + speed, 0.45, 0, 2);
    gridItem.hidden = true;
};

export const showGridItem = (x: GridItemCoordinate, y: GridItemCoordinate, time: number, speed: number = 100) => {
    const gridItem = getGridItems(x, y).find(g => g.hidden);
    if (!gridItem) return;
    gridItem.sb.scale(time, time + speed, 0, 0.45, 2);
    gridItem.hidden = false;
};

export const forEachItem = (callback: (gridItem: GridItem) => any) => {
    grid.forEach(callback);
};

export const forEachItemInRow = (row: number, callback: (gridItem: GridItem) => any) => {
    grid.filter(g => g.currentRow === row).forEach(callback);
};

export const forEachItemInColumn = (column: number, callback: (gridItem: GridItem) => any) => {
    grid.filter(g => g.currentColumn === column).forEach(callback);
};

export const forEachVisibleItem = (callback: (gridItem: GridItem) => any) => {
    grid.filter(g => !g.hidden).forEach(callback);
};