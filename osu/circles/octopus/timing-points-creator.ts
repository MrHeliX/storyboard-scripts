import { parseHitObjects } from "../../../hitObjectsParser";
import * as fs from "fs";
import * as clipboardy from "clipboardy";

export const generateTimingPoints = async (): Promise<void> => {
    const allHitObjects = await parseHitObjects(`${__dirname}/true_circles.txt`)
    const invisibleCircles = await parseHitObjects(`${__dirname}/invisible_circles.txt`);
    const timingPoints = (await fs.readFileSync(`${__dirname}/timing_points.txt`)).toString();

    const extraTimingPoints = allHitObjects.map((h, i) => {
        const isInvisible = invisibleCircles.some(c => h.startTime === c.startTime);
        const volume = isInvisible ? "70" : "70";
        const startTime = isInvisible ? h.startTime - 10 : h.startTime - 10;
        const modulo = !invisibleCircles.some(c => h.startTime === c.startTime) ? (i % 3) + 3 : i % 3;
        return `${startTime},-100,4,2,${modulo + 1},${volume},0,0\r\n${startTime + 20},-100,4,2,0,70,0,0`;
    }).join("\r\n");

    const results = timingPoints.concat("\r\n", extraTimingPoints);
    clipboardy.writeSync(results);
    console.log("Timing points copied to clipboard");
};

generateTimingPoints();