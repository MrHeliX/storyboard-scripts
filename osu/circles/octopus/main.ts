import { Storyboard } from "../../../storyboard-helpers"
import { forEachItem, forEachItemInColumn, forEachItemInRow, forEachVisibleItem, getGridItem, getGridItemPosition, getGridStoryboards, GridItemCoordinate, hideGridItem, initializeGrid, moveColumn, moveGridItem, moveRawGridItem, moveRow, showGridItem } from "./grid";
import clipboardy from "clipboardy";
import { parseHitObjects } from "../../../hitObjectsParser";
import { HitCircle } from "../../../interfaces";

const storyboard: Storyboard = new Storyboard();

const startListeningTimes: number[] = [13150, 17950, 21550, 23950, 26350, 31150, 34750, 37150, 39550, 49150, 51850, 54250, 57850, 62350, 69550, 71050, 75550, 78250, 80650, 85750];
const startPlayingTimes: number[] = [15550, 19750, 22750, 25150, 28750, 32950, 35950, 38350, 44350, 50650, 53050, 56050, 59950, 65950, 70450, 73150, 77050, 79450, 83350, 87550];

const generateStoryboard = async () => {
    const hitObjects = (await parseHitObjects(`${__dirname}/circles.txt`)) as HitCircle[];
    const trueHitObjects = (await parseHitObjects(`${__dirname}/true_circles.txt`)) as HitCircle[];
    const invisibleCircles = await parseHitObjects(`${__dirname}/invisible_circles.txt`);

    initializeGrid();

    // Anticheat circles are drawn under the blocker so you don't see them during legit gameplay
    hitObjects.forEach(hitObject => {
        generateAntiCheatCircles(hitObject);
    });

    generateBlocker();
    generateListeningAndPlayingColors();

    setGridMovements();

    storyboard.append(getGridStoryboards());

    hitObjects.forEach(hitObject => {
        if (hitObject.startTime >= 58150 && hitObject.startTime < 70150)
            generateTaikoCircle(hitObject, hitObject.startTime >= 62950);
        else if (hitObject.startTime >= 71350 && hitObject.startTime < 73750)
            generateManiaCircle(hitObject);
        else
            generateCircle(hitObject);

        generateHitsound(hitObject);
    });

    trueHitObjects.forEach((hitObject, i) => {
        const isInvisible = invisibleCircles.some(c => hitObject.startTime === c.startTime);
        const modulo = !isInvisible ? (i % 3) + 3 : i % 3;
        generateHitBurst(hitObject, modulo);
        if (isInvisible)
            generateHitsound(hitObject);
    });

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const generateListeningAndPlayingColors = () => {
    storyboard
        .loadSprite("sb/square.png")
        .initScale(0, 20)
        .initColor(0, "93,187,114")
        .fade(startPlayingTimes[0], startPlayingTimes[0], 0, 1);

    storyboard.fadeOutDecreasing(90550, 92350, 1, 0);

    // Small triangles
    for (let x = -10; x < 30; x++) {
        for (let y = -10; y < 17; y++) {
            const rotationStart = (x + y) % 2 === 0 ? 0 : Math.PI * 2;
            const rotationEnd = (x + y) % 2 === 0 ? Math.PI * 2 : 0;
            storyboard
                .loadSprite("sb/triangle.png", { x: x * 32, y: y * 32 })
                .initColor(startPlayingTimes[0], "72,163,104")
                .initScale(startPlayingTimes[0], 0.2)
                .fade(startPlayingTimes[0], startPlayingTimes[0], 0, 1)
                .loop(startPlayingTimes[0], 20)
                .rotate(0, 8000, rotationStart, rotationEnd, 0)
                .endLoop();

            storyboard.fadeOutDecreasing(90000, 91500, 1, 0);
        }
    }

    storyboard
        .loadSprite("sb/square.png")
        .initScale(0, 20)
        .initColor(0, "233,100,121")
        .fade(startListeningTimes[0], startListeningTimes[0], 0, 1);

    startListeningTimes.forEach((t, i) => {
        const startPlayingTime = startPlayingTimes[i];
        storyboard.moveYIncreasing(-500, 320, t - 75, t + 250 - 75);

        if (startPlayingTime)
            storyboard.moveYDecreasing(320, -500, startPlayingTime - 75, startPlayingTime + 250 - 75);
    });

    // Small circles
    for (let x = -10; x < 30; x++) {
        for (let y = -10; y < 17; y++) {
            const loopStartTime = (x + y) % 2 === 0 ? 0 : 2000;
            storyboard
                .loadSprite("sb/circle.png", { x: x * 32, y: y * 32 })
                .initColor(0, "227,77,95")
                .initScale(0, 0.2)
                .fade(startListeningTimes[0], startListeningTimes[0], 0, 1)
                .loop(startListeningTimes[0], 20)
                .scale(loopStartTime, loopStartTime + 2000, 0.2, 0.1, 0)
                .scale(loopStartTime + 2000, loopStartTime + 4000, 0.1, 0.2, 0)
                .endLoop();

            startListeningTimes.forEach((t, i) => {
                const startPlayingTime = startPlayingTimes[i];
                storyboard.moveYIncreasing(y * 32 - 520, y * 32, t - 75, t + 250 - 75);

                if (startPlayingTime)
                    storyboard.moveYDecreasing(y * 32, y * 32 - 520, startPlayingTime - 75, startPlayingTime + 250 - 75);
            });
        }
    }
};

const generateBlocker = () => {
    //Anticheat
    storyboard
        .loadSprite("sb/circle.png")
        .initColor(13750, "125,185,182")
        .initScale(13750, 20)
        .fadeIn(13750)
        .fadeOut(90550);

    // Regular blocker
    storyboard
        .loadSprite("sb/zwart.jpg")
        .initScale(0, 20)
        .fadeIn(0, 1000, 0, 0.5)
        .fadeIn(11350, 12550, 0.5, 1)
        .fadeOut(90550, 92950, 1, 0.5)
        .fadeIn(95350, 96250, 0.5, 1)
        .fadeOut(120000);
};

const generateCircle = (hitObject: HitCircle) => {
    const { sbPosition, startTime } = hitObject;
    const startTimeAR = getARStartTime(startTime);
    storyboard
        .loadSprite("sb/circle.png", sbPosition)
        .initColor(startTimeAR, "125,185,182")
        .fadeIn(startTimeAR, startTime)
        .scale(startTimeAR, startTime, 0.1, 0.85)
        .fadeOut(startTime, startTime + 150);
};

const generateTaikoCircle = (hitObject: HitCircle, stay: boolean = false) => {
    const { sbPosition, startTime } = hitObject;
    const startTimeAR = getARStartTime(startTime);
    storyboard
        .loadSprite("sb/circle.png", sbPosition)
        .initColor(startTimeAR, "125,185,182")
        .scale(startTimeAR, startTime, 1, 0.2, 2)
        .moveXDecreasing(-100, sbPosition.x, startTimeAR, startTime);

    if (stay)
        storyboard.fadeOut(startTime + 900, startTime + 1100);
};

const generateManiaCircle = (hitObject: HitCircle) => {
    const { sbPosition, startTime } = hitObject;
    const startTimeAR = getARStartTime(startTime);
    storyboard
        .loadSprite("sb/circle.png", sbPosition)
        .initColor(startTimeAR, "125,185,182")
        .scale(startTimeAR, startTime, 1, 0.2, 2)
        .moveYDecreasing(-100, sbPosition.y, startTimeAR, startTime);
};

const generateAntiCheatCircles = (hitObject: HitCircle) => {
    forEachVisibleItem(item => {
        const position = getGridItemPosition(item.currentColumn, item.currentRow);
        if (position.x !== hitObject.sbPosition.x || position.y !== hitObject.sbPosition.y) {
            generateCircle({ ...hitObject, sbPosition: position });
        }
    });
};

const generateHitsound = (hitObject: HitCircle) => {
    storyboard.loadSound("sb/hitsound.wav", hitObject.startTime, 75);
};

const generateHitBurst = (hitObject: HitCircle, modulo: number) => {
    storyboard
        // .loadSprite("sb/circle_border.png", hitObject.sbPosition)
        .loadSprite("sb/circle.png", hitObject.sbPosition)
        .trigger(`HitSound${modulo + 1}`, hitObject.startTime - 75, hitObject.startTime + 74)
        .color(0, 0, "0,0,0", "185,113,147")
        .fadeInDecreasing(0, 100, 0, 1)
        .scale(0, 200, 0, 0.40)
        .fade(200, 400, 1, 0)
        .endTrigger();
};

const setGridMovements = () => {
    for (let i = 0; i < 10; i++) {
        const time = 48550 + 60 * i;
        const x: GridItemCoordinate = i % 2 === 1 ? -1 : 1;
        const y: GridItemCoordinate = Math.floor(i / 2) - 2 as GridItemCoordinate;
        hideGridItem(x, y, time);
    }

    moveRow(-2, 49750, "left", 1);
    moveRow(0, 50050, "left", 1);
    moveRow(2, 50350, "left", 1);

    moveRow(2, 52150, "right", 1);
    moveRow(0, 52450, "right", 2);
    moveRow(-2, 52750, "right", 1);

    forEachItemInRow(0, item => {
        if (item.originalColumn !== 0)
            moveRawGridItem(item, 56050, item.currentColumn - 1, item.currentRow);
    });
    // moveRow(0, 56050, "left", 1);

    forEachItemInColumn(-2, item => {
        hideGridItem(item.currentColumn, item.currentRow, 57250 - 75, 200);
    });

    moveGridItem(1, 0, 57250 - 75, 0, 0, 1, 200);

    forEachItemInColumn(0, item => {
        hideGridItem(item.currentColumn, item.currentRow, 57550 - 75, 200);
    });

    for (let y = -2; y <= 2; y++) {
        moveGridItem(2, y as GridItemCoordinate, 59950, -2, y, 4, 250);
        moveGridItem(-2, y as GridItemCoordinate, 59950, 2, y, 4);
    }

    for (let y = -2; y <= 2; y++) {
        const time = 61150 + (y + 2) * 300;
        for (let x = -1; x <= 2; x++) {
            showGridItem(x as GridItemCoordinate, y as GridItemCoordinate, time - 75, 200);
        }
    }

    forEachItem(item => {
        if (item.currentColumn !== 0 || item.currentRow !== 0)
            hideGridItem(item.currentColumn, item.currentRow, 69550, 200);
    });

    forEachItem(item => {
        if (item.currentRow === 0 && item.currentColumn !== 0)
            showGridItem(item.currentColumn, item.currentRow, 70750, 175);
        // hideGridItem(item.currentColumn, item.currentRow, 70450, 175);
    });

    forEachItemInRow(0, item => {
        const currentColumn = item.currentColumn;

        moveGridItem(item.currentColumn, 0, 73150, 0, item.currentColumn * -1, 1, 200);

        moveGridItem(0, currentColumn * -1 as GridItemCoordinate, 73150, 0, 0, 1, 200, true);
    });

    for (let y = -2; y <= 2; y++) {
        showGridItem(0, 0, 75400, 75);
        moveGridItem(0, y as GridItemCoordinate, 75400, y, 0, 1, 200, true);
    };

    forEachVisibleItem(item => {
        moveRawGridItem(item, 76750, item.currentRow * -1, item.currentColumn, 200);
        moveRawGridItem(item, 79150, item.currentRow * -1, item.currentColumn, 200);
    });

    forEachVisibleItem(item => {
        if (Math.abs(item.currentColumn) !== 2 && Math.abs(item.currentRow) !== 2)
            hideGridItem(item.currentColumn, item.currentRow, 83350, 200);
    });

    showGridItem(0, -1, 85150 - 75, 175);
    hideGridItem(0, -2, 85150 - 75, 175);
    showGridItem(1, 0, 85300 - 75, 175);
    hideGridItem(2, 0, 85300 - 75, 175);
    showGridItem(0, 1, 85450 - 75, 175);
    hideGridItem(0, 2, 85450 - 75, 175);
    showGridItem(-1, 0, 85600 - 75, 175);
    hideGridItem(-2, 0, 85600 - 75, 175);

    moveGridItem(0, -1, 85750, 2, 2, 1, 200);
    moveGridItem(1, 0, 86350, -2, -2, 1, 200);
    moveGridItem(0, 1, 86950, -2, 2, 1, 200);
    moveGridItem(-1, 0, 87550, 2, -2, 1, 200);

    hideGridItem(-2, -2, 90550, 200);
    hideGridItem(2, -2, 91150, 200);
    hideGridItem(-2, 2, 91750, 200);
    hideGridItem(2, 2, 92350, 200);
};

const getARStartTime = (startTime: number) => startTime - 550;

generateStoryboard();