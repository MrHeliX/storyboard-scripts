import { HitObjectOptions } from '../../interfaces';
import { Storyboard } from '../../storyboard-helpers';
import { HitCircle } from '../beatmap-parser/objects/HitObjects/HitCircle';

const colors = ["255,80,80", "18,124,255"];

export const generateSbHitCircle = (hitObject: HitCircle, options?: HitObjectOptions): Storyboard => {
    const sb = new Storyboard();

    getHitCircleSB(sb, hitObject, options);
    getHitCircleOverlaySB(sb, hitObject, options);
    getApproachCircleSB(sb, hitObject, options);
    if (!options?.hideNumber)
        getHitCircleNumberSB(sb, hitObject, options);

    return sb;
};

export const getStoryboardValues = (hitObject: HitCircle, options?: HitObjectOptions) => {
    const duration = getMsFromAr(options?.ar);
    return {
        csFactors: getPixelsFromCs(options?.cs),
        startTime: hitObject.StartTime - duration,
        endTime: hitObject.StartTime
    };
};

const getHitCircleSB = (sb: Storyboard, hitObject: HitCircle, options?: HitObjectOptions) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject, options);
    sb
        .loadSprite("hitcircle.png", hitObject.StoryboardPosition)
        .initScale(startTime, csFactors.decreasingHitCircleFactor)
        .initColor(startTime, options?.color ?? colors[1])
        .fade(startTime, endTime)
        .scale(endTime, endTime + 50, csFactors.decreasingHitCircleFactor, csFactors.increasingHitCircleFactor)
        .fade(endTime, endTime + 200, 1, 0);
};

const getHitCircleOverlaySB = (sb: Storyboard, hitObject: HitCircle, options?: HitObjectOptions) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject, options);
    sb
        .loadSprite("hitcircleoverlay.png", hitObject.StoryboardPosition)
        .initScale(startTime, csFactors.decreasingHitCircleFactor)
        .fade(startTime, endTime)
        .scale(endTime, endTime + 50, csFactors.decreasingHitCircleFactor, csFactors.increasingHitCircleFactor)
        .fade(endTime, endTime + 200, 1, 0);
};

const getApproachCircleSB = (sb: Storyboard, hitObject: HitCircle, options?: HitObjectOptions) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject, options);
    sb
        .loadSprite("approachcircle.png", hitObject.StoryboardPosition)
        .initColor(startTime, options?.color ?? colors[1])
        .fade(startTime, endTime)
        .scale(startTime, endTime, csFactors.increasingApproachRateFactor, csFactors.decreasingApproachRateFactor);
};

const getHitCircleNumberSB = (sb: Storyboard, hitObject: HitCircle, options?: HitObjectOptions) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject, options);
    let number = hitObject.ObjectNumber;
    while (number >= 10) number = number % 10 + 1;

    sb
        .loadSprite(`default-${number}.png`, hitObject.StoryboardPosition)
        .initScale(startTime, csFactors.decreasingNumberFactor)
        .fade(startTime, endTime);
};

const getMsFromAr = (ar: number): number => {
    return ar <= 5
        ? Math.round(1800 - (ar * 120))
        : Math.round(1200 - ((ar - 5) * 150));
};

const getPixelsFromCs = (cs: number = 4) => {
    if (cs < 0)
        return {
            decreasingHitCircleFactor: 0.5752727,
            increasingHitCircleFactor: 0.75,
            decreasingApproachRateFactor: 0.5896,
            increasingApproachRateFactor: 2.15,
            decreasingNumberFactor: 0.45,
            sliderBallAndFollowCircleFactor: 0.5694545
        };

    const radius = 54.4 - 4.48 * cs;

    const diameter = radius * 2;
    return {
        decreasingHitCircleFactor: diameter / 126.82680752971590690814982181494,
        increasingHitCircleFactor: (diameter / 126.82680752971590690814982181494) * 1.3037295181919809509472637933279,
        decreasingApproachRateFactor: diameter / 123.7449118046132971506105834464,
        increasingApproachRateFactor: (diameter / 123.7449118046132971506105834464) * 3.646540027137042062415196743555,
        decreasingNumberFactor: diameter / 162.13333333333333333333333333333,
        sliderBallAndFollowCircleFactor: diameter / 122.32282596576939230067004272748
    };
};
