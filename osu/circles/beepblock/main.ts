import { Storyboard, StoryboardAnchor } from "../../../storyboard-helpers";
import { Beatmap } from "../../beatmap-parser/Objects/Beatmap";
import { parseBeatmapFromFile } from "../../beatmap-parser/beatmap-parser";
import { HitObject } from "../../beatmap-parser/objects/HitObjects/HitObject";
import { beepBlockGenerateBottomLeftStoryboard } from "./bottom-left-sb";
import { beepBlockGenerateBottomRightStoryboard } from "./bottom-right-sb";
import { beepBlockSaveMap } from "./generate-map";
import _ from "lodash";
import { beepBlockGenerateTopLeftStoryboard } from "./top-left-sb";
import { beepBlockGenerateTopRightStoryboard } from "./top-right-sb";

const totalBeeps = 42;
const beepStart = 2477;
const finalBeep = 146477;
const beepLength = 2906 - beepStart;
const beepInterval = Math.round((finalBeep - beepStart) / totalBeeps);

const topLeft: StoryboardAnchor = "BottomRight";
const topRight: StoryboardAnchor = "BottomLeft";
const bottomLeft: StoryboardAnchor = "TopRight";
const bottomRight: StoryboardAnchor = "TopLeft";
const anchors: StoryboardAnchor[] = [topLeft, topRight, bottomLeft, bottomRight];
let lastFourIndices: number[] = [1, -1, -1, -1];
let sectionsLastTimeSeen = {
    TopLeft: 0,
    TopRight: 0,
    BottomLeft: 0,
    BottomRight: 0
};

const forcedAnchors: { time: number, anchor: StoryboardAnchor }[] = [
    { time: 60763, anchor: "BottomRight" },
    { time: 64192, anchor: "BottomLeft" },
    { time: 67620, anchor: "BottomRight" },
    { time: 71049, anchor: "BottomLeft" }
];

const blockedAnchors: { time: number, anchor: StoryboardAnchor }[] = [
    { time: 57335, anchor: "BottomRight" }
];

const calculateNewAnchor = (time: number): StoryboardAnchor => {
    const updatePreviousValues = (anchor: StoryboardAnchor) => {
        const otherAnchors = anchors.filter(a => a !== anchor);
        otherAnchors.forEach(a => sectionsLastTimeSeen[a]++);
        sectionsLastTimeSeen[anchor] = 0;
        lastFourIndices.unshift(anchors.findIndex(a => a === anchor));
        lastFourIndices.pop();
    };

    const forcedAnchor = forcedAnchors.find(a => a.time >= time - 10 && a.time <= time + 10);
    if (forcedAnchor) {
        updatePreviousValues(forcedAnchor.anchor);
        return forcedAnchor.anchor;
    };

    const blockedAnchor = blockedAnchors.find(a => a.time >= time - 10 && a.time <= time + 10);
    const blockedIndex = blockedAnchor ? anchors.findIndex(a => a === blockedAnchor.anchor) : -1;

    const unrecentAnchor = anchors.find(a => sectionsLastTimeSeen[a] >= 5);
    if (unrecentAnchor && unrecentAnchor !== blockedAnchor?.anchor) {
        updatePreviousValues(unrecentAnchor);
        return unrecentAnchor;
    }

    let index = Math.floor(Math.random() * 4);
    while (index === lastFourIndices[0] || index === blockedIndex || (!blockedIndex && lastFourIndices.filter(i => i === index).length >= 2))
        index = (index + 1) % 4;
    const nextAnchor = anchors[index];
    updatePreviousValues(nextAnchor);
    return nextAnchor;
};

const getFilePath = (difficulty: string): string => `O:\\osu!\\Songs\\kuribo98_-_Beep_Block_Skyway_but_in_128_time_signature_with_beeps\\kuribo98 - Beep Block Skyway but in 128 time signature (with beeps) (Mr HeliX) [${difficulty}].osu`;

let topLeftBG;
let topRightBG;
let bottomLeftBG;
let bottomRightBG;

const generateStoryboard = async (index: number) => {
    for (let i = 0; i < 1; i++) {
        const storyboard = new Storyboard();
        lastFourIndices = [1, -1, -1, -1];
        sectionsLastTimeSeen = {
            TopLeft: 0,
            TopRight: 0,
            BottomLeft: 0,
            BottomRight: 0
        };

        topLeftBG = new Storyboard().loadSprite("sb/background_top_left_active", { x: 320, y: 240 }, "Overlay", "BottomRight");
        topRightBG = new Storyboard().loadSprite("sb/background_top_right_active", { x: 320, y: 240 }, "Overlay", "BottomLeft");
        bottomLeftBG = new Storyboard().loadSprite("sb/background_bottom_left_active", { x: 320, y: 240 }, "Overlay", "TopRight");
        bottomRightBG = new Storyboard().loadSprite("sb/background_bottom_right_active", { x: 320, y: 240 }, "Overlay", "TopLeft");
    
        generateBlocker(storyboard);
    
        const [topLeft, topRight, bottomLeft, bottomRight] = await Promise.all(["Top left", "Top right", "Bottom left", "Bottom right"].map(async d => await parseBeatmapFromFile(getFilePath(`[PRACTICE] ${d}`))));
        const hitObjects: HitObject[] = topLeft.HitObjects.filter(h => h.StartTime < beepStart + 2.5 * beepLength);
    
        const beepsStoryboard = new Storyboard();
        let previousAnchor;
    
        for (let currentBeep = beepStart; currentBeep <= finalBeep; currentBeep += beepInterval) {
            const anchor = currentBeep === beepStart ? "BottomLeft" : calculateNewAnchor(currentBeep);
            generateBeepEffects(currentBeep, anchor, previousAnchor, beepsStoryboard);
    
            let section: Beatmap | null = null;;
            switch (anchor) {
                case "TopLeft": section = bottomRight; break;
                case "TopRight": section = bottomLeft; break;
                case "BottomLeft": section = topRight; break;
                case "BottomRight": section = topLeft; break;
            };
    
            if (!section) continue;
            const nextSectionStart = currentBeep + 2.5 * beepLength;
            const nextSectionEnd = nextSectionStart + beepInterval;
            const filteredHitObjects = section.HitObjects.filter(h => h.StartTime >= nextSectionStart && h.StartTime < nextSectionEnd);
            hitObjects.push(...filteredHitObjects);
            previousAnchor = anchor;
        }
    
        storyboard.append([topLeftBG, topRightBG, bottomLeftBG, bottomRightBG, beepsStoryboard]);
    
        generateField(storyboard);
    
        beepBlockGenerateTopLeftStoryboard(storyboard, topLeft.HitObjects, topLeft);
        beepBlockGenerateTopRightStoryboard(storyboard, topRight.HitObjects);
        beepBlockGenerateBottomLeftStoryboard(storyboard, bottomLeft.HitObjects);
        beepBlockGenerateBottomRightStoryboard(storyboard, bottomRight.HitObjects);
    
        const difficulty = `Mr HeliX's & YokesPai's Collab Baby #${index + 1}`;
        beepBlockSaveMap(storyboard, hitObjects, getFilePath(difficulty), difficulty);
    }
};

const generateBlocker = (storyboard: Storyboard) => {
    storyboard
        .loadSprite("bg.jpg")
        .initScale(-5000, 0.445)
        .fadeIn(-5000)
        .fadeOut(0);

    storyboard
        .loadSprite("sb/background_top_left", { x: 320, y: 240 }, "Overlay", "BottomRight")
        .fadeInDecreasing(-4000, -3000)
        .fadeOutDecreasing(146049, 147049);

    storyboard
        .loadSprite("sb/background_top_right", { x: 320, y: 240 }, "Overlay", "BottomLeft")
        .fadeInDecreasing(-4000, -3000)
        .fadeOutDecreasing(146049, 147049);


    storyboard
        .loadSprite("sb/background_bottom_left", { x: 320, y: 240 }, "Overlay", "TopRight")
        .fadeInDecreasing(-4000, -3000)
        .fadeOutDecreasing(146049, 147049);


    storyboard
        .loadSprite("sb/background_bottom_right", { x: 320, y: 240 }, "Overlay", "TopLeft")
        .fadeInDecreasing(-4000, -3000)
        .fadeOutDecreasing(146049, 147049);

    topLeftBG
        .fadeIn(-1000, -500)
        .fadeOut(3563, 3662);
};

const generateField = (storyboard: Storyboard) => {
    storyboard
        .loadSprite("sb/line.png", { x: -300, y: 240 }, "Overlay", "CentreLeft")
        .fadeIn(-3500)
        .vector(-3500, -2000, 0, 0.65, 3, 0.65, 1)
        .vector(145049, 146549, 3, 0.65, 0, 0.65, 2)
        .fadeOut(147763);

    storyboard
        .loadSprite("sb/line.png", { x: 320, y: -100 }, "Overlay", "CentreLeft")
        .fadeIn(-3500)
        .initRotate(-3500, 0.5 * Math.PI)
        .vector(-3500, -2000, 0, 0.65, 2, 0.65, 1)
        .vector(145049, 146549, 2, 0.65, 0, 0.65, 2)
        .fadeOut(147763);
};

const generateBeepEffects = (currentBeep: number, anchor: StoryboardAnchor, previousAnchor: StoryboardAnchor, sb: Storyboard) => {
    const sections = [
        { start: 0, end: 34620, type: "normal" },
        { start: 34620, end: 62049, type: "reverse" },
        { start: 62049, end: 103192, type: "normal" },
        { start: 103192, end: 130620, type: "reverse" },
        { start: 130620, end: 180000, type: "normal" },
    ];

    const section = sections.find(s => s.start <= currentBeep && s.end > currentBeep);

    const anchorsToBeep = section?.type === "reverse"
        ? _.shuffle(anchors.filter(a => a !== anchor && a !== previousAnchor)).concat(previousAnchor)
        : [anchor, anchor, anchor];

    const color = section?.type === "reverse" ? "130, 33, 30" : "137, 240, 164";

    for (let i = 0; i < 3; i++) {
        const t = currentBeep + beepLength * i;
        const a = anchorsToBeep[i];

        const startT = t - 500;
        sb
            .loadSprite("sb/square.png", { x: 320, y: 240 }, "Overlay", a)
            .initColor(startT, color)
            .initScale(startT, 20)

        if (section?.type === "reverse")
            sb
                .fade(startT, t, 0, 0.7, 12)
                .fadeOutIncreasing(t + 250 + beepLength * (2 - i), t + beepLength + beepLength * (2 - i), 0.7, 0);
        else
            sb
                .fadeInDecreasing(t, t + 100, 0, 0.7)
                .fadeOutIncreasing(t + 100, t + 250, 0.7, 0);
    }

    let bgStoryboard: Storyboard;
    switch (anchor) {
        case "BottomRight": bgStoryboard = topLeftBG; break;
        case "BottomLeft": bgStoryboard = topRightBG; break;
        case "TopRight": bgStoryboard = bottomLeftBG; break;
        case "TopLeft": bgStoryboard = bottomRightBG; break;
        default: throw new Error(`Unknown anchor: ${anchor}`);
    }

    bgStoryboard.fadeIn(currentBeep + 3 * beepLength - 100, currentBeep + 3 * beepLength, 0, 1);

    if (currentBeep + beepInterval < finalBeep)
        bgStoryboard.fadeOut(currentBeep + 3 * beepLength + beepInterval - 200, currentBeep + 3 * beepLength + beepInterval - 101, 1, 0);
    else
        bgStoryboard.fadeOutDecreasing(145129, 146129);
};

generateStoryboard(9);
