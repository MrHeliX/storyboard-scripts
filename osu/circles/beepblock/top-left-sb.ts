import { HitObjectOptions } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import { Beatmap } from "../../beatmap-parser/Objects/Beatmap";
import { HitCircle } from "../../beatmap-parser/objects/HitObjects/HitCircle";
import { HitObject, isSlider } from "../../beatmap-parser/objects/HitObjects/HitObject";
import { generateSbHitCircle, getStoryboardValues } from "../storyboard-circle-creator";
import { beepblockGenerateSbSlider } from "./beepblock-slider-creator";

const sliderColors = ["18,124,255", "255,80,80"];
export const beepBlockGenerateTopLeftStoryboard = (storyboard: Storyboard, hitObjects: HitObject[], beatmap: Beatmap): void => {
    const options: HitObjectOptions = { ar: beatmap.Difficulty.ApproachRate, cs: beatmap.Difficulty.CircleSize, hideNumber: true };
    let lastColorIndex = 1;

    [...hitObjects].reverse().forEach(h => {
        if (isSlider(h)) {
            lastColorIndex = (lastColorIndex + 1) % 2;
            storyboard.append([beepblockGenerateSbSlider(h, { ...options, color: sliderColors[lastColorIndex] })]);
        }
        else
            storyboard.append([generateSbHitCircle(h, { ...options, color: "207, 224, 72" })])
    });
};

const beepBlockGenerateSbHitCircle = (hitObject: HitCircle, options?: HitObjectOptions): Storyboard => {
    const sb = new Storyboard();

    getHitCircleSB(sb, hitObject, options);
    getApproachCircleSB(sb, hitObject, options);

    return sb;
};

const getHitCircleSB = (sb: Storyboard, hitObject: HitCircle, options?: HitObjectOptions) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject, options);
    sb
        .loadSprite("sb/circle.png", hitObject.StoryboardPosition)
        .initColor(1, options?.color ?? sliderColors[0])
        .fadeIn(startTime)
        .scale(startTime, endTime - 150, 0, csFactors.decreasingHitCircleFactor * 1.4, 1)
        .fadeOut(endTime, endTime + 200);
    sb

        .loadSprite("sb/circle_border.png", hitObject.StoryboardPosition)
        .fadeIn(startTime)
        .scale(startTime, endTime - 150, 0, csFactors.decreasingHitCircleFactor * 1.6, 1)
        .fadeOut(endTime, endTime + 200);
};

const getHitCircleOverlaySB = (sb: Storyboard, hitObject: HitCircle, options?: HitObjectOptions) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject, options);
    sb
        .loadSprite("sb/circle_border.png", hitObject.StoryboardPosition)
        .fade(startTime, endTime)
        .scale(endTime, endTime + 50, csFactors.decreasingHitCircleFactor, csFactors.increasingHitCircleFactor)
        .fade(endTime, endTime + 200, 1, 0);
};

const getApproachCircleSB = (sb: Storyboard, hitObject: HitCircle, options?: HitObjectOptions) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject, options);
    sb
        .loadSprite("approachcircle.png", hitObject.StoryboardPosition)
        .initColor(1, options?.color ?? sliderColors[0])
        .fade(startTime, endTime)
        .scale(startTime, endTime, csFactors.increasingApproachRateFactor, csFactors.decreasingApproachRateFactor);
};
