import { Position } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import { HitObject } from "../../beatmap-parser/objects/HitObjects/HitObject";
import { Vector2 } from "../../beatmap-parser/objects/Vector2";

const firstObject = 7192
const lastObject = 62049;
const sectionLength = (lastObject - firstObject) / 16;

export const beepBlockGenerateBottomLeftStoryboard = (storyboard: Storyboard, hitObjects: HitObject[]): void => {
    for (let i = 0; i < 4; i++) {
        storyboard
            .loadSprite("sb/rounded_square.png", { x: 280, y: 280 + i * 50 })
            .initScale(3763, 0)
            .fadeInDecreasing(3763, 4763, 0, 1)
            .scale(3763, 4763, 0, 0.13, 1)
            .scale(145049, 146549, 0.13, 0, 2)
            .fadeOutDecreasing(145049, 146549);

        storyboard
            .loadSprite("sb/line.png", { x: 280, y: 280 + i * 50 }, "Overlay", "CentreRight")
            .initVector(4763, 0, 0.25)
            .fadeIn(4763)
            .vector(4763 + i * 50, 6477 + i * 50, 0, 0.25, 2, 0.25, 2);

        if (i === 1 || i === 2)
            storyboard
                .rotate(65477, 66477, 0, 0.03 * Math.PI * (i % 2 === 1 ? -1 : 1), 1)
                .rotate(144335, 145000, 0.03 * Math.PI * (i % 2 === 1 ? -1 : 1), 0, 1);
            
        storyboard.moveXIncreasing(280, -300, 144500, 145500);
    }

    generatePart1HitObjects(storyboard, hitObjects);
};

const generatePart1HitObjects = (storyboard: Storyboard, hitObjects: HitObject[]) => {
    const colors = { 280: "199,85,50", 330: "21,179,131", 380: "90, 84, 161", 430: "193,82,196" };
    hitObjects.forEach((h, i) => {
        const nextObject = hitObjects[i + 1];
        const bigCircle = Math.round((h.StartTime - firstObject) / sectionLength * 10) % 10 === 0;
        const square = nextObject ? Math.round((nextObject.StartTime - firstObject) / sectionLength * 10) % 10 === 0 : false;

        storyboard
            .loadSprite(square ? "sb/square.png" : "sb/circle.png", { x: -500, y: h.StoryboardPosition.y });

        if (square) storyboard.initRotate(h.StartTime - 2000, 0.25 * Math.PI);

        storyboard
            .initScale(h.StartTime - 2000, bigCircle || square ? 0.35 : 0.2)
            .initColor(h.StartTime - 2000, colors[h.StoryboardPosition.y]);

        if (h.StartTime < 65477 || [280, 430].includes(h.StoryboardPosition.y))
            storyboard.moveX(-500, h.StoryboardPosition.x - 25, h.StartTime - 2000, h.StartTime);
        else {
            const inverse = h.StoryboardPosition.y === 380;
            const p = new Vector2(-500, h.StoryboardPosition.y);
            const q = new Vector2(h.StoryboardPosition.x - 25, h.StoryboardPosition.y);
            const r = calculatePointPosition(p, h.StoryboardPosition, inverse);
            const s = calculatePointPosition(q, h.StoryboardPosition, inverse);
            storyboard.move(h.StartTime - 2000, h.StartTime, r, s);
        }
    });
};

const calculatePointPosition = (position: Vector2, origin: Vector2, inverse: boolean): Position => {
    const currentRotation = inverse ? -0.03 * Math.PI : 0.03 * Math.PI;

    const relativePosition = position.subtract(origin);
    const correctedPosition = new Vector2(relativePosition.x, -relativePosition.y);
    const newPosition = new Vector2(
        correctedPosition.x * Math.cos(currentRotation) + correctedPosition.y * Math.sin(currentRotation),
        -(correctedPosition.x * Math.sin(currentRotation) + correctedPosition.y * Math.cos(currentRotation))
    );

    return newPosition.add(origin);
};
