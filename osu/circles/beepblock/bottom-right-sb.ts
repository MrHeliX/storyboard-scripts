import { Position } from "../../../interfaces";
import { Storyboard } from "../../../storyboard-helpers";
import { HitObject } from "../../beatmap-parser/objects/HitObjects/HitObject";
import { Vector2 } from "../../beatmap-parser/objects/Vector2";

const armRotations = [
    { start: 0.5 * Math.PI, end: 16.5 * Math.PI },
    { start: 7/6 * Math.PI, end: (16.5 + 2 / 3) * Math.PI },
    { start: 11/6 * Math.PI, end: (17.5 + 1 / 3) * Math.PI }
];

const firstObject = 7192
const lastObject = 62049;
const sectionLength = (lastObject - firstObject) / 16;

export const beepBlockGenerateBottomRightStoryboard = (storyboard: Storyboard, hitObjects: HitObject[]) => {
    const position: Position = { x: 450, y: 355 };
    storyboard
        .loadSprite("sb/circle.png", position)
        .initScale(3763, 0)
        .scale(3763, 4763, 0, 0.3, 1)
        .fadeInDecreasing(3763, 4763, 0, 1)
        .scale(65477, 66477, 0.3, 0.15, 2)
        .fadeOutIncreasing(65477, 66477, 1, 0);

    storyboard
        .loadSprite("sb/line_with_triangle.png", position, "Overlay", "CentreRight")
        .initVector(4763, 0, 0.25)
        .vector(4763, 6477, 0, 0.25, 0.24, 0.25, 1)
        .initRotate(4763, armRotations[0].start)
        .fadeInDecreasing(4763, 6477, 0, 1)
        .rotate(4763, 63763, armRotations[0].start, armRotations[0].end)
        .fadeOut(63763);

    storyboard
        .loadSprite("sb/line_with_triangle.png", { x: 450, y: 304 })
        .initVector(63763, 0.24, 0.25)
        .initRotate(63763, 0.5 * Math.PI)
        .fadeIn(63763)
        .vector(65477, 66477, 0.24, 0.25, 0.35, 0.35, 1)
        .moveY(304, 284, 65477, 66477, 1)
        .rotate(65477, 66477, 0.5 * Math.PI, Math.PI, 1)
        .fadeOutDecreasing(144049, 145049);

    storyboard
        .loadSprite("sb/circle.png", { x: 362, y: 284 })
        .scale(65477, 66477, 0, 0.5, 1)
        .fadeIn(65477, 66477, 0, 1)
        .scale(145049, 146549, 0.5, 0, 2)
        .fadeOutDecreasing(145049, 146549);

    storyboard
        .loadSprite("sb/line_with_triangle.png", position, "Overlay", "CentreRight")
        .initVector(4763, 0, 0.25)
        .vector(4763, 6477, 0, 0.25, 0.24, 0.25, 1)
        .initRotate(4763, armRotations[1].start)
        .fadeInDecreasing(4763, 6477, 0, 1)
        .rotate(4763, 63763, armRotations[1].start, armRotations[1].end)
        .fadeOut(63763);
        
    storyboard
        .loadSprite("sb/line_with_triangle.png", { x: 494, y: 380 })
        .initVector(63763, 0.24, 0.25)
        .initRotate(63763, 7/6 * Math.PI)
        .fadeIn(63763)
        .moveY(380, 360, 65477, 66477, 1)
        .rotate(65477, 66477, 7/6 * Math.PI, 5/3 * Math.PI, 1)
        .vector(65477, 66477, 0.24, 0.25, 0.35, 0.35, 1)
        .fadeOutDecreasing(144049, 145049);

    storyboard
        .loadSprite("sb/circle.png", { x: 450, y: 436 })
        .scale(65477, 66477, 0, 0.5, 1)
        .fadeIn(65477, 66477, 0, 1)
        .scale(145049, 146549, 0.5, 0, 2)
        .fadeOutDecreasing(145049, 146549);

    storyboard
        .loadSprite("sb/line_with_triangle.png", position, "Overlay", "CentreRight")
        .initVector(4763, 0, 0.25)
        .vector(4763, 6477, 0, 0.25, 0.24, 0.25, 1)
        .initRotate(4763, armRotations[2].start)
        .fadeInDecreasing(4763, 6477, 0, 1)
        .rotate(4763, 63763, armRotations[2].start, armRotations[2].end)
        .fadeOut(63763);

    storyboard
        .loadSprite("sb/line_with_triangle.png", { x: 406, y: 380 })
        .initVector(63763, 0.24, 0.25)
        .initRotate(63763, 11/6 * Math.PI)
        .fadeIn(63763)
        .rotate(65477, 66477, 11/6 * Math.PI, 7/3 * Math.PI, 1)
        .moveY(380, 360, 65477, 66477, 1)
        .vector(65477, 66477, 0.24, 0.25, 0.35, 0.35, 1)
        .fadeOutDecreasing(144049, 145049);

    storyboard
        .loadSprite("sb/circle.png", { x: 538, y: 284 })
        .scale(65477, 66477, 0, 0.5, 1)
        .fadeIn(65477, 66477, 0, 1)
        .scale(145049, 146549, 0.5, 0, 2)
        .fadeOutDecreasing(145049, 146549);

    const part1Objects = hitObjects.filter(h => h.StartTime < 65477);
    const part2Objects = hitObjects.filter(h => h.StartTime > 65477);
    generatePart1HitObjects(storyboard, part1Objects);
    generatePart2HitObjects(storyboard, part2Objects);
};

const generatePart1HitObjects = (storyboard: Storyboard, hitObjects: HitObject[]) => {
    const calculatePointPosition = (position: Vector2, origin: Vector2, time: number, armIndex: number): Position => {
        const arm = armRotations[armIndex];
        const rotationPerMillisecond = (arm.end - arm.start) / (63763 - 4763);
        const currentRotation = (time - 4763) * rotationPerMillisecond + 2 / 3 * Math.PI * armIndex;

        const relativePosition = position.subtract(origin);
        const correctedPosition = new Vector2(relativePosition.x, -relativePosition.y);
        const newPosition = new Vector2(
            correctedPosition.x * Math.cos(currentRotation) + correctedPosition.y * Math.sin(currentRotation),
            -(correctedPosition.x * Math.sin(currentRotation) + correctedPosition.y * Math.cos(currentRotation))
        );

        return newPosition.add(origin);
    };

    hitObjects.forEach((h, i) => {
        const nextObject = hitObjects[i + 1];
        const rawStartPosition = h.StoryboardPosition.subtract(new Vector2(0, 100));
        const armIndex = Math.floor(Math.random() * 3);
        const startTime = h.StartTime - 600;
        const startPosition = calculatePointPosition(rawStartPosition, h.StoryboardPosition, startTime, armIndex);

        const bigCircle = Math.round((h.StartTime - firstObject) / sectionLength * 10) % 10 === 0;
        const greenCircle = nextObject ? Math.round((nextObject.StartTime - firstObject) / sectionLength * 10) % 10 === 0 : false;

        storyboard
            .loadSprite("sb/circle.png", startPosition)
            .initColor(startTime, greenCircle ? "118, 204, 110" : "193,82,196")
            .initScale(startTime, bigCircle ? 0.25 : 0.15);

        let previousPosition = startPosition;;
        for (let t = startTime - 10; t <= h.StartTime; t += 10) {
            const progress = (t - startTime) / (h.StartTime - startTime);
            const updatedPosition = new Vector2(rawStartPosition.x, rawStartPosition.y + 90 * progress);
            const currentPosition = calculatePointPosition(updatedPosition, h.StoryboardPosition, t, armIndex);
            storyboard.move(t - 10, t, previousPosition, currentPosition);
            previousPosition = currentPosition;
        }
    });
};

const generatePart2HitObjects = (storyboard: Storyboard, hitObjects: HitObject[]) => {
    const startPositions = [
        { source: { x: 362, y: 284 }, end: { x: 450, y: 436 } },
        { source: { x: 450, y: 436 }, end: { x: 538, y: 284 } },
        { source: { x: 538, y: 284 }, end: { x: 362, y: 284 } }
    ];

    hitObjects.forEach((h, i) => {
        const nextObject = hitObjects[i + 1];
        const source = startPositions.find(p => p.end.x === h.StoryboardPosition.x && p.end.y === h.StoryboardPosition.y).source;
        if (!source) throw new Error(`Unknown position: ${h.StoryboardPosition.x},${h.StoryboardPosition.y}`);

        const bigCircle = Math.round((h.StartTime - firstObject) / sectionLength * 10) % 10 === 0;
        const greenCircle = nextObject ? Math.round((nextObject.StartTime - firstObject) / sectionLength * 10) % 10 === 0 : false;

        storyboard
            .loadSprite("sb/circle.png", source)
            .initColor(h.StartTime - 750, greenCircle ? "118, 204, 110" : "193,82,196")
            .initScale(h.StartTime - 750, bigCircle ? 0.25 : 0.15)
            .fadeIn(h.StartTime - 650)
            .move(h.StartTime - 750, h.StartTime + 100, source, h.StoryboardPosition)
            .fadeOut(h.StartTime);
    });
};
