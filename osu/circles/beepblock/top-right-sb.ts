import { Storyboard } from "../../../storyboard-helpers";
import { HitObject } from "../../beatmap-parser/objects/HitObjects/HitObject";

const storyboard = new Storyboard();

const firstObject = 7192
const lastObject = 62049;
const sectionLength = (lastObject - firstObject) / 16;

export const beepBlockGenerateTopRightStoryboard = (sb: Storyboard, hitObjects: HitObject[]): void => {
    generateGrid();
    hitObjects.forEach((h, i) => generateGridCircle(h, hitObjects[i + 1]));
    sb.append([storyboard]);
};

const generateGrid = () => {
    for (let x = 0; x < 5; x++) {
        for (let y = 0; y < 3; y++) {
            storyboard
                .loadSprite("sb/rounded_square.png", { x: 310 + (x + 1) * 50, y: 100 + y * 50 })
                .fadeInDecreasing(2049, 2549)
                .scale(2049, 2549, 0, 0.13, 1)
                .scale(145049, 146549, 0.13, 0, 2)
                .fadeOutDecreasing(145049, 146549);
        }
    }
};

const generateGridCircle = (hitObject: HitObject, nextObject?: HitObject) => {
    const { StartTime, StoryboardPosition } = hitObject;
    const ARStartTime = StartTime - 600;
    const bigTick = Math.round((hitObject.StartTime - firstObject) / sectionLength * 20) % 20 === 0;
    const nextBigTick = nextObject ? Math.round((nextObject.StartTime - firstObject) / sectionLength * 20) % 20 === 0 : false;

    let color = "213,104,88";
    if (bigTick) color = "18,124,255";
    else if (nextBigTick) color = "118, 204, 110";

    storyboard
        .loadSprite("sb/rounded_square.png", StoryboardPosition)
        .initColor(ARStartTime, color)
        .scale(ARStartTime, StartTime, 0, 0.13)
        .fadeOut(StartTime, StartTime + 100);
};
