import { writeFileSync } from "fs";
import { Storyboard } from "../../../storyboard-helpers";
import { HitObject, isSlider } from "../../beatmap-parser/objects/HitObjects/HitObject";
import { PathType } from "../../beatmap-parser/Objects/PathType";

export const beepBlockSaveMap = async (sb: Storyboard, hitObjects: HitObject[], filePath: string, difficulty: string) => {
    const map = `osu file format v14

[General]
AudioFilename: audio.mp3
AudioLeadIn: 0
PreviewTime: 7120
Countdown: 0
SampleSet: Soft
StackLeniency: 0
Mode: 0
LetterboxInBreaks: 0
UseSkinSprites: 1
WidescreenStoryboard: 1

[Editor]
Bookmarks: 335,3763,7192,10620,14049,17477,20906,24335,27763,31192,34620,38049,41477,44906,48335,51763,55192,58620,62049,65477,68906,72335,75763,79192,82620,86049,89477,92906,96335,99763,103192,106620,110049,113477,116906,120335,123763,127192,130620,134049,137477,140906,144335,147763
DistanceSpacing: 1
BeatDivisor: 6
GridSize: 8
TimelineZoom: 0.3

[Metadata]
Title:Beep Block Skyway but in 12/8 time signature (with beeps)
TitleUnicode:Beep Block Skyway but in 12/8 time signature (with beeps)
Artist:kuribo98
ArtistUnicode:kuribo98
Creator:Mr HeliX
Version:${difficulty}
Source:
Tags:
BeatmapID:0
BeatmapSetID:-1

[Difficulty]
HPDrainRate:0
CircleSize:4
OverallDifficulty:8.5
ApproachRate:0
SliderMultiplier:2
SliderTickRate:1

[Events]
//Background and Video events
0,0,"bg.jpg",0,0
//Break Periods
//Storyboard Layer 0 (Background)
//Storyboard Layer 1 (Fail)
//Storyboard Layer 2 (Pass)
//Storyboard Layer 3 (Foreground)
//Storyboard Layer 4 (Overlay)
${sb.toStoryboard()}
//Storyboard Sound Samples

[TimingPoints]
335,428.571428571429,4,2,0,100,1,0
335,-125,4,1,0,50,0,0

[Colours]
Combo1 : 255,128,255
Combo2 : 255,128,128
Combo3 : 128,255,128
Combo4 : 94,174,255

[HitObjects]
${hitObjects.map(h => {
    if (isSlider(h)) {
        // return "";
        let curveType;
        switch (h.Path.pathType) {
            case PathType.Bezier: curveType = "B"; break;
            case PathType.Catmull: curveType = "C"; break;
            case PathType.Linear: curveType = "L"; break;
            case PathType.PerfectCurve: curveType = "P"; break;
            default: console.error(`Unknown curve type: ${h.Path.pathType}`);
        };

        const [_, ...otherPoints] = h.Path.controlPoints;
        const calculatedOtherPoints = otherPoints.map(v => v.add(h.Position));

        return `${h.Position.x},${h.Position.y},${h.StartTime},${h.ObjectNumber === 1 ? 6 : 2},0,${curveType}|${calculatedOtherPoints.map(o => `${o.x}:${o.y}`).join("|")},${h.RepeatCount + 1},${h.Path.expectedDistance}`;
    };

    return `${h.Position.x},${h.Position.y},${h.StartTime},${h.ObjectNumber === 1 ? 5 : 1},${h.HitSound},0:0:0:0:`;
}).filter(l => l.length).join("\n")}
`;

    await writeFileSync(filePath, map);
};
