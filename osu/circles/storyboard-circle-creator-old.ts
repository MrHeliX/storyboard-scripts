import * as clipboardy from 'clipboardy';
import { parseHitObjects } from "../../hitObjectsParser";
import { HitCircle } from '../../interfaces';
import { Storyboard } from '../../storyboard-helpers';

const storyboard = new Storyboard();
const colors = ["255,80,80", "18,124,255"];
let currentColor = colors[1];

const generateHitCircles = async () => {
    const hitObjects = (await parseHitObjects(`${__dirname}/circles.txt`)) as HitCircle[];

    let currentNumber = 1;
    hitObjects.forEach(hitObject => {
        if (hitObject.circleNumber === 1) {
            currentNumber = 1;
            currentColor = colors.find(c => c !== currentColor);
        }

        getHitCircleSB(hitObject);
        getHitCircleOverlaySB(hitObject);
        getApproachCircleSB(hitObject);
        getHitCircleNumberSB(hitObject, currentNumber);
        currentNumber++;
    });

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const getStoryboardValues = (hitObject: HitCircle) => {
    const duration = getMsFromAr(hitObject);
    return {
        csFactors: getPixelsFromCs(hitObject),
        startTime: hitObject.startTime - duration,
        endTime: hitObject.startTime
    };
};

const getHitCircleSB = (hitObject: HitCircle) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject);
    storyboard
        .loadSprite("hitcircle.png", hitObject.sbPosition)
        .initScale(1, csFactors.decreasingHitCircleFactor)
        .initColor(1, currentColor)
        .fade(startTime, endTime)
        .scale(endTime, endTime + 50, csFactors.decreasingHitCircleFactor, csFactors.increasingHitCircleFactor)
        .fade(endTime, endTime + 200, 1, 0);
};

const getHitCircleOverlaySB = (hitObject: HitCircle) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject);
    storyboard
        .loadSprite("hitcircleoverlay.png", hitObject.sbPosition)
        .initScale(1, csFactors.decreasingHitCircleFactor)
        .fade(startTime, endTime)
        .scale(endTime, endTime + 50, csFactors.decreasingHitCircleFactor, csFactors.increasingHitCircleFactor)
        .fade(endTime, endTime + 200, 1, 0);
};

const getApproachCircleSB = (hitObject: HitCircle) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject);
    storyboard
        .loadSprite("approachcircle.png", hitObject.sbPosition)
        .initColor(1, currentColor)
        .fade(startTime, endTime)
        .scale(startTime, endTime, csFactors.increasingApproachRateFactor, csFactors.decreasingApproachRateFactor);
};

const getHitCircleNumberSB = (hitObject: HitCircle, circleNumber: number) => {
    const { csFactors, startTime, endTime } = getStoryboardValues(hitObject);
    storyboard
        .loadSprite(`default-${circleNumber}.png`, hitObject.sbPosition)
        .initScale(1, csFactors.decreasingNumberFactor)
        .fade(startTime, endTime);
};

const getMsFromAr = (hitObject: HitCircle) => {
    switch (hitObject.approachRate) {
        case 0:
            return 1800;
        case 1:
            return 1680;
        case 2:
            return 1560;
        case 3:
            return 1440;
        case 4:
            return 1320;
        case 5:
            return 1200;
        case 6:
            return 1050;
        case 7:
            return 900;
        case 8:
            return 750;
        case 8.5:
            return 675;
        case 9:
            return 600;
        case 9.5:
            return 525;
        case 10:
            return 450;
        case 10.33:
            return 400;
        case 10.5:
            return 375;
        case 11:
            return 300;
        case 11.5:
            return 225;
        case 12:
            return 150;
        default:
            return 600; //AR 9
    };
};

const getPixelsFromCs = (hitObject: HitCircle) => {
    if (hitObject.circleSize < 0)
        return {
            decreasingHitCircleFactor: 0.5752727,
            increasingHitCircleFactor: 0.75,
            decreasingApproachRateFactor: 0.5896,
            increasingApproachRateFactor: 2.15,
            decreasingNumberFactor: 0.45
        };

    let radius = 0;
    switch (hitObject.circleSize) {
        case 4:
            radius = 36.48;
            break;
        case 4.5:
            radius = 34.25;
            break;
        case 5:
            radius = 32.01;
            break;
        case 5.5:
            radius = 29.77;
            break;
        case 6:
            radius = 27.53;
            break;
        case 6.5:
            radius = 25.29;
            break;
        case 7:
            radius = 23.05;
            break;
        case 8:
            radius = 18.57;
            break;
        case 9:
            radius = 14.09;
            break;
        case 10:
            radius = 9.6;
            break;
        default:
            radius = 36.48;
    };

    const diameter = radius * 2;
    return {
        decreasingHitCircleFactor: diameter / 126.82680752971590690814982181494,
        increasingHitCircleFactor: (diameter / 126.82680752971590690814982181494) * 1.3037295181919809509472637933279,
        decreasingApproachRateFactor: diameter / 123.7449118046132971506105834464,
        increasingApproachRateFactor: (diameter / 123.7449118046132971506105834464) * 3.646540027137042062415196743555,
        decreasingNumberFactor: diameter / 162.13333333333333333333333333333
    };
};

generateHitCircles();