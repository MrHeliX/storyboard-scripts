import * as clipboardy from 'clipboardy';
import { parseHitObjects, isHitCircle } from "../../hitObjectsParser";
import { Slider } from '../../interfaces';
import { Storyboard } from '../../storyboard-helpers';

const storyboard = new Storyboard();

const generateHitbursts = async () => {
    const hitObjects = await parseHitObjects(`${__dirname}/hitbursts.txt`);

    hitObjects.forEach(hitObject => {
        if (isHitCircle(hitObject)) {
            storyboard
                .loadSprite("hit300.png", hitObject.sbPosition)
                .trigger("HitSound1", hitObject.startTime - 93, hitObject.startTime + 93)
                .fade(0, 100)
                .scale(0, 100, 0.3, 0.7)
                .scale(120, 140, 0.7, 0.5)
                .scale(140, 170, 0.5, 0.6)
                .fade(500, 1100, 1, 0)
                .endTrigger();
        }
        else {
            const slider = hitObject as Slider;
            if (slider.endTime) {
                const sliderTime = slider.endTime - slider.startTime;
                storyboard
                    .loadSprite("hit300.png", slider.sbEndPosition)
                    .trigger("HitSound1", slider.startTime - 93, slider.startTime + 93)
                    .fade(sliderTime, sliderTime + 100)
                    .scale(sliderTime, sliderTime + 100, 0.3, 0.7)
                    .scale(sliderTime + 120, sliderTime + 140, 0.7, 0.5)
                    .scale(sliderTime + 140, sliderTime + 170, 0.5, 0.6)
                    .fade(sliderTime + 500, sliderTime + 1100, 1, 0)
                    .endTrigger();
            }
            else
                console.error(`Slider without endtime: ${slider.startTime}`);
        }
    });

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

generateHitbursts();