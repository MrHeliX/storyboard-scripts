import * as clipboardy from "clipboardy";
import { parseHitObjects, isSlider } from "../../hitObjectsParser"
import { Slider, Position } from "../../interfaces"
import { Storyboard } from "../../storyboard-helpers";

const storyboard = new Storyboard();

const generateSliderBall = async () => {
    const sliders = (await parseHitObjects(`${__dirname}/sliders.txt`)) as Slider[];
    if (sliders.some(slider => !isSlider(slider))) {
        console.error("Detected objects that are not sliders. Please remove those");
        return;
    }

    sliders.forEach(slider => {
        getSliderBall(slider);
        getSliderBallFollowCircle(slider);
    });

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const getSliderBall = (slider: Slider): void => {
    storyboard
        .loadAnimation("sliderb.png", slider.sbPosition, 10, 10)
        .scale(slider.startTime, null, 1, 0.5694545)
        .fade(slider.startTime, null);

    const allPoints = [slider.sbPosition, ...slider.sbSliderPoints, slider.sbEndPosition];
    const duration = slider.endTime - slider.startTime;
    let previousPoint = { ...slider.sbPosition };
    let previousRotation = 0;
    let t = 1;
    while (t <= duration) {
        const pos = calculatePositionBezierCurve(allPoints, t / duration);
        const { rotation, shouldFlip } = calculateRotation(pos, previousPoint, slider.backwards);
        storyboard
            .move(slider.startTime + t - 1, slider.startTime + t, previousPoint, pos)
            .rotate(slider.startTime + t - 1, slider.startTime + t, previousRotation, rotation);
        if (shouldFlip)
            storyboard.flip(slider.startTime + t - 1, slider.startTime + t, "H");
        previousPoint = pos;
        previousRotation = rotation;
        t++;
    }
};

const getSliderBallFollowCircle = (slider: Slider): void => {
    storyboard
        .loadSprite("sliderfollowcircle.png", slider.sbPosition)
        .scale(slider.startTime, null, 1, 0.5694545)
        .fade(slider.startTime, null);

    const allPoints = [slider.sbPosition, ...slider.sbSliderPoints, slider.sbEndPosition];
    const duration = slider.endTime - slider.startTime;
    let previousPoint = { ...slider.sbPosition };
    let t = 1;
    while (t <= duration) {
        const pos = calculatePositionBezierCurve(allPoints, t / duration);
        storyboard.move(slider.startTime + t - 1, slider.startTime + t, previousPoint, pos);
        previousPoint = pos;
        t++;
    }
};

export const calculatePositionBezierCurve = (points: Position[], timeStamp: number): Position => {
    const degree = points.length - 1;
    let x = 0;
    let y = 0;
    points.forEach((point, index) => {
        if (index === 0) {
            x += Math.pow(1 - timeStamp, degree) * point.x;
            y += Math.pow(1 - timeStamp, degree) * point.y;
        }
        else if (index === degree) {
            x += Math.pow(timeStamp, degree) * point.x;
            y += Math.pow(timeStamp, degree) * point.y;
        }
        else {
            x += getBinomial(degree, index) * Math.pow(timeStamp, index) * Math.pow(1 - timeStamp, degree - index) * point.x;
            y += getBinomial(degree, index) * Math.pow(timeStamp, index) * Math.pow(1 - timeStamp, degree - index) * point.y;
        }
    });

    return { x, y };
};

const calculateRotation = (currentPosition: Position, previousPosition: Position, backwards: boolean): { rotation: number, shouldFlip: boolean } => {
    const deltaX = Math.abs(currentPosition.x - previousPosition.x);
    const deltaY = Math.abs(currentPosition.y - previousPosition.y);

    let radians = deltaX === 0 ? Math.PI * 0.25 : Math.atan(deltaY / deltaX);

    const isClockwiseRotation = (currentPosition.x > previousPosition.x && currentPosition.y > previousPosition.y) || (currentPosition.x < previousPosition.x && currentPosition.y < previousPosition.y);
    const isGoingRight = currentPosition.x > previousPosition.x;

    if (!isClockwiseRotation)
        radians *= -1;
    // if (!isGoingRight)
    //     radians += Math.PI;

    const shouldFlip = backwards ? isGoingRight : !isGoingRight;
    return { rotation: radians, shouldFlip };
};

const getBinomial = (n: number, k: number): number => {
    return getFactorial(n) / (getFactorial(k) * getFactorial(n - k));
};

const getFactorial = (n: number): number => {
    if (n === 1)
        return 1;
    else
        return n * getFactorial(n - 1);
};

generateSliderBall();