import { Position } from "./interfaces";
import * as clipboardy from "clipboardy";

export type StoryboardLayer = "Background" | "Fail" | "Pass" | "Foreground" | "Overlay";
export type StoryboardAnchor = "TopLeft" | "TopCentre" | "TopRight" | "CentreLeft" | "Centre" | "CentreRight" | "BottomLeft" | "BottomCentre" | "BottomRight";

export class Storyboard {
    private storyboard: string[];
    private nestingLevel: number;

    constructor() {
        this.storyboard = [];
        this.nestingLevel = 0;
    };

    public getStoryboard(): string[] {
        return this.storyboard;
    };

    public loadSprite(fileName: string, position: Position = { x: 320, y: 240 }, layer: StoryboardLayer = "Overlay", anchor: StoryboardAnchor = "Centre"): Storyboard {
        this.storyboard.push(`Sprite,${layer},${anchor},"${fileName}",${position.x},${position.y}`);
        this.nestingLevel = 1;
        return this;
    };

    public loadAnimation(fileName: string, position: Position, numberOfFrames: number, milliSecondsPerFrame: number, loopType: string = "LoopForever"): Storyboard {
        this.storyboard.push(`Animation,Overlay,Centre,${fileName},${position.x},${position.y},${numberOfFrames},${milliSecondsPerFrame},${loopType}`);
        this.nestingLevel = 1;
        return this;
    };

    public loadSound(fileName: string, time: number, volume: number = 100, layer: number = 3): Storyboard {
        this.storyboard.push(`Sample,${time},${layer},"${fileName}",${volume}`);
        this.nestingLevel = 1;
        return this;
    };

    public move(startTime: number, endTime: number, startPos: Position, endPos: Position, moveType: number = 0): Storyboard {
        this.storyboard.push(`${this.spaces()}M,${moveType},${startTime},${endTime || ""},${startPos.x},${startPos.y},${endPos.x},${endPos.y}`);
        return this;
    };

    public moveDecreasing(startPos: Position, endPos: Position, startTime: number, endTime: number = startTime): Storyboard {
        return this.move(startTime, endTime, startPos, endPos, 1);
    };

    public moveIncreasing(startPos: Position, endPos: Position, startTime: number, endTime: number = startTime): Storyboard {
        return this.move(startTime, endTime, startPos, endPos, 2);
    };

    public moveX(startX: number, endX: number, startTime: number, endTime: number = startTime, moveType: number = 0): Storyboard {
        this.storyboard.push(`${this.spaces()}MX,${moveType},${startTime},${endTime},${startX},${endX}`);
        return this;
    };

    public moveXDecreasing(startX: number, endX: number, startTime: number, endTime: number = startTime): Storyboard {
        return this.moveX(startX, endX, startTime, endTime, 1);
    };

    public moveXIncreasing(startX: number, endX: number, startTime: number, endTime: number = startTime): Storyboard {
        return this.moveX(startX, endX, startTime, endTime, 2);
    };

    public moveY(startY: number, endY: number, startTime: number, endTime: number = startTime, moveType: number = 0): Storyboard {
        this.storyboard.push(`${this.spaces()}MY,${moveType},${startTime},${endTime},${startY},${endY}`);
        return this;
    };

    public moveYDecreasing(startY: number, endY: number, startTime: number, endTime: number = startTime): Storyboard {
        return this.moveY(startY, endY, startTime, endTime, 1);
    };

    public moveYIncreasing(startY: number, endY: number, startTime: number, endTime: number = startTime): Storyboard {
        return this.moveY(startY, endY, startTime, endTime, 2);
    };

    public fade(startTime: number, endTime: number, fadeStart: number = 0, fadeEnd: number = 1, fadeType: number = 0): Storyboard {
        this.storyboard.push(`${this.spaces()}F,${fadeType},${startTime},${endTime || ""},${fadeStart},${fadeEnd}`);
        return this;
    };

    public fadeIn(startTime: number, endTime: number = startTime, fadeStart: number = 0, fadeEnd: number = 1): Storyboard {
        return this.fade(startTime, endTime, fadeStart, fadeEnd, 0);
    };

    public fadeInDecreasing(startTime: number, endTime: number = startTime, fadeStart: number = 0, fadeEnd: number = 1): Storyboard {
        return this.fade(startTime, endTime, fadeStart, fadeEnd, 1);
    };

    public fadeInIncreasing(startTime: number, endTime: number = startTime, fadeStart: number = 0, fadeEnd: number = 1): Storyboard {
        return this.fade(startTime, endTime, fadeStart, fadeEnd, 2);
    };

    public fadeOut(startTime: number, endTime: number = startTime, fadeStart: number = 1, fadeEnd: number = 0): Storyboard {
        return this.fade(startTime, endTime, fadeStart, fadeEnd, 0);
    };

    public fadeOutDecreasing(startTime: number, endTime: number = startTime, fadeStart: number = 1, fadeEnd: number = 0): Storyboard {
        return this.fade(startTime, endTime, fadeStart, fadeEnd, 1);
    };
    
    public fadeOutIncreasing(startTime: number, endTime: number = startTime, fadeStart: number = 1, fadeEnd: number = 0): Storyboard {
        return this.fade(startTime, endTime, fadeStart, fadeEnd, 2);
    };

    public scale(startTime: number, endTime: number, scaleStart: number, scaleEnd: number, scaleType: number = 0): Storyboard {
        this.storyboard.push(`${this.spaces()}S,${scaleType},${startTime},${endTime || ""},${scaleStart},${scaleEnd}`);
        return this;
    };

    public initScale(time: number, scale: number): Storyboard {
        return this.scale(time, time, 0, scale);
    };

    public vector(startTime: number, endTime: number, scaleStartX: number, scaleStartY: number, scaleEndX: number, scaleEndY: number, scaleType: number = 0): Storyboard {
        this.storyboard.push(`${this.spaces()}V,${scaleType},${startTime},${endTime || ""},${scaleStartX},${scaleStartY},${scaleEndX},${scaleEndY}`);
        return this;
    };

    public initVector(time: number, x: number, y: number): Storyboard {
        return this.vector(time, time, 0, 0, x, y);
    };

    public color(startTime: number, endTime: number, colorStart: string = "0,0,0", colorEnd: string = "255,255,255", colorType: number = 0): Storyboard {
        this.storyboard.push(`${this.spaces()}C,${colorType},${startTime},${endTime || ""},${colorStart},${colorEnd}`);
        return this;
    };

    public initColor(time: number, color: string, white?: boolean): Storyboard {
        return this.color(time, time, white ? "255,255,255" : "0,0,0", color);
    };

    public rotate(startTime: number, endTime: number, rotateStart: number, rotateEnd: number, rotateType: number = 0): Storyboard {
        this.storyboard.push(`${this.spaces()}R,${rotateType},${startTime},${endTime},${rotateStart},${rotateEnd}`);
        return this;
    };

    public initRotate(time: number, rotation: number): Storyboard {
        return this.rotate(time, time, rotation, rotation);
    };

    public trigger(condition: string, startTime: number, endTime: number): Storyboard {
        this.storyboard.push(`${this.spaces()}T,${condition},${startTime},${endTime}`);
        this.nestingLevel++;
        return this;
    };

    public flip(startTime: number, endTime: number, type: "H" | "V"): Storyboard {
        this.storyboard.push(`${this.spaces()}P,0,${startTime},${endTime},${type}`);
        return this;
    };

    public endTrigger(): Storyboard {
        this.nestingLevel--;
        return this;
    };

    public loop(startTime: number, loopCount: number): Storyboard {
        this.storyboard.push(`${this.spaces()}L,${startTime},${loopCount}`);
        this.nestingLevel++;
        return this;
    };

    public endLoop(): Storyboard {
        this.nestingLevel--;
        return this;
    };

    public toStoryboard(): string {
        return this.storyboard.join(`\r\n`);
    };

    public append(sbs: Storyboard[]): Storyboard {
        this.storyboard.push(...(sbs.flatMap(sb => sb.storyboard)));
        return this;
    };

    private spaces(): string {
        const array: string[] = [];
        for (let i = 0; i < this.nestingLevel; i++) {
            array.push(" ");
        }
        return array.join("");
    };

    public copyToClipboard(): void {
        clipboardy.writeSync(this.toStoryboard());
        console.log("Storyboard copied to clipboard");
    };
};