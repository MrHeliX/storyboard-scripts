import { HitObject } from "./osu/beatmap-parser/objects/HitObjects/HitObject";
import { Vector2 } from "./osu/beatmap-parser/objects/Vector2";

export interface Position {
    x: number;
    y: number;
}

export type Direction = "left" | "right";
export type CtbDirection = "up" | "down";
export type ManiaDirection = "up" | "down";
export type TaikoColor = "red" | "blue";

export interface HitCircle {
    startTime: number;
    position: Position;
    sbPosition: Position;
    approachRate: number;
    circleSize: number;
    color: string;
    backwards?: boolean;
    rawString: string;
    circleNumber: number;
};

export interface Slider {
    startTime: number;
    endTime?: number;
    position: Position;
    sbPosition: Position;
    sliderPoints: Position[];
    sbSliderPoints: Position[];
    endPosition: Position;
    sbEndPosition: Position;
    backwards?: boolean;
    rawString: string;
    circleNumber: number;
    approachRate: number;
    circleSize: number;
    color: string;
};

export interface TaikoCircle {
    timeStamp: number;
    startPosition: Position;
    startPositionSB: Position;
    endPosition: Position;
    endPositionSB: Position;
    duration: number;
    color: TaikoColor;
    direction: Direction;
};

export interface CtbFruit {
    timeStamp: number;
    startPosition: Position;
    startPositionSB: Position;
    endPosition: Position;
    endPositionSB: Position;
    duration: number;
    direction: CtbDirection;
};

export interface ManiaNote {
    timeStamp: number;
    startPosition: Position;
    startPositionSB: Position;
    endPosition: Position;
    endPositionSB: Position;
    approachRate: number;
    direction: ManiaDirection;
    length: number;
};

export type HitObjectOld = HitCircle | Slider;

export interface HitObjectOptions {
    ar?: number,
    cs?: number,
    color?: string
    hideNumber?: boolean
};