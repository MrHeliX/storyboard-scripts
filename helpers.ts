import { Position } from "./interfaces";
import { Vector2 } from "./osu/beatmap-parser/objects/Vector2";

export const isDefined = <T>(item: T | undefined | null): item is T => !!item;

export const convertToSBPosition = (pos: Position | Vector2): Position => {
    return { x: pos.x + 64, y: pos.y + 56 };
};

export const convertApproachRateToMilliseconds = (ar: number): number => {
    return ar <= 5
        ? 1800 - (ar * 120)
        : 1200 - ((ar - 5) * 150);
};