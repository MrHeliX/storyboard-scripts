import * as clipboardy from "clipboardy";
import { Storyboard } from "../storyboard-helpers";
import { parseCtbObjects } from "../hitObjectsParser";
import { CtbFruit } from "../interfaces";

const storyboard = new Storyboard();

export const generateCtbObjects = async () => {
    const ctbFruits = (await parseCtbObjects(`${__dirname}/ctb.txt`));
    ctbFruits.forEach(fruit => {
        generateSBFruit(fruit);
    });

    clipboardy.writeSync(storyboard.toStoryboard());
    console.log("Storyboard copied to clipboard");
};

const generateSBFruit = (ctbFruit: CtbFruit): void => {
    const { startPositionSB, endPositionSB, timeStamp, duration } = ctbFruit;
    storyboard
        .loadSprite("ctb-grapes.png", startPositionSB)
        .scale(1, null, 0, 0.6)
        .fade(timeStamp - duration, timeStamp - duration)
        .move(timeStamp - duration, timeStamp, startPositionSB, endPositionSB);
};

generateCtbObjects();